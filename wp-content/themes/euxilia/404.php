<?php get_header(); ?>

    <main id="main" class="site-main" role="main">
        <section class="content-404 et_pb_section">
            <div class="vertical">
                <div class="vertical-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-offset-1 col-xs-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 wrapper-404">
                                <h1>404</h1>
                                <p><?php pll_e('Siamo spiacenti ma non abbiamo trovato il contenuto che stavi cercando'); ?>.</p>
                                <a href="<?php echo home_url(); ?>" class=" et_pb_button"><?php pll_e('Torna alla home'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- #main -->

<?php get_footer();
