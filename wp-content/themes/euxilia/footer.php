</div><!--ANIMSITION-->
<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container-fluid wrap">
        <div class="footer-logo">
            <span class="auxiell-icon logo">
                                 <?php echo file_get_contents( get_stylesheet_directory_uri().'/resources/assets/vectors/logo-euxilia.svg'); ?>
            </span>
        </div>

        <nav class="footer-navigation" role="navigation" aria-label="<?php _e( 'Footer menu', THEME_SLUG ); ?>">
            <div class="row">

                <div class="col-xs-12 social-links-col">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'social',
                        'menu_class'     => 'social-links-menu',
                        'depth'          => 1,
                    ) );
                    ?>
                </div>
            </div>
        </nav><!-- .social-navigation -->

        <?php get_template_part( 'template-parts/footer/site', 'info' ); ?>
    </div><!-- .wrap -->
    <a id="nooo-credits" href="http://noooagency.com" rel="nofollow" target="_blank"><img src="<?php echo asset_path('images/nooo-agency-logo-white.png'); ?>" alt="noooagency logo"></a>
</footer><!-- #colophon -->
<?php wp_footer(); ?>
</body>
<?php if( is_page( fetchTranslatedSlug('la-nostra-proposta-valore') ) ){
    get_template_part( 'template-parts/components/popup', 'template' );
} ?>

</html>
