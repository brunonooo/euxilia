<?php
/**
 * INCLUDE TRANSLATIONS IF MULTILANG IS ACTIVE
 **/
$multiLang = defined( 'POLYLANG_VERSION' );
if($multiLang){
    include 'app/translations.php';
}

require_once 'app/ManifestInterface.php';
require_once 'app/JsonManifest.php';
require_once 'app/detects.php';
require_once 'app/helpers.php';
require_once 'app/theme-config.php';
require_once 'app/option-pages.php';
require_once 'app/resources.php';
require_once 'app/theme-components.php';

/**
 * LOAD EXITING THEME TRANSLATIONS
 **/
load_theme_textdomain( 'auxiell', get_bloginfo('stylesheet_directory').'/lang' );


/**
 * INCLUDE CUSTOM DIVI BUILDER MODULES
 **/
include(get_stylesheet_directory() . '/custom-builder-modules.php');

/**
 * INCLUDING CUSTOM OPTIONS PAGES
 **/

include_once "custom-options-shortcodes/euxilia-page-intro.php";
include_once "custom-options-shortcodes/edxiting-examples.php";
include_once "custom-options-shortcodes/edxiting-examples-feed.php";
include_once "custom-options-shortcodes/edxiting-phone.php";
include_once "custom-options-shortcodes/edxiting-gallery.php";
include_once "custom-options-shortcodes/youtube-videos.php";
include_once "custom-options-shortcodes/timeline-module.php";
include_once "custom-options-shortcodes/main-news-slider.php";
include_once "custom-options-shortcodes/news-filters-and-grid-module.php";
if(!is_admin()){
include_once "custom-options-shortcodes/job-module.php";
}
include_once "custom-options-shortcodes/map.php";
include_once "custom-options-shortcodes/euxilia_schemes.php";
include_once "custom-options-shortcodes/forms.php";
include_once "custom-options-shortcodes/social-icons.php";

register_nav_menus( array(
    'header-menu'       =>  'Header Menu',
    'quick-links'       =>  'Quick Links',
    'edxiting-footer'       =>  THEME_NAME.' Footer',
    'social'       =>  'Social',
));



/**s
 * MODIFY READ MORE LINK
 **/
function modify_read_more_link() {
    $readMore='|Read More';

    if(defined( 'POLYLANG_VERSION' )){
        $readMore = pll__('Read More');
    }

    return '<a class="more-link" href="' . get_permalink() . '">'. $readMore .'</a>';
}

add_filter( 'the_content_more_link', 'modify_read_more_link' );

/**
 * STATIC IMG MARKUP GENERATOR
 **/
function static_img_markup($name, $alt, $classes='', $attributes=false){
    $markup ='<img width="100%" class="seo-img '. $classes .'" '. $attributes . ' src="'.get_bloginfo('stylesheet_directory').'/assets/images/'.$name.'" alt="'.$alt.'" srcset="' . get_bloginfo('stylesheet_directory') . '/assets/images/' . $name . ' 1x,' . get_bloginfo('stylesheet_directory') . '/assets/images/@2x/' . $name . ' 2x" alt="' . $alt . '"/>';
    echo $markup;
}

/**
 * UPDATE MAPS API KEY SETTING
 **/
function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyDCioepAYK2zeTztB5uucDsjRFSnsPTHjI');
}

add_action('acf/init', 'my_acf_init');


function my_et_builder_post_types( $post_types ) {
    $post_types[] = 'examples-edxiting';

    return $post_types;
}
add_filter( 'et_builder_post_types', 'my_et_builder_post_types' );


/**
 * CUSTOM GET THE TITLE
 **/
function custom_get_title($limit){

    $title = get_the_title();
    $title = preg_replace(" (\[.*?\])",'',$title);
    $title = strip_shortcodes($title);
    $title = strip_tags($title);

    if( strlen($title) > $limit ){
        $title = substr($title, 0, $limit);
        $title = substr($title, 0, strripos($title, " "));
        $title = trim(preg_replace( '/\s+/', ' ', $title));
        $title = $title.'...';
    }

    return $title;
}

function custom_excerpt_length( $length ) {

    $length = !empty($length) ?  30 : $length;

    return $length;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_excerpt_more( $more ) {
    $more = !empty($more) ? '...' : $more;
    return $more;
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * NIFTY GET FORM NAME FUNCTION
 **/
function get_form_name($formShortcode){
    $start = 7 + strpos( $formShortcode , 'title="' );
    $end = strpos( $formShortcode , '"', $start );
    $name =  substr($formShortcode, $start, $end-$start);
    return $name;
}

/**
 * CHANGE WP-MAIL FUNCTION CONTENT TYPE
 **/
function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );