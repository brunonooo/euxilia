<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php get_template_part( 'template-parts/header/header', 'favicons' ); ?>
    <?php wp_head(); ?>

    <!-- no js support -->
    <noscript>
        <link rel="stylesheet" href="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/css/noscript.css">
    </noscript>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>

        /**
         *
         *
         * SETUP GlobalObject
         *
         */

        window.gc = {
            isMobile: false,
            themePath: '<?php echo get_stylesheet_directory_uri(); ?>',
            domain: '<?php echo get_site_url(); ?>',
            lang: '<?php  echo ( defined('CURRLANG') ? CURRLANG : false ); ?>',
            cookies: null,
            fpEnabled: null,
            gmapsApiReady: null,
            mapsCodeReady: null,
            initMap: null,
            mapsCheck: function(){
                console.log('mapscheck');

                if(window.gc.mapsCodeReady){
                    window.gc.initMap();
                }

                window.gc.gmapsApiReady = true;
            }
        };

        /**
         * Check if js is available
         */
        var root = document.getElementsByTagName('html')[0];

        if (root.classList){
            root.classList.remove('no-js');
            root.classList.add('js');
        } else {
            root.className += ' js';
        }

    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-98417716-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-120239430-1');
    </script>

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>

    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#ffffff",
                        "text": "#000000"
                    },
                    "button": {
                        "background": "#000000",
                        "text": "#ffffff"
                    }
                },

                "position": "bottom-left",
                "content": {
                    "message": "<?php _e('Questo sito internet utilizza cookies per migliorare l\'esperienza d\'uso. Navigando ne autorizzi l\'installazione in accordo con la ', 'Edxiting' ); ?>",
                    "dismiss": "<?php _e('Accetto', 'Edxiting' ); ?>",
                    "link": "Cookie policy.",
                    "href": "http://euxilia.noooserver.com/privacy-policy"
                }
            })});
    </script>

</head>

<?php global $post;
$post_slug = fetchBaseTranslationSlug( $post->post_name );
$extraClasses = '';

if(is_page('i-nostri-processi')){$extraClasses = 'page-template-default';}?>

<body id="auxiell" <?php body_class('auxiell '.$post_slug.' '.$extraClasses); ?>>
<div id="preloader"></div>
<div class="full-overlay"></div>

<header id="masthead" class="site-header" role="banner">
    <?php
    get_template_part( 'template-parts/navigation/navigation', 'top' );
    get_template_part( 'template-parts/navigation/navigation', 'right' ); ?>
</header><!-- #masthead -->

<div class="animsition <?php if(is_page(fetchTranslatedSlug('la-nostra-proposta-valore'))){
    echo 'skrollr-body';
}?>">
