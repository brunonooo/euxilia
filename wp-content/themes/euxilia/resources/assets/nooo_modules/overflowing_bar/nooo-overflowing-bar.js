export default (function (){
    let _selector, _container, _scroller, _scrollerWidth, _overflow;

    function _calcOverflow(){

        if(_scroller.hasChildNodes()){

            var c = 0;
            _scroller.childNodes.forEach(function(n, i){
                c += (n.nodeType === 1 ? n.clientWidth : 0);
            });

            return c  - _scrollerWidth;
        }
    }

    function _onResize(e){
        _scrollerWidth = _scroller.clientWidth;
        _overflow = _calcOverflow();
    }

    function _onPan(e){
        if(e.target.scrollLeft > 3){
            H.debounce(H.extraClass(_container, 'scrolling'), 50);
        }else{
            H.debounce(H.deleteClass(_container, 'scrolling'), 50)
        }

        if(e.target.scrollLeft > _overflow  - 3){
            H.debounce(H.deleteClass(_container, 'scrolling'), 50)
            H.debounce(H.extraClass(_container, 'scrolled'), 50)
        }else{
            H.debounce(H.deleteClass(_container, 'scrolled'), 50)
        }
    }

    function _init(cSelector){
        _selector = cSelector || '.overflowing-bar';

        _container = document.querySelectorAll(_selector)[0];

        if(_container){
            _scroller = _container.querySelectorAll('.overflowing-inner')[0];
            _scrollerWidth = _scroller.clientWidth;
            _overflow = _calcOverflow();

            _scroller.addEventListener('scroll', _onPan);
            _scroller.addEventListener('resize', _onResize);
        }else{
            console.log('You\'re doing it wrong! There is no element matching the specified selector in the page!');
        }
    }

    return {
        init: _init
    }

})();