import merge from 'deepmerge';

export default ( function(){

    let isIn = false;

    let opt = {
        selector: '#back-to-top',
        dest: 0,
        speed: 600,
        easing: 'easeOutQuad',
        cb: null,
        fpDest: false,
    };

    const easings = {
        linear(t) {
            return t;
        },
        easeInQuad(t) {
            return t * t;
        },
        easeOutQuad(t) {
            return t * (2 - t);
        },
        easeInOutQuad(t) {
            return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
        },
        easeInCubic(t) {
            return t * t * t;
        },
        easeOutCubic(t) {
            return (--t) * t * t + 1;
        },
        easeInOutCubic(t) {
            return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
        },
        easeInQuart(t) {
            return t * t * t * t;
        },
        easeOutQuart(t) {
            return 1 - (--t) * t * t * t;
        },
        easeInOutQuart(t) {
            return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
        },
        easeInQuint(t) {
            return t * t * t * t * t;
        },
        easeOutQuint(t) {
            return 1 + (--t) * t * t * t * t;
        },
        easeInOutQuint(t) {
            return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
        }
    };

    function show(){
        if(!isIn) {
            $(opt.selector).addClass('in');
            isIn = true;
        }
    }

    function hide(){
        if(isIn) {
            $(opt.selector).removeClass('in');
            isIn = false;
        }
    }

    function scrollTo(dest, duration, easing, callback){

        opt.dest = dest !== undefined ? dest : opt.dest;
        opt.speed = duration !== undefined ? duration : opt.speed;
        opt.easing = easing !== undefined ? easing : opt.easing;
        opt.cb = callback !== undefined ? callback : opt.cb;

        const start = window.pageYOffset;
        const startTime = 'now' in window.performance ? performance.now() : new Date().getTime();

        const documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
        const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

        let destinationOffset;

        switch(typeof opt.dest){
            case 'number':
                destinationOffset = opt.dest;
                break;
            case 'object':
                destinationOffset = opt.dest.offsetTop;
                break;
            case 'string':
                destinationOffset = document.querySelectorAll(opt.dest)[0];
                break;
            default :
                destinationOffset = opt.dest.offsetTop;
        };

        const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);

        if ('requestAnimationFrame' in window === false) {
            window.scroll(0, destinationOffsetToScroll);
            if (opt.cb) {
                opt.cb();
            }
            return;
        }

        function scroll() {

            if(typeof opt.fpDest !== 'boolean'){
                    $.fn.fullpage.moveTo(opt.fpDest);
                    return;
            }

            const now = 'now' in window.performance ? performance.now() : new Date().getTime();
            const time = Math.min(1, ((now - startTime) / opt.speed));
            const timeFunction = easings[opt.easing](time);
            window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));

            if (window.pageYOffset === destinationOffsetToScroll) {
                if (callback) {
                    callback();
                }
                return;
            }

            requestAnimationFrame(scroll);

        }

        scroll();
    }

    function init(options){

        if(options){
            // Merge default options and passed options
            opt = merge(opt, options);
        }

        let nodesList = document.querySelectorAll(opt.selector);

        Array.prototype.forEach.call(nodesList, function (el, i) {
            el.addEventListener('click', function(){
                scrollTo(opt.dest);
            });
        })
    }

    //API
    return {
        init: init,
        in: show,
        out: hide,
        go: scrollTo,
    }
})();