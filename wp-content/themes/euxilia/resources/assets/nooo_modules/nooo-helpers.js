var h = {
    $body : $('body'),
    $html : $(document.documentElement),
    isHome :  document.querySelector('.home'),
    isBlog: document.querySelector('.blog'),
    header: document.querySelectorAll('.site-header')[0],
    headersHeight: 0,
    scrollPos() {
        return window.scrollY || window.pageYOffseth
    },
    extraClass(el, className){

        let classList = className.split(' ');

        classList.forEach( (singleClass) => {
            if (el.classList){
                el.classList.add(singleClass);
            }else{
                el.className += ' ' + singleClass;
            }
        })
    },
    deleteClass(el, className){
        let classList = className.split(' ');

        classList.forEach((singleClass) => {
            if (el.classList)
                el.classList.remove(singleClass);
            else
                el.className = el.singleClass.replace(new RegExp('(^|\\b)' + singleClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        });
    },
    debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    },
};

h.headersHeight =  h.header.clientHeight;

window.addEventListener('resize', function(){
    h.headersHeight = h.header.clientHeight;
});

export default h;