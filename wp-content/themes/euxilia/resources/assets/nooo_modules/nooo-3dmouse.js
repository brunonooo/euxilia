export default ( function(){
    function debounce(func, wait, immediate) {
        var timeout;

        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    var ticking = false,
        $stage,
        eventCache = null,
        h,
        w,
        pOffset,
        targetIsStage,
        selector = selector || '.platform-3d';

    function zeroToOne(val, max, min) {
        return Math.max(0, Math.min(1, val - min / max - min));
    };


    function moveElements() {
        if (eventCache) {
            var e = eventCache;

            let offsetX = 1 - e.clientX / (pOffset.left + ($stage.width() / 2)),
                offsetY = 1 - e.clientY / (pOffset.top + ($stage.height() / 2)),
                moveFactor = $stage.data("move-factor") || 1,
                scaleTransform = $stage.data("scale"),
                transformStage = "translate3d(0, " + -offsetX * moveFactor + "px, 0) rotateX(" + -offsetY * moveFactor + "deg) rotateY(" + (offsetX * (moveFactor * 2)) + "deg)";

            // --------- APPLY TRANFORM TO POSTER
            $stage.css('transform', transformStage);

            let $layers = $("[class*='el-3d']", $stage);

            // --------- APPLY TRANFORM TO EACH LAYER
            $layers.each(function () {
                let $this = $(this),
                    moveFactor = $this.data('move-factor') || 0,
                    scaleLayer = $this.data('scale') || 0,
                    offsetY = 1 - e.clientY / (pOffset.top + ($stage.height() / 2));

                let scaleX = 1, scaleY = 1;

                if (scaleLayer) {
                    scaleX = ( Math.abs(offsetX) > .1 ? Math.abs(offsetX) : .1 );
                    scaleY = ( Math.abs(offsetY) > .1 ? Math.abs(offsetY) : .1 );
                }

                let translateX = offsetX * moveFactor;
                let translateY =  offsetY * moveFactor * 1.5 ;

                let transformLayer = 'translate3d('+translateX+'px, '+translateY+'px, 0) scale('+scaleX+', '+scaleY+')';

                $this.css("transform", transformLayer);

            });

            ticking = false;
            eventCache = null;
        }
    }

    function start3D(selector = '.platform-3d') {

        targetIsStage = selector == '.platform-3d';
        $stage = $(selector);

        if (!targetIsStage) {
            $stage = $('.platform-3d', $stage);
        }

        w = $stage.width();
        h = $stage.height();
        pOffset = $stage.offset();

        $stage.on("mousemove", function (e) {
            if (!ticking) {
                eventCache = e;
                window.requestAnimationFrame(moveElements);
                ticking = true;
            }

        });

        return this;
    }

    if($(window).width() > 768){
        let threeD = start3D();
    }
})();