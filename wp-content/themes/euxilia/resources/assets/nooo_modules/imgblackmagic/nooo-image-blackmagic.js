/**
 *
 * IMAGE BLACKMAGIC
 *
 */

import merge from 'deepmerge';

export default (function(){

    let instance;

    function init(options) {

        let wWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            wHeight,
            ticking;

        let wScroll = getScrollPos();

        const imgMaxWidth = 1800;

        let opt = {
            wrapper: '.img-wrapper',
            target: '[data-src]',
            dataSource: 'data-src',
            placeholderSize: 50,
            loadOnScroll: true,
            threshold: 1,
            nodesList: []
        }

        function getScrollPos () {
            return window.scrollY || window.pageYOffset
        }

        function getOffset (node) {
            return node.getBoundingClientRect().top + wScroll;
        }

        function inViewport (node) {
            const viewTop = wScroll;
            const viewBot = viewTop + wHeight;

            const nodeTop = getOffset(node);
            const nodeBot = nodeTop + node.offsetHeight;

            const offset = (opt.threshold / 100) * wHeight;

            return (nodeBot >= viewTop - offset) && (nodeTop <= viewBot + offset);
        }

        function updateScroll () {
            wScroll = getScrollPos();
            requestFrame(check);
        }

        function requestFrame (callback) {
            if (!ticking) {
                window.requestAnimationFrame(callback);
                ticking = true
            }
        }

        function swapSrc(node){

            let img = node.querySelector(opt.target);

            if(img){

                // GET LAZY URL
                let lazyUrl = img.getAttribute(opt.dataSource);

                img.src = lazyUrl;

                img.onload = function() {
                    img.removeAttribute(opt.dataSource);
                    autoUpdate();
                };
            }
        }

        function check(){
            wHeight = window.innerHeight;

            var i = 0;

            opt.nodesList.forEach(function(node){
                if(inViewport(node)){
                    swapSrc(node);
                };
            });

            ticking = false
            return this
        }

        function createSrcsetTree(srcset){
            let srcsetList = srcset.split(',');

            let array = [];

            for (var i = 0; i < srcsetList.length; ++i) {

                var srcSetValues = srcsetList[i].trim().split(' ');
                var singleSrcSet = {
                    url     : srcSetValues[0],
                    width   : parseInt(srcSetValues[1].replace('w', '')),
                };

                array.push(singleSrcSet);
            }

            return array;
        }

        function pickBestSrcset(img) {
            let srcsetTree = img.srcsetTree,
                maxSize = img.maxSize;

            for (var i = 0; i < srcsetTree.length; ++i) {

                if (srcsetTree[i].width > wWidth ) {

                    return srcsetTree[i].url;

                }

                if (i + 1 == srcsetTree.length) {

                    if (maxSize < wWidth && srcsetTree[i].width < imgMaxWidth) {

                        return false;

                    } else {

                        return srcsetTree[i].url;

                    }
                }
            }
        }

        function getMaxSize(imgsizes){
            let reversed = imgsizes.split(' ').reverse();
            let last = reversed[0];
            let cleanedVal = last.replace('px', '');
            return Number(cleanedVal);
        }

        function getPlaceholder(img){
            let srcsetTree = img.srcsetTree;

            for (var i = 0; i < srcsetTree.length; ++i) {
                if(srcsetTree[i].width <= opt.placeholderSize){
                    return srcsetTree[i].url;
                }
            }
        }

        function magicLoad(context){
            let sel = context || '.img-wrapper';
            opt.nodesList = document.querySelectorAll(sel);

            Array.prototype.forEach.call(opt.nodesList, function(el, i){
                swapSrc(el);
            });

            ticking = false
            return this
        }

        function mergeOptions(options){
            if(options){
                // Merge default options and passed options
                opt = merge(opt, options);
            }
        }

        function optionsUpdate(options){
            if(options){
                // Merge default options and passed options
                opt = merge(opt, options);
                autoUpdate();
            }
        }

        function autoUpdate(){

            let wrappers = document.querySelectorAll(opt.wrapper);
            opt.nodesList = [];

            Array.prototype.forEach.call(wrappers, function(el, i){

                if(el.querySelector(opt.target)){
                    opt.nodesList.push(el);
                }
            });

            if(window.gc.isDev){
                console.log(opt.nodesList);
            }

        }

        function forceUpdate(){
                autoUpdate();
                prepPlaceholders(opt.nodesList);
                updateScroll();
        }

        function createPlaceholder(el, imgData){
            var placeholderSrc;
            let dataHolder = el.querySelector(opt.target);

            if(opt.placeholderSize){
                placeholderSrc = getPlaceholder(imgData);
            }

            let img = new Image();

            if (img.classList){
                img.classList.add('cover');
            }else{
                img.className += ' ' + 'cover';
            }

            img.setAttribute(opt.dataSource, imgData.src);
            el.insertBefore(img, el.firstChild);

            if(placeholderSrc){
                img.src = placeholderSrc;
            }
        }

        function prepPlaceholders(nodesList){
            Array.prototype.forEach.call(nodesList, function(el, i){

                let dataHolder = el.querySelector(opt.target);

                // GET LAZY URL
                let lazyUrl = dataHolder.getAttribute(opt.dataSource);

                // REMOVE ATTRIBUTE TO REMOVE FROM QUERY LIST
                dataHolder.removeAttribute(opt.dataSource);

                if(lazyUrl){

                    let imgData = {
                        src:        lazyUrl,
                        srcset:     dataHolder.getAttribute('data-srcset'),
                        sizes:      dataHolder.getAttribute('data-sizes'),
                        srcsetTree: [],
                        maxSize:    null,
                        placeholder: null,
                    };

                    //MANAGING SRCSET CASE
                    if (imgData.srcset) {
                        imgData.srcsetTree = createSrcsetTree(imgData.srcset);
                        imgData.maxSize = getMaxSize(imgData.sizes);

                        // Returns false if src is best pick
                        let bestPick = pickBestSrcset(imgData);

                        if(bestPick){
                            imgData.src = bestPick;
                        }
                    }

                    createPlaceholder(el, imgData);
                }
            });
        }

        /**
         * ACTUAL INITIALIZATION CODE
         */
        mergeOptions(options);
        autoUpdate();
        prepPlaceholders(opt.nodesList);


        if(opt.loadOnScroll){
            document.addEventListener('scroll', updateScroll);
            updateScroll();
        }

        return {
            lazyLoad: magicLoad,
            refresh: forceUpdate,
            options: optionsUpdate,
        }
    };

    // API
    return {
        init:  function (options) {
            if (!instance) {
                instance = init(options);
            } else {
                console.log('Hey, no cheating! Only one imageManager per page allowed my friend!')
            }

            return instance;
        }
    };

})();