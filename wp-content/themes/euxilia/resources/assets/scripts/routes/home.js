'use strict';

import $ from 'jquery';
import 'lightslider';

import H from '../../nooo_modules/nooo-helpers';
import ua from '../../nooo_modules/nooo-hound';
import S from '../../nooo_modules/nooo-scroll';
import threeD from '../../nooo_modules/nooo-3dmouse';

export default {
    init() {
        var visualBuilderIsActive = $('body.et_fb').length > 0;

        var moveSeeAllCall = function(context){
            if(!window.gc.isMobile){
                var wrapper = context.parents('.et_pb_section');
                var seeMore = $('.see-more-ls-call', wrapper).detach();
                var currentPager = $('.lSPager', wrapper);
                seeMore.appendTo(currentPager);
            }
        };

        var newsfeed = $(".newsfeed-slider-horizontal"),
            trainingCentersSlider = $('#training-center-slider');

        if(typeof newsfeed != 'undefined'){
            var newsfeedPause = 5000,
                newsfeedSlider = newsfeed.lightSlider({
                    item: 1,
                    autoWidth: false,
                    slideMove: 1, // slidemove will be 1 if loop is true
                    slideMargin: 50,

                    pause:newsfeedPause,
                    speed: 1000,
                    keyPress: true,
                    controls: false,
                    auto:false,

                    pager: true,
                    enableTouch:true,
                    enableDrag:true,
                    freeMove:true,
                    swipeThreshold: 40,

                    responsive : [
                        {
                            breakpoint:768,
                            settings: {
                                item:1,
                                slideMove:1,
                                pause: 2000,
                                auto:true,
                            }
                        },
                    ],
                    onSliderLoad: function ($el) {
                        moveSeeAllCall($el);

                        if(ua.isMobile || $(window).width() < 768){
                            $('li:last-child', $el).remove();
                            newsfeedSlider.refresh();
                        }

                    },
                    onBeforeSlide: function ($el) {
                        var slides = $('.lslide', $el),
                            goingToIndex = $el.getCurrentSlideCount() - 1,
                            comingFromIndex = slides.index($('.lslide.active', $el));

                        if(goingToIndex != comingFromIndex){
                            slides.removeClass('passed');
                            $(slides[goingToIndex]).prevAll('.lslide').addClass('passed');
                        }

                        if(goingToIndex >= slides.length - 3 && !ua.isMobile){
                            newsfeedSlider.pause();
                        }
                    },
                });

            newsfeed.on('dragstart', function(e){
                $(this).addClass('dragging');
            });
            newsfeed.on('dragend', function(e){
                $(this).removeClass('dragging');
            });

            $('#back-to-start').on('click', function(){
                newsfeed.goToSlide(0);
            });
        }

        if(typeof trainingCentersSlider !== 'undefined'){
            var slidesNum = 4,
                uploadedSlides = trainingCentersSlider.attr('data-slidesnum');

            if(uploadedSlides < 4 && !ua.isMobile){
                slidesNum = uploadedSlides;
            }

            var sliderOptions = {
                item: slidesNum,
                autoWidth: false,
                slideMove:2,
                slideMargin: 50,
                auto:true,
                loop:true,

                speed: 700,
                keyPress: true,
                controls: false,

                pager: true,
                enableTouch:true,
                enableDrag:true,
                freeMove:false,
                swipeThreshold: 40,

                responsive : [
                    {
                        breakpoint:768,
                        settings: {
                            item:1,
                            slideMove:1,
                            slideMargin:30,
                            loop:true,
                            pause: 2000
                        }
                    },
                ],
                onSliderLoad: function (el) {
                    moveSeeAllCall(el);
                },
            };

            var trainingCentersSliderIntance = trainingCentersSlider.lightSlider(sliderOptions);
        }

        if(ua.isMobile){
            var pressSliderOptions = {
                item: 1,
                autoWidth: false,
                slideMove:1,
                slideMargin: 50,
                auto:true,
                loop:true,

                pause:2000,
                speed: 700,
                keyPress: true,
                controls: false,

                pager: true,
                enableTouch:true,
                enableDrag:false,
                freeMove:false,
                swipeThreshold: 40,
            };

            $('#auxiell-two-col-grid').lightSlider(pressSliderOptions);
        }

        if(!visualBuilderIsActive){
            if(!window.gc.isMobile && $(window).width() > 767) {

                var sections = $('.section'),
                    currentSection = '',
                    navActive = '',
                    fpHasLoaded= false,
                    scrollDelay = 50,
                    fpState = {
                        'index': null,
                        'next': null,
                        'direction': null,
                        'waitingToScroll': false,
                        'timeout' : null,
                        'hasScrolled': false,
                        'autoScroll': true,
                    },
                    lastKnownScroll = $(window).scrollTop();


                $(document).on('scroll', function(){

                    var scrollTop = $(window).scrollTop(),
                        scrollBottom = scrollTop + $(window).height(),
                        scrollingUp = lastKnownScroll > scrollTop;

                    if(!fpState.autoScroll && scrollingUp){

                        if( scrollBottom <  lastSectionTop ){
                            H.$body.removeClass('free-scrolling hide-sidebar-and-logo');

                            $.fn.fullpage.setAutoScrolling(true);
                            fpState.autoScroll = true;

                        }
                    }

                    lastKnownScroll = $(window).scrollTop();
                });

                var ccc = 0;
                $('.site-main').fullpage({
                    verticalCentered: false,
                    hybrid: true,
                    navigation: true,
                    navigationPosition: 'left',
                    fitToSection: false,
                    onLeave: function(index, next, direction) {
                        if (!fpState.hasScrolled) {

                            if (direction == 'up') {
                                H.$body.addClass('scrolling-up');
                            } else {
                                H.$body.addClass('scrolling-down');
                            }

                            // ------ VIEWING CLASS FOR ANIMATIONS
                            H.$body.removeClass('viewing-section');

                            fpState.hasScrolled = true;
                        }


                        // ----- DELAYED STUFF
                        clearTimeout(fpState.timeout);

                        fpState.timeout = setTimeout(function(){

                            fpState.waitingToScroll = true;

                            $.fn.fullpage.moveTo(next);

                            fpState.hasScrolled = false;


                        }, scrollDelay);

                        return fpState.waitingToScroll;
                    },
                    afterLoad: function(anchorLink, index) {
                        initCustomStuff(fpHasLoaded);

                        // ------------ SCROLL HAS FINISHED
                        fpState.waitingToScroll = false;

                        var currentSprites = currentSection.find('.sprite-icon');

                        currentSprites.each(function () {
                            launchSpriteAnimation($(this));
                        });

                        // ------ VIEWING CLASS FOR ANIMATIONS
                        H.$body.addClass('viewing-section');
                    },
                    afterRender: function(){
                        initCustomStuff(fpHasLoaded);

                        // ------ VIEWING CLASS FOR ANIMATIONS
                        H.$body.addClass('viewing-section');
                    },

                });

                function initCustomStuff(firstLoad){
                    // ----------------- FIRST LOAD SETUP
                    if(!firstLoad){
                        currentSection = $('.section.active');
                        fpHasLoaded = true;
                        $('.mockup .ion-chevron-down').click(function(){
                            $.fn.fullpage.moveSectionDown();
                        });
                    }//----
                }
            }
        }



        function mobileMediaListener(mediaQuery) {
            if (mediaQuery.matches || window.gc.isMobile) {

            }
        }

        function smMinMediaListener(mediaQuery) {
            if (mediaQuery.matches || window.gc.isMobile) {

                $(window).scroll(function(){
                    let footerOffset = $('#colophon').offset().top;
                    let scroll = $(window).scrollTop();
                    let wHalfHeight = $(window).height() * .85;

                    if( footerOffset < wHalfHeight+ scroll ){
                        H.extraClass(document.documentElement, 'hide-nav');
                    }else {
                        H.deleteClass(document.documentElement, 'hide-nav');
                    }
                });

            }
        }

        function mdMinMediaListener(mediaQuery) {
            if (mediaQuery.matches  && !window.gc.isMobile) {

            }
        }

        /**
         *
         * MEDIA QUERIES
         *
         *
         */
        var mobileMediaCheck = window.matchMedia('(max-width: 767px)');
        mobileMediaCheck.addListener(mobileMediaListener);

        mobileMediaListener(mobileMediaCheck);

        var smMinMediaCheck = window.matchMedia('(min-width: 991px)');
        smMinMediaCheck.addListener(smMinMediaListener);

        smMinMediaListener(smMinMediaCheck);

        var mdMinMediaCheck = window.matchMedia('(min-width: 992px)');
        mdMinMediaCheck.addListener(mdMinMediaListener);

        mdMinMediaListener(mdMinMediaCheck);
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
