'use strict';

import $ from 'jquery';
import objectFitImages from 'object-fit-images';
import Modernizr from 'modernizr';
import 'imagesloaded';
import ImageMage from '../../nooo_modules/imgblackmagic/nooo-image-blackmagic';
import Popper from 'popper.js';

import { TweenMax, TimelineMax } from 'gsap';

import H from '../../nooo_modules/nooo-helpers';
import ua from '../../nooo_modules/nooo-hound';
import S from '../../nooo_modules/nooo-scroll';


/**
 * IMPORTING AUXIELL SCRIPTS
 */
import '../auxiell-scripts';

export default {
    init() {

        if(ua.isRetina){
            H.$body.addClass('hdpi');
        }

        function mobileMediaListener(mediaQuery) {
            if (mediaQuery.matches || window.gc.isMobile) {

            }
        }

        function smMinMediaListener(mediaQuery){
            if (mediaQuery.matches && !window.gc.isMobile) {


            }

            if(mediaQuery.matches && window.gc.isMobile) {

            }
        }

        //START IMAGE LOADING!
        ImageMage.init();

        //TUTORIAL MODULE
        let $tut = $('.tutorial'),
            $clickable = $('.clickable-area'),
            $poppers = $('[data-step-id]'),
            $scrollableContainer = $('.resonant-path-scheme .et_pb_code_inner');

        function closeThePopper(el){
            if( el.$current.attr('data-popped') == 'yes' ){
                $poppers.hide();
                el.$current.attr('data-popped', null);
            } else {
                $poppers.hide();
                el.$current.attr('data-popped', null);

                $(el.popper).show();
                el.$current.attr('data-popped', 'yes');
                el.ppr.update();
            }
        }

        function closeThePopper(el){
            $poppers.hide();
            el.$current.attr('data-popped', null);
        }

        function openThePopper(el){
            $poppers.hide();
            el.$current.attr('data-popped', null);

            $(el.popper).show();
            el.$current.attr('data-popped', 'yes');

            el.ppr.update();
        }

        function initPoppers() {
            $clickable.each(function(){
                let $current = $(this);
                let ref = $current.attr('id');
                let popper = document.querySelector('[data-step-id="'+ref+'"]');

                if(popper){
                    let ppr = new Popper(this, popper, {
                        placement: 'top',
                        modifiers: {
                            flip: {
                                behavior: ['top', 'bottom'],
                            },
                            preventOverflow: {
                                enabled: true,
                            },
                        }
                    });

                    let popperEl = {
                        $current : $current,
                        popper : popper,
                        ppr: ppr
                    };

                    if(!ua.isMobile){
                        $current.mouseenter(function(){
                            openThePopper(popperEl);
                        });

                        $current.mouseleave(function(){
                            closeThePopper(popperEl);
                        });
                    }else {
                        $current.click(function(){
                            if( $current.attr('data-popped') == 'yes' ){
                                closeThePopper(popperEl);
                            } else {
                                openThePopper(popperEl);
                            }
                        });
                    }
                }
            });
        }

        /*if($tut.length && ua.isMobile){
            $scrollableContainer.addClass('no-scroll');
            $poppers.hide();

            $tut.click(function(){
                $scrollableContainer.removeClass('no-scroll');
                $tut.addClass('disappear');
                initPoppers();
            });
        } else {*/
            $poppers.hide();
            initPoppers();
/*        }*/

        /**
         *
         * EVENT CONSTRUCTOR POLYFILL ( IE 9 up )
         *
         **/

        (function () {

            if ( typeof window.CustomEvent === "function" ) return false;

            function CustomEvent ( event, params ) {
                params = params || { bubbles: false, cancelable: false, detail: undefined };
                var evt = document.createEvent( 'CustomEvent' );
                evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
                return evt;
            }

            CustomEvent.prototype = window.Event.prototype;

            window.Event = CustomEvent;
        })();

        /**
         *
         * Simulated fullheight link
         *
         */

        $('.simulated-link-target').click( function (e){
            e.stopPropagation();
            e.preventDefault();

            var targetBlank = $(this).find('.simulated-link-router').attr('target') == '_blank';
            var route = $(this).find('.simulated-link-router').attr('href');

            if( targetBlank  || e.metaKey ) {
                window.open( route, '_blank' );
            } else {
                window.location = route;
            }

        });

        function flyIn($el, offset){
            $el.each(function () {
                if (($(window).scrollTop() + $(window).height() - offset) > $(this).offset().top) {
                    $(this).addClass('fly-in');
                }
            });
        }

        function flyInAnimations($appearOffset){
            if($('.flight-ready').length) {

                // ----- set appear offset from the bottom of the window
                if( typeof $appearOffset != 'undefined'){
                    $appearOffset = $appearOffset
                }else{
                    $appearOffset =  300
                };

                // ---------- FLY IN WHAT NEEDS TO BE IN
                flyIn($('.flight-ready'), 1);

                //---- FLY IN ON SCROLL
                $(document).scroll(function () {
                    flyIn($('.flight-ready'), $appearOffset);
                });
            }
        }

        $('html').imagesLoaded(function(){

            H.$body.css({
                'overflow': 'inherit',
            });

            H.$body.removeClass('page-loading');

            // DISPLAY NONE OF LOADER FOR SEO
            window.setTimeout(function(){
                $('preloader').addClass('killed');
            }, 1500);

                window.setTimeout(function(){
                    flyInAnimations( 300 );
                }, 100);
        });

        /**
         *
         * Cool Android form effect
         *
         */

        var $inputs = $('.input-wrapper input, .input-wrapper textarea');
        $inputs.focus(function(){
            $(this).parents('.input-wrapper').addClass('filled');
        });

        $inputs.blur(function(){
            if( $(this).val() === '' ){
                $(this).parents('.input-wrapper').removeClass('filled');
            }
        });

        /**
         *
         * FORM SUBMISSION
         *
         */

        var ajax = null;
        var $form = $('.form-wrapper');
        var stopAjax = function(){
            $form.removeClass('doing-ajax');
        };
        var customValidation = function(event){
            $('.input-wrapper').each(function(){

                if($('.wpcf7-not-valid', this ).length ){
                    $(this).addClass('not-valid');
                }else{
                    $(this).removeClass('not-valid');
                }
            });

            stopAjax();
        }

        $('.wpcf7-form .btn').click(function () {
            $(this).siblings('.wpcf7-submit').click();

            $form = $(this).parents('.form-wrapper');
            $form.addClass('doing-ajax');
        });

        $('.wpcf7').on('wpcf7:invalid', customValidation);

        $('.wpcf7').on('wpcf7:spam', stopAjax);

        $('.wpcf7').on('wpcf7:mailsent', customValidation);

        $('.wpcf7').on('wpcf7:mailfailed', stopAjax);

        $('.wpcf7').on('wpcf7:submit', stopAjax);


        /**
         * MENU TOGGLE
         */

        var Menu = {
            $header: $('.site-header'),
            $toggle: $('#menu-toggle'),
            $overlay: $('.overlay-menu'),
            $items: $('.menu-item'),
            isOpen: false,
        };

        function menuOpen(){
            Menu.$header.addClass('menu-open');
            H.$body.addClass('scroll-block');


            if(window.gc.fpEnabled) {
                $.fn.fullpage.setAllowScrolling(false);
                $.fn.fullpage.setKeyboardScrolling(false);
            }

            Menu.isOpen = true;
        }

        function menuClose(){

            Menu.$header.removeClass('menu-open');
            H.$body.removeClass('scroll-block');

            if( window.gc.fpEnabled ){
                $.fn.fullpage.setAllowScrolling(true);
                $.fn.fullpage.setKeyboardScrolling(true);
            }

            Menu.isOpen = false;
        }

        Menu.$toggle.on('click', function(){
            console.log(Menu);
            if(Menu.isOpen){
                menuClose();
            }else{
                menuOpen();
            }
        });

        Menu.$items.on('click', function(){
            menuClose();
        });

        if($('#popup-wrapper').length){

            $.fn.popper = function() {
                // -------- popup elements
                var popupWrapper = $('#popup-wrapper'),
                    popup = $('#popup'),
                    targetImg = $('.popup-img', popupWrapper ),
                    targetTitle = $('[data-target="title"]', popupWrapper),
                    targetContent = $('[data-target="content"]', popupWrapper),
                    tempIconClasses = '';

                // ----------------- POPUP BEHAVIOUR

                // ----------------- MOVE WRAPPER TO BODY
                popupWrapper.detach().appendTo($('body'));

                $('.popper').on('click', function(){

                    var trigger = $(this),
                        popupIcon = trigger.data('popup-icon'),
                        popupTitle = trigger.data('popup-title'),
                        popupContent = trigger.data('popup-content');


                    targetImg.html(popupIcon);
                    targetTitle.html(popupTitle);
                    targetContent.html(popupContent);

                    $('body').addClass('no-scroll overlayed');
                    window.requestAnimationFrame(openPopup);

                    setTimeout(function(){
                        targetImg.addClass('fly-in');
                    }, 200);
                });

                $('.full-overlay').on('click', function(){
                    closePopup();
                });

                $('.btn-close', popup).on('click', function(){
                    closePopup();
                });

                function openPopup(){
                    popupWrapper.addClass('popup-open');
                }

                function closePopup(){
                    if(popupWrapper.hasClass('popup-open')){
                        popupWrapper.removeClass('popup-open');
                    }
                    targetImg.removeClass('fly-in');

                    $('body').removeClass('no-scroll overlayed');

                    var stateObj = { popup: 'closed' };
                    history.pushState(stateObj, "popup-closed", '');
                }
            }

            /**
             * POPUP MANAGER
             */

            $.fn.popper();
        }

        /**
         *
         * HEXAGON GRAPHICS ANIMATION
         */
        let hGraph = $('#hexagons');

        if(hGraph.length) {

            let cb = $('#center-big'),
                cs = $('#center-small'),
                rigthy = $('.righty', hGraph),
                lefty = $('.lefty', hGraph);



            let tl = new TimelineMax();

            tl.to(cs, 1, { scale: 1, ease: Power4.easeOut})
                .to(cb, 1, { scale: 1, ease: Power4.easeOut}, '200');

        }


        /**
         *
         * Media queries
         *
         */

        function mobileMediaListener(mediaQuery) {
            if (mediaQuery.matches || window.gc.isMobile) {
                if(!window.gc.fpEnabled){

                }
            }
        }

        function smMaxMediaListener(mediaQuery) {
            if (mediaQuery.matches || window.gc.isMobile) {
                if(H.isHome){
                    menuClose();
                }
            }
        }

        function mdMinMediaListener(mediaQuery) {
            if (mediaQuery.matches  && !window.gc.isMobile) {
                if(H.isHome){
                    menuClose();
                }
            }
        }

        /**
         *
         * MEDIA QUERIES
         *
         *
         */
        var mobileMediaCheck = window.matchMedia('(max-width: 767px)');
        mobileMediaCheck.addListener(mobileMediaListener);

        mobileMediaListener(mobileMediaCheck);

        var smMaxMediaCheck = window.matchMedia('(max-width: 991px)');
        smMaxMediaCheck.addListener(smMaxMediaListener);

        smMaxMediaListener(smMaxMediaCheck);

        var mdMinMediaCheck = window.matchMedia('(min-width: 992px)');
        mdMinMediaCheck.addListener(mdMinMediaListener);

        mdMinMediaListener(mdMinMediaCheck);
    },
    finalize() {

        /**
         *
         * WINDOW RESIZE EVENT MANAHEMENT
         *
         **/

        var rtime,
            timeout = false,
            delta = 200,
            rszend = new CustomEvent('rszend');


        $(window).resize(function() {
            rtime = new Date();
            if (timeout === false) {
                timeout = true;
                setTimeout(resizeend, delta);
            }
        });

        function resizeend() {
            if (new Date() - rtime < delta) {
                setTimeout(resizeend, delta);
            } else {
                timeout = false;
                window.dispatchEvent(rszend);
            }
        }

        /**
         *
         * FLEXBOX FALLBACK
         *
         **/

        function flexRefresh() {

            var $flexContainer = $('.flex');

            $flexContainer.each(function(){


                var $flexChildren = $(this).children(),
                    tallestHeight = 0;

                $flexChildren.removeAttr('style');

                $flexChildren.each(function(){

                    var currentHeight = $(this).height();

                    if(currentHeight > tallestHeight){
                        tallestHeight = currentHeight;
                    }

                });

                $flexChildren.height(tallestHeight);

            })
        }

        if(!Modernizr.flexbox){
            flexRefresh();
            window.addEventListener('rszend', flexRefresh);

            flexRefresh();
        }

        /**
         * BACK TO TOP BUTTON
         */


        if( !window.gc.fpEnabled ){
            S.init({
                dest: 0,
                fpDest: false,
            })
        }

        document.addEventListener('scroll', function(e){
            if( !window.gc.fpEnabled && H.scrollPos() > window.innerHeight){
                S.in();
            }

            if(H.scrollPos() < window.innerHeight){
                S.out();
            }
        });

        /**
         * TIMELINE ANIMATION
         */

        var timeWrapper = $('.time-wrapper');

        if(timeWrapper.length && !ua.isMobile){

            var s = skrollr.init({
                'forceHeight':false,
            });

            // ---- blocking time-flow to flow-back
            var timeWrapper = $('.time-wrapper');

            $(document).scroll(function () {
                if ($(window).scrollTop() + $(window).height() - 200 > timeWrapper.offset().top + timeWrapper.height()) {
                    timeWrapper.addClass('time-has-flown');
                }
            });
        }

        /**
         * OBJECT FIT POLYFILL
         */
        objectFitImages();
    },
};
