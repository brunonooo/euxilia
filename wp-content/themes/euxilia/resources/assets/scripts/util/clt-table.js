(function(window, $) {
    'use strict';

    $.fn.cltTable = function(options) {

        // ------- TABLE element
        var el = this,
            tableHeight = el.height(),
            firstInit = true,

            // ------- headers of rows
            rowElement = $('.row-container'),
            rowHeader = $('.row-header'),
            lastScrollPosition = window.scrollY,

            // ------- affixed header element
            affixedHeader = $('.table-affixed'),
            affHeaderHeight = affixedHeader.height(),

            // -------- PROGRESS BARS AND MARKERS
            progressBars = $('.progress-bar'),
            progressMarker = $('.progress-marker'),
            stepsNumber = 0,
            singleStep = $('.open').outerHeight(true),
            singleHalfStep = $('.row-container.closed').height(),
            fullProgress = 0,
            offset = 0,
            anyRowOpen = $('.row-container.open').length > 0,
            currentPosition = 0,
            baseProgress = 0,
            currentProgress = 0,
            newProgress = 0,
            hasStepped = false,

            // -------- POPUP ELEMENTS
            popupWrapper = $('#popup-wrapper'),
            popup = $('#popup-clt'),
            targetIcon = $('[data-target="icon"]', popupWrapper),
            targetTitle = $('[data-target="title"]', popupWrapper),
            targetAbstract = $('[data-target="abstract"]', popupWrapper),
            targetContent = $('[data-target="content"]', popupWrapper),
            tempIconClasses= '',

            // -------- ACCORDEON BEHAVIOUR
            // -------- AFFIX AND SCROLL



            openRow = function($trigger){
                var target = $trigger.data('affects');
                $(target).toggleClass('closed');
                $(target).toggleClass('open');
                return $(target);
            },

            refreshTableSizes = function(){
                affHeaderHeight = affixedHeader.height();
                tableHeight = el.height();
            },

            initProgressBars = function(){
                offset = singleStep / stepsNumber;
                fullProgress = singleStep * stepsNumber;
                baseProgress = - fullProgress + (offset/2);
                currentProgress = - fullProgress + (offset/2);
                progressBars.css({
                    height: fullProgress,
                    transform: 'translateY(-'+offset+'px)'
                });
            },

            initprogressMarker = function(){
                progressMarker.css({
                    height: fullProgress,
                    transform: 'translateY('+currentProgress+'px)'
                });
            },

            countFoldableRows = function(){
                var count = 0;
                rowElement.each(function () {
                    $(this).attr('data-rownum', count);
                    ++stepsNumber;
                    ++count;
                });
            },

            initPlugin = function(){
                countFoldableRows();
                initProgressBars();
                initprogressMarker();
                firstInit = false;
            },

            moveMarker = function(isLastRow, isFirstRow, closing){
                var rowsOpened = -1,
                    rowsClosed = 0,
                    invertedRowList = rowElement.get().reverse(),
                    invertedIndex = invertedRowList.length - 1,
                    foundLastOpenedRow = false;

                $(invertedRowList).each(function(){
                    var currentRow = $(this);

                    if(!foundLastOpenedRow && currentRow.hasClass('open')){
                        currentRow.addClass('last-opened');
                    }

                    if(currentRow.hasClass('open')){
                        foundLastOpenedRow = true;
                    }

                    if(foundLastOpenedRow){
                        if(currentRow.hasClass('open')){
                            ++rowsOpened;
                        }

                        if(currentRow.hasClass('closed')){
                            ++rowsClosed;
                        }
                    }
                    --invertedIndex;
                });

                var openedProgress = rowsOpened * singleStep,
                    closedProgress = rowsClosed * singleHalfStep;
                newProgress = baseProgress + openedProgress + closedProgress;

                if(isLastRow){
                    if(!closing){
                        newProgress = 0;
                    }
                }

                if(isFirstRow){
                    progressBars.css({
                        'transform' : 'translateY(-'+offset+'px)'
                    });

                    if(closing){
                        var middleClosedRowHeight = - singleHalfStep / 2;
                        progressBars.css({
                            'transform' : 'translateY('+middleClosedRowHeight+'px)'
                        });
                    }
                }


                window.requestAnimationFrame(paintProgress)

                hasStepped = true;
                currentProgress = newProgress;
            },

            paintProgress = function(){
                progressMarker.css({
                    transform: 'translateY('+newProgress+'px)'
                });
            },

            stepForward = function(){
                var newProgress = currentProgress + singleStep;

                progressMarker.css({
                    transform: 'translateY('+newProgress+'px)'
                });

                hasStepped = true;
                currentProgress = newProgress;
            },

            stepBackwards = function(){
                var newProgress = currentProgress - singleStep;

                progressMarker.css({
                    transform: 'translateY('+newProgress+'px)'
                });

                hasStepped = true;
                currentProgress = newProgress;
            };

        initPlugin();


        $(window).on('resize', function(){
            refreshTableSizes();
            initProgressBars();
        });

        rowHeader.on('click', function(){
            var currentTrigger = $('a', $(this)),
                collapsibleTarget = $(currentTrigger.data('target')),
                rowAffected = openRow(currentTrigger),
                isLastRow = false,
                isFirstRow = false;

            rowElement.removeClass('last-opened');


            if(rowAffected.hasClass('first-row')){
                isFirstRow = true;
            }

            if(rowAffected.hasClass('last-row')){
                isLastRow = true;
            }

            if(!rowAffected.hasClass('open')){
                moveMarker(isLastRow, isFirstRow, true);
                collapsibleTarget.collapse('hide');
                collapsibleTarget.on('hidden.bs.collapse', function(){
                    if(!hasStepped){
                        refreshTableSizes();
                    }
                });
                hasStepped = false;
            }else{
                moveMarker(isLastRow, isFirstRow, false);
                collapsibleTarget.collapse('show');
                collapsibleTarget.on('shown.bs.collapse	', function(){
                    if(!hasStepped){
                        refreshTableSizes();
                    }
                });
                hasStepped = false;
            }
        });

        // ----------------- POPUP BEHAVIOUR

        // ----------------- MOVE WRAPPER TO BODY
        popupWrapper.detach().appendTo($('body'));

        $('.icon-btn', el).on('click', function(){
            $('.popup-icon').removeClass('hidden');

            var trigger = $(this),
                popupTitle = trigger.data('popup-title');
            tempIconClasses = trigger.data('popup-icon');

            targetIcon.addClass(tempIconClasses);

            $('.popup-svg').html('');
            targetTitle.html(popupTitle);
            targetAbstract.html(trigger.data('popup-abstract'));
            targetContent.html(trigger.data('popup-content'));

            $('body').addClass('no-scroll overlayed');
            window.requestAnimationFrame(openPopup);

            setTimeout(function(){
                $('.popup-icon').addClass('fly-in');
            }, 200);
        });

        $('.full-overlay').on('click', function(){
            closePopup();
        });

        $('.btn-close', popup).on('click', function(){
            closePopup();
        });

        function openPopup(){
            popupWrapper.addClass('popup-open');
        }

        function closePopup(){
            if(popupWrapper.hasClass('popup-open')){
                popupWrapper.removeClass('popup-open');
            }
            $('.popup-icon').removeClass('fly-in');

            $('body').removeClass('no-scroll overlayed');
            setTimeout(function(){
                $('.popup-icon').removeClass(tempIconClasses);
            }, 500);

            var stateObj = { popup: 'closed' };
            history.pushState(stateObj, "popup-closed", '');
        }

        // -------- SVG POPUP ELEMENTS
        var popupWrapper = $('#popup-wrapper'),
            svgDropZone = $('.popup-svg');

        // ------------- 4D E SPDCA POPUP

        $('.see-more-btn').on('click', function(){
            var context = $(this).parents('.col-inner'),
                popupSvg = context.find('.svg-wrapper'),
                popupCopy = context.find('.copy-svg').html();

            // ------ REPLACE POPUP CONTENT
            $('.popup-icon').addClass('hidden');
            $('.popup-title').html('');
            svgDropZone.html('');
            popupSvg.clone().removeClass('fly-in').appendTo(svgDropZone);

            $('.popup-content').html(popupCopy);

            $('body').addClass('no-scroll overlayed');

            window.history.pushState({ popup: 'open'}, "see-more-popup", '');
            console.log(window.history.state);

            window.requestAnimationFrame(openPopup);

            setTimeout(function(){
                $('.svg-wrapper', popupWrapper).addClass('fly-in');
            }, 500);
        });

        // ------------------ AFFIX TABLE ON LOAD
        if(window.scrollY > el.offset().top && window.scrollY < el.offset().top + tableHeight){
            affixedHeader.show();
        }

        window.onpopstate = function(event) {
            var check = !window.history.state == null || !window.history.state == false;
            console.log(check);

            if( check ){

                if(window.history.state.popup != 'closed'){
                    closePopup();
                }
            }
        };

        // ------------------ AFFIX el ON SCROLL
        $(window).on('scroll', function(){
            var scrollingDown = lastScrollPosition < window.scrollY,
                scrollingUp = lastScrollPosition > window.scrollY;

            if(scrollingDown){
                if(scrollY > el.offset().top){

                    affixedHeader.show();

                    if(scrollY > (el.offset().top + tableHeight - affHeaderHeight)){
                        affixedHeader.addClass('rest-bottom');
                    }
                }
            }

            if(scrollingUp){

                if(scrollY < el.offset().top){

                    affixedHeader.hide();
                }

                if(scrollY > el.offset().top) {
                    if (scrollY < (el.offset().top + tableHeight - affHeaderHeight)) {
                        affixedHeader.removeClass('rest-bottom');
                    }
                }
            }

            lastScrollPosition = window.scrollY;
        });

    }
})(window, jQuery);
