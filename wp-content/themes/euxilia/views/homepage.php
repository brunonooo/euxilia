<?php
/*
 * Template Name: HOMEPAGE
 */
get_header();
get_template_part( 'template-parts/navigation/navigation', 'left' ); ?>

<div id="main" class="site-main" role="main">
    <?php
    while ( have_posts() ) : the_post();?>
        <?php the_content(); ?>
    <?php endwhile; // End of the loop.?>
    <?php get_footer();?>
</div><!-- #main -->
</div><!--ANIMSITION-->
