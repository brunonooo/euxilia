##Installazione di tutte le dipendenze:

npm install

bower install

gulp bootstrap-sass


##Generazione del file modernizr-custom:

gulp modernizr


##Generazione favicons per tutti i dispositivi:

*Sostituire il file ‘master-picture.png’ presente in:

‘assets/images/icons/‘

con la propria favicon (un file .png ad alta risoluzione: ad esempio 1000px x 1000px)*

gulp generate-favicon


##Generazione automatica dei links alle favicons nel tag head:

gulp inject-favicon-markups


##Iniziare a lavorare:

aprire 'gulpfile.js' e sostituire l'indirizzo 'localhost:8888/base-project' (task: browserSync) con l'url del proprio progetto

gulp
