<div class="top-bar">
    <div class="top-bar-inner">
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <div class="navbar-brand-inner">
                <span class="auxiell-icon logo">
                    <?php echo file_get_contents( get_stylesheet_directory_uri().'/resources/assets/vectors/logo-euxilia.svg'); ?>
                </span>
            </div>
        </a>
    </div>
</div>
