<div class="right-bar">
    <button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar icon-bar-top"></span>
        <span class="icon-bar icon-bar-middle"></span>
        <span class="icon-bar icon-bar-bottom"></span>
        <span class="label">MENU</span>
    </button>
</div>

<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="top-menu">
    <div class="vertical">
        <div class="vertical-middle">
            <?php wp_nav_menu( array(
                'theme_location' => 'header-menu',
                'menu_id'        => 'header-menu',
                'container'      => false
            ) ); ?>
        </div>
    </div>

    <!--<ul id="langs">
        <?php /*pll_the_languages(
            array(
                'display_names_as' => 'slug',
            )
        ); */?>
    </ul>-->
</nav><!-- #site-navigation -->
