<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/site.webmanifest">
<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/safari-pinned-tab.svg" color="#2a54a1">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-config" content="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon/browserconfig.xml">
<meta name="theme-color" content="#2a54a1"> 

