<div id="popup-wrapper">
    <div id="popup">
        <span class="btn-close ion-ios-close-empty"></span>
        <div class="popup-inner scroll-y">
            <div class="vertical">
                <div class="vertical-middle">
                    <span data-target="icon" class="popup-img"></span>
                    <h4 data-target="title" class="popup-title">POPUP</h4>
                    <div data-target="content" class="popup-content">
                        <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo. Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo</p>
                        <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo. Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo</p>
                    </div>

                    <a class="et_pb_more_button et_pb_button" href="<?php echo fetchTranslatedLink('contatti'); ?>"><?php pll_e('contattaci', 'edxiting'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>