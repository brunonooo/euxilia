<?php get_header(); ?>

    <main id="main" class="site-main" role="main">

        <?php
        while ( have_posts() ) : the_post(); ?>
            <div id="welcome-single-post" class="et_pb_section et_pb_section_video welcome-section welcome-post et_pb_section_0 et_pb_with_background et_section_regular">
                <div class="img-wrapper">
                    <?php the_post_thumbnail($id, 'full', array(
                        'class' => 'seo-img'
                    )); ?>
                    <div class="overlay-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
                </div>
                <div class="post-intro-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-offset-1 col-xs-10">
                                <div class="vertical">
                                    <div class="vertical-bottom">
                                        <!--<span class="post-date"><?php /*the_date('j F Y'); */?></span>-->
                                        <h1><?php the_title();?></h1>
                                        <?php

                                        $catList =  get_the_category();
                                        $catTags = '<div class="tags-wrapper">';
                                        $filtersList = ['eventi', 'evidenza', 'news', 'press'];

                                        foreach($catList as $singleCat ){
                                            $currentName = $singleCat->slug;

                                            if(array_search($currentName, $filtersList) !== false) {
                                                $catTags .= sprintf('
                                                <span class="cat-tag">%1$s</span>
                                                 ', $singleCat->name);
                                            }
                                        }


                                        echo $catTags.'</div>' ;

                                        ?>

                                        <?php $videoRef = get_field('video_id'); if(!empty($videoRef)) : ?>
                                            <a class="et_pb_button btn-std btn-white btn-play et_pb_play_video_button" href="#watch-the-video"><?php pll_e('GUARDA IL VIDEO'); ?></a>

                                            <div id="welcome-video" class="et_pb_code et_pb_module yt-video et_pb_code_0">
                                                  <div id="yt-player" data-ytvideo-id="<?php echo $videoRef;?>"></div><span class="ion-ios-close-empty close"></span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php the_content(); ?>
        <?php endwhile; ?>

    </main><!-- #main -->

<?php get_footer();
