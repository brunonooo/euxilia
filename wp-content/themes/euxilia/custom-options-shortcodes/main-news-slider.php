<?php

// ----------------------- MAIN NEWS SLIDER SHORTCODE

//usage: [euxilia_slider_news lang="lang-value"]
function sliderNews_func( $atts ) {

    $atts = shortcode_atts( array(
        'lang'    => 'it',
    ), $atts );


    $args = array(
        'post_type'         => 'post',
        'category_name'     => 'in-evidenza',
    );

    $mainNews = new WP_Query($args);
    $output = '';

    if($mainNews->have_posts()) {
        $output = '<ul id="main-news-slider">';

        while ($mainNews->have_posts()){
            $mainNews->the_post();
            $id = get_the_ID();
            $thumb = get_the_post_thumbnail($id, 'full', array(
                'class' => 'seo-img'
            ));

            $thumbUrl = get_the_post_thumbnail_url();

            // SET DEFAULT IMAGE IN CASE THERE IS NO THUMB SET
            if(empty($thumb)){
                $thumb = wp_get_attachment_image(1569, 'full', false, array(
                    'class' => 'seo-img'
                ));
                $thumbSrc =  wp_get_attachment_image_src(1569, 'full');
                $thumbUrl =  $thumbSrc[0];
            }

            $cat = get_the_category_list(' ');
            $title = get_the_title();
            $excerpt = custom_get_excerpt(200, "content");
            /*$excerpt = get_the_excerpt();*/
            $date = get_the_date('j F Y');
            $link = get_the_permalink();


            $output .= sprintf('
            <li class="single-main-news">
                 <div class="img-wrapper">
                     %1$s
                    <div class="overlay-image" style="background-image: url(%2$s)"></div>
                </div>
                <!--<span class="single-main-cat">%3$s</span>-->
                <div class="vertical article-meta">
                    <div class="vertical-middle">
                    <div class="meta-inner">
                        <span class="main-news-date">%4$s</span>
                        <h3>%5$s</h3>
                        <p>%6$s</p>
                        <a href="%7$s" class="btn-std btn- btn-opacity">%8$s</a>
                        </div>
                    </div>
                </div>
            </li>
            ',
                $thumb, //1
                $thumbUrl, //2
                $cat, //3
                $date, //4
                $title, //5
                $excerpt, //6
                $link, //7
                pll__('Scopri di più')); //7
        }// ---- END WHILE
        $output .= '</ul>';
    }// ---END IF

    return $output;
}
add_shortcode( 'euxilia_slider_news', 'sliderNews_func' );