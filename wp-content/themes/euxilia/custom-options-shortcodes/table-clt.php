<?php
// EXTRA STUFF

$colorClass = array(
    'bg-scan movable',
    'bg-scan',
    'bg-plan',
    'bg-do movable',
    'bg-do movable',
    'bg-do',
    'bg-check',
    'bg-act movable',
    'bg-act movable',
);

// ------------- MAPPING CLT TABLE POPUPS array( ROW => array(COL => array => ALL VALUES) )
$tableMap = array();

if( have_rows('popups', $id) ){
    while ( have_rows('popups', $id) ) {
        the_row();
        $rowNum = get_sub_field('row_pos_popup');
        $colNum = get_sub_field('col_pos_popup');
        $icon = get_sub_field('icon_classes');
        $title = get_sub_field('title_popup');
        $abstract = get_sub_field('abstract_popup');
        $content = get_sub_field('content_popup');

        $tableMap[$rowNum][$colNum] = array(
            'icon' => $icon,
            'title' => $title,
            'abstract' => $abstract,
            'content' => $content,
        );
    }
}

function get_cols_markup( $rowNum, $tableMap, $colorClass){

    $row = $rowNum;
    $in = '';
    $itemClass = '';
    $rowPopups = $tableMap[$row];

    switch($rowNum){
        case(1):
            $itemClass = 'del-item';
            $in = 'in';
            break;
        case(2):
            $itemClass = 'dev-item';
            break;
        case(3):
            $itemClass = 'dem-item';
            break;
        case(4):
            $itemClass = 'dep-item';
            break;
    }

    for ($col = 0; $col < 9; $col++) {

        $popup = '';

        if(!empty($rowPopups[$col+1])){
            $current = $rowPopups[$col+1];
            $icon = ( $current['icon'] ) ? $current['icon'] : '' ;
            $title = $current['title'];
            $abstract = $current['abstract'];
            $content = $current['content'];

            $popup = sprintf('
                    <a class="ion-information-circled icon-btn" data-popup-icon="%1$s" data-popup-title="%2$s" data-popup-abstract="%3$s" data-popup-content=\'%4$s\'></a>',
                $icon,
                $title,
                $abstract,
                $content
            );
        }

        echo sprintf('
            <td>
                    <div class="collapse %6$s cell-item %5$s %1$s">
                    <div class="cell-inner">
                        <span class="et-pb-icon %2$s"></span>
                        <p>%3$s</p>
                        %4$s
                    </div>
                </div>
            </td>',
            $colorClass[$col],
            $icon,
            $title,
            $popup,
            $itemClass,
            $in
        );
    };
}
?>

<div class="table-affixed">
    <table>
        <tr>
            <td class="trans-cell"></td>
            <th colspan="2" scope="col"><?php pll_e('SCAN') ?></th>
            <th colspan="1" scope="col"><?php pll_e('PLAN') ?></th>
            <th colspan="3" scope="col"><?php pll_e('DO') ?></th>
            <th colspan="1" scope="col"><?php pll_e('CHECK') ?></th>
            <th colspan="2" scope="col"><?php pll_e('ACT') ?></th>
        </tr>
    </table>
</div>

<table class="responsive table-clt">
    <thead>
    <tr>
        <td class="trans-cell"></td>
        <th colspan="2" scope="col"><?php pll_e('SCAN') ?></th>
        <th colspan="1" scope="col"><?php pll_e('PLAN') ?></th>
        <th colspan="3" scope="col"><?php pll_e('DO') ?></th>
        <th colspan="1" scope="col"><?php pll_e('CHECK') ?></th>
        <th colspan="2" scope="col"><?php pll_e('ACT') ?></th>
    </tr>
    </thead>
    <tbody>
    <tr id="delivery" class="row-container open last-opened first-row">
        <td class="row-header">
            <a data-toggle="collapse" data-target=".del-item" data-affects="#delivery"><?php pll_e('DELIVERY'); ?></a>
        </td>
        <?php get_cols_markup(1, $tableMap, $colorClass); ?>
    </tr>

    <tr>
        <td></td>
        <td>
            <div class="progress-wrapper">
                <div class="progress-bar">
                    <span class="progress-marker"></span>
                </div>
            </div>
        </td>
        <td></td>
        <td></td>
        <td>
            <div class="progress-wrapper">
                <div class="progress-bar">
                    <span class="progress-marker"></span>
                </div>
            </div>
        </td>
        <td>
            <div class="progress-wrapper">
                <div class="progress-bar">
                    <span class="progress-marker"></span>
                </div>
            </div>
        </td>
        <td></td>
        <td></td>
        <td>
            <div class="progress-wrapper">
                <div class="progress-bar">
                    <span class="progress-marker"></span>
                </div>
            </div>
        </td>
        <td>
            <div class="progress-wrapper">
                <div class="progress-bar">
                    <span class="progress-marker"></span>
                </div>
            </div>
        </td>
    </tr>

    <tr id="development" class="row-container closed">
        <td class="row-header">
            <a data-toggle="collapse" data-target=".dev-item" data-affects="#development"><?php pll_e('DEVELOPMENT') ?></a>
        </td>
        <?php get_cols_markup(2, $tableMap, $colorClass); ?>
    </tr>

    <tr id="demand" class="row-container closed">
        <td class="row-header">
            <a data-toggle="collapse" data-target=".dem-item" data-affects="#demand"><?php pll_e('DEMAND') ?></a>
        </td>
        <?php get_cols_markup(3, $tableMap, $colorClass); ?>
    </tr>

    <tr id="deploy" class="row-container closed last-row">
        <td class="row-header">
            <a data-toggle="collapse" data-target=".dep-item" data-affects="#deploy"><?php pll_e('DEPLOYMENT') ?></a>
        </td>
        <?php get_cols_markup(4, $tableMap, $colorClass)?>
    </tr>
    </tbody>
</table>


<div id="popup-wrapper">
    <div id="popup-clt">
        <span class="btn-close ion-ios-close-empty"></span>
        <div class="popup-inner scroll-y">
            <div class="vertical">
                <div class="vertical-middle">
                    <div class="popup-svg"></div>
                    <span data-target="icon" class="popup-icon"></span>
                    <h4 data-target="title" class="popup-title">COMPLETE LEAN TRANSFORMATION</h4>
                    <div data-target="content" class="popup-content">
                        <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo. Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo</p>
                        <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo. Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
