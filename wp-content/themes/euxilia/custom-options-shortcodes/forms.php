<?php
/**
 * EDXITING CUSTOM OPTION PAGES
 **/

//usage: [edxiting_contact_form lang="lang-value"]
function tt_func( $atts ) {

    $atts = shortcode_atts( array(
        'lang'    => 'it',
        'version' => 'default',
    ), $atts );

    $id = '';
    $tooltipsModule = '';

    switch($atts['lang']){
        case 'en':
            $id = 'edxiting-tooltips-en-id';
            $tooltipsModule = do_shortcode('[contact-form-7 id="2049" title="Contact form EN"]');
            break;
        default:
            $id = 'edxiting-tooltips-it-id';
            $tooltipsModule = do_shortcode('[contact-form-7 id="377" title="Contact form IT"]');
            break;
    }

    $tooltipsModule .= sprintf(
        '<div id="tooltips-content"> 
    <span data-tooltip="1">%1$s</span> 
    <span data-tooltip="2">%2$s</span> 
    <span data-tooltip="3">%3$s</span> 
    <span data-tooltip="4">%4$s</span> 
</div>',
        get_field('tooltip1', $id),
        get_field('tooltip2', $id),
        get_field('tooltip3', $id),
        get_field('tooltip4', $id)
    );

    return $tooltipsModule;
}
add_shortcode( 'edxiting_contact_form', 'tt_func' );