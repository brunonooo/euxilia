<?php

//usage: [euxilia_examples]
function examples_grid_shortcode( ) {

    $examples = get_posts(array(
        'post_type' => 'examples-'.THEME_SLUG,
        'posts_per_page' => 2,
    ));

    $result = '';

    if(!empty($examples)) {

        foreach($examples as $example) {
            setup_postdata($example);

            $result.= sprintf('
            <div class="example-box">
            <a href="%4$s">
            <div class="example-inner">
                <div class="img-wrapper three-two">
                    %1$s
                </div>
                
                <div class="example-meta">
                   
                        <h3>%2$s</h3>
                        <p>%3$s</p>
                   
                </div>
                </div>
            </a>
            </div>
            ',
                get_seo_img($example->ID),
                get_the_title($example->ID),
                get_the_excerpt(),
                get_the_permalink()
            );

            wp_reset_postdata();
        }

    }else {
        $result = 'Nothing found';
    }

    return $result;
}

add_shortcode( 'euxilia_examples', 'examples_grid_shortcode' );
