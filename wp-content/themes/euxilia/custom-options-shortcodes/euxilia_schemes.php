<?php

/**
 * @return string
 */
function get_resonance_scheme(){
    $scheme = file_get_contents(get_stylesheet_directory_uri().'/resources/assets/vectors/x-resonance.svg');

    $tut = sprintf('
                    <div class="tutorial">
                        <div class="tutorial-inner">
                            <div class="single-tut">
                                <span class="click">
                                <span class="click-icon-wrap">
                                    <span class="click-echo"></span>
                                    <span class="click-tap"></span>
                                    </span>
                                    %1$s
                                </span>
                            </div>
                            <div class="single-tut">
                                <span class="slide">
                                <span class="slide-icon-wrap">
                                    <span class="slide-echo"></span>
                                    <span class="slide-tap"></span>
                                </span>
                                   %2$s
                                </span>
                            </div>
                    
                            <div class="button-wrapper">
                                <span class="et_pb_button">%3$s</span>
                            </div>
                        </div>
                    </div>',
        __('clicca per maggiori info', THEME_CONTEXT),
        __('scorri per scoprire il percorso', THEME_CONTEXT),
        __('inizia', THEME_CONTEXT));

    $poppers = sprintf('
            <div class="popper" data-step-id="%1$s" role="tooltip">
            <span x-arrow></span>
            <span class="popper-text">
            %2$s
            </span>
            </div>
            <div class="popper" data-step-id="%3$s" role="tooltip">
            <span x-arrow></span>
            <span class="popper-text">
            %4$s
            </span>
            </div>
            <div class="popper" data-step-id="%5$s" role="tooltip">
            <span x-arrow></span>
            <span class="popper-text">
            %6$s
            </span>
            </div>
            <div class="popper" data-step-id="%7$s" role="tooltip">
            <span x-arrow></span>
            <span class="popper-text">
            %8$s
            </span>
            </div>
            <div class="popper" data-step-id="%9$s" role="tooltip">
            <span x-arrow></span>
            <span class="popper-text">
            %10$s
            </span>
            </div>
            ',
        'scan',
        get_field('tt_scan', pll_get_post(4079)),
        'plan',
        get_field('tt_plan', pll_get_post(4079)),
        'do',
        get_field('tt_do', pll_get_post(4079)),
        'check',
        get_field('tt_check', pll_get_post(4079)),
        'act',
        get_field('tt_act', pll_get_post(4079)));

    $resScheme = '<div class="res-scheme-wrapper">'.$scheme.$tut.$poppers.'</div>';
    return $resScheme;
}
//usage: [euxilia_scheme type="empty"]
function scheme_shortcode( $atts ) {

    $atts = shortcode_atts( array( 'type' => 'share' ), $atts );
    $type = $atts['type'];
    $tPath = get_stylesheet_directory();

    $schemes = array(
        'x-resonance' => get_resonance_scheme(),
        'right-x' => file_get_contents($tPath.'/resources/assets/vectors/right-x.svg'),
        'empty' => file_get_contents($tPath.'/resources/assets/vectors/ripple.svg'),
        'grey' => file_get_contents($tPath.'/resources/assets/vectors/ripple-grey.svg'),
        'half'  => file_get_contents($tPath.'/resources/assets/vectors/half-ripple.svg'),
        'full'  => file_get_contents($tPath.'/resources/assets/vectors/full-ripple.svg'),
        'bg'  => file_get_contents($tPath.'/resources/assets/vectors/bg.svg'),
        'contesto1'  => file_get_contents($tPath.'/resources/assets/vectors/ripple-context1.svg'),
        'contesto2'  => file_get_contents($tPath.'/resources/assets/vectors/ripple-context2.svg'),
        'contesto3'  => file_get_contents($tPath.'/resources/assets/vectors/ripple-context3.svg'),
        'decisionale1'  => file_get_contents($tPath.'/resources/assets/vectors/ripple-descisionale1.svg'),
        'decisionale2'  => file_get_contents($tPath.'/resources/assets/vectors/ripple-descisionale2.svg'),
        'decisionale3'  => file_get_contents($tPath.'/resources/assets/vectors/ripple-descisionale3.svg'),
    );

    return $schemes[$type];
}

add_shortcode( 'euxilia_scheme', 'scheme_shortcode' );
