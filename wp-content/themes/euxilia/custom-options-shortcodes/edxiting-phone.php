<?php

//usage: [edxiting_phone]
function example_phone_shortcode( ) {
    $result = '';

    $exampleIcon = get_field('example_logo');
    $exampleStores = get_field('example_stores');
    $phone = '<img class="phone" src="'.asset_path('images/phone.png').'" alt="Edixiting Phone">';

    if(!empty($exampleStores)){
        $stores = '<ul class="not-a-list stores">';
        foreach($exampleStores as $es){
            if( $es == 'apple') {
                $stores .= '<li class="store"><a href="'.get_field('apple_store').'"><img src="'.asset_path('vectors/apple.svg').'" alt="Apple Store"></li>';
            }else if( $es == 'gplay' ){
                $stores .= '<li class="store"><a href="'.get_field('g_store').'"><img src="'.asset_path('vectors/google-play.svg').'" alt="Google Play"></a></li>';
            }
        }
        $stores .= '</ul>';
    }else {
        $stores = '';
    }

    $result .= sprintf('<div class="example">
                <div class="example-wrapper">
                %1$s
                <div class="example-inner">
                <span class="example-icon">
                <img src="%2$s" alt="%3$s icon">
                </span>
                <h2 class="example-name">%3$s</h2>
                %4$s
                </div>
                </div>
            </div>',
        $phone,
        $exampleIcon,
        get_the_title(),
        $stores

    );



    return $result;
}

add_shortcode( 'edxiting_phone', 'example_phone_shortcode' );
