<?php

/**
 * YOUTUBE VIDEOS SHORTCODE
 **/

//usage: [edxiting_ytvideo id="youtubeid" name="custom_name"]
// NB! use custom name in case there are more videos and remember to use the same hash in the button href!
function edxiting_ytvideo_func( $atts ) {

// ---------------- DEFAULT ATTS FALLBACK
    $atts = shortcode_atts( array(
        'id' => '1PQYrMZKlAk',
        'name'      => 'default-video'
    ), $atts );


    $videoId = $atts['id'];
    $videoName = $atts['name'];

    $ytVideoMarkup = sprintf('<div id="yt-player-%2$s" class="yt-player" data-ytvideo-id="%1$s" data-ytvideo-name="%2$s"></div>',
        $videoId,
        $videoName);

    return $ytVideoMarkup;
}

add_shortcode( 'edxiting_ytvideo', 'edxiting_ytvideo_func' );
