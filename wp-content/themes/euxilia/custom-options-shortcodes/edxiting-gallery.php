<?php

//usage: [edxiting_gallery type="how_it_works"]
function example_gal_shortcode( $atts ) {
    $atts = shortcode_atts( array(
        'lang'    => 'it',
        'type' => 'how_it_works',
    ), $atts );

    switch($atts['type']){
        case 'what_it_looks_like':
            $gallery = get_field('gallery_how_it_looks');
            break;
        default:
            $gallery = get_field('gallery_how_it_works');
            break;
    }

    $result = '';

    if(!empty($gallery)) {
        $result .= '<ul class="gallery not-a-list">';
        $count = 1;
        foreach ($gallery as $img) {

            $result .= sprintf('<li class="gallery-el">
<span class="gallery-number">%2$s</span>
<div class="img-wrapper">
%1$s
</div>
</li>',
                wp_get_attachment_image( $img['ID'], 'large'),
                $count);


            ++$count;
        }
        $result .= '</ul>';
    }



    return $result;
}

add_shortcode( 'edxiting_gallery', 'example_gal_shortcode' );
