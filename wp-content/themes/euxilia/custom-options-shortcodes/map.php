<?php
/**
 * EDXITING CUSTOM OPTION PAGES
 **/

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> __(THEME_NAME.' Maps', THEME_CONTEXT),
        'menu_title'	=> __(THEME_NAME.' Maps', THEME_CONTEXT),
        'menu_slug' 	=> THEME_SLUG.'-maps',
        'position'      => '28.8',
        'icon_url'      => 'dashicons-location-alt',
        'redirect'      => true
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> __(THEME_NAME.' Maps', THEME_CONTEXT),
        'menu_title'	=> __(THEME_NAME.' Maps', THEME_CONTEXT),
        'menu_slug' 	=> THEME_SLUG.'-maps-it',
        'parent_slug' 	=> THEME_SLUG.'-maps',
        'post_id'       => THEME_SLUG.'-maps-it-id'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> __(THEME_NAME.' Maps', THEME_CONTEXT),
        'menu_title'	=> __(THEME_NAME.' Maps', THEME_CONTEXT),
        'menu_slug' 	=> THEME_SLUG.'-maps-en',
        'parent_slug' 	=> THEME_SLUG.'-maps',
        'post_id'       => THEME_SLUG.'-maps-en-id'
    ));

}

//usage: [edxiting_map lang="it"]
function map_func( $atts ) {

    $atts = shortcode_atts( array(
        'lang'    => 'it',
        'version' => 'default',
    ), $atts );

    $id = '';

    switch($atts['lang']){
        case 'en':
            $id = THEME_SLUG.'-maps-en-id';
            break;
        default:
            $id = THEME_SLUG.'-maps-it-id';
            break;
    }

    $map = '';
    $zoom = ( !empty(get_field("zoom", $id)) ? get_field("zoom", $id) : '');

    if( have_rows('locations', $id) ) {

        $map .= '<img class="acf-map-zoom-overlay" src=""><div class="acf-map" data-zoom="'.$zoom.'" data-icon="'.get_bloginfo('stylesheet_directory').'/assets/images/gmarker.png">';

        while ( have_rows('locations', $id) ) {
            the_row();

            $location = get_sub_field('marker_pos');
            $lat = $location['lat'];
            $lng = $location['lng'];
            $title = get_sub_field('marker_t');
            $address = $location['address'];
            $description = get_sub_field('marker_d');

            $map .= sprintf('
            <div class="marker" data-lat="%1$s" data-lng="%2$s">
                <h4>%3$s</h4>
                <p class="address">%4$s</p>
                <p>%5$s</p>
            </div>',
                $lat,
                $lng,
                $title,
                $address,
                $description);
        }
        $map .= '</div>';
    }
    return $map;
}

add_shortcode( 'edxiting_map', 'map_func' );
