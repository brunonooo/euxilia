<?php

/**
 * NEWS GRID AND FILTERS SHORTCODE
 */

//usage: [edxiting_news_grid lang="lang-value"]
function filteredGrid_func( $atts ) {

    $atts = shortcode_atts( array(
        'lang'    => 'it',
    ), $atts );


    $allFilter =  pll__('Tutti gli articoli');

    // PICKING CATS
    $blogCategories = get_categories(array(
        'meta_key'   => 'show_in_blog',
        'meta_value' => true,
    ));


    // ----- START FILTERS
    $filters = sprintf('
        <div class="filters-wrapper">
            <a href="#%2$s" class="filter selected" data-cat-filter="cat-all">%1$s</a>',
        $allFilter,
        str_replace(' ', '-' , strtolower($allFilter)));

    // CREATING A LIST OF CATEGORY ID TO QUERY POSTS
    $catsInIDs = array();
    // CREATING A LIST OF CATEGORY SLUG TO FILTER FRONTEND TAGS
    $filtersInSlug = array();

    foreach ($blogCategories as $bCat){
        $catName = $bCat->name;
        $catSlug = $bCat->slug;
        $filtersInSlug[] = $catSlug;
        $catsInIDs[] = $bCat->term_id;
        $acfId = 'category_'.$bCat->term_id;
        $visibleLabel = get_field('show_label', $acfId );

        if( $visibleLabel !== false){
            $filters .= sprintf('
            <a href="#%2$s" class="filter" data-cat-filter="cat-%2$s">%1$s</a>
    ',
                $catName,
                $catSlug);
        }
    }

    $filters .= sprintf( '</div>' );

        $output = !empty($blogCategories) ? $filters : '';
    // ----- END FILTERS

    $args = array(
        'post_type'         => 'post',
        'lang'              => $atts['lang'],
    );

    if(!empty($blogCategories)){
        $args['category__in'] = $catsInIDs;
    }

    $news = new WP_Query($args);


    if($news->have_posts()) {
        $output .= '<div id="news-grid-wrapper" class="container-fluid"><div id="news-grid" class="row grid">';

        while ($news->have_posts()){
            $news->the_post();
            $id = get_the_ID();
            $thumb = get_the_post_thumbnail($id, 'full', array(
                'class' => 'seo-img'
            ));
            $thumbUrl = get_the_post_thumbnail_url();

            // SET DEFAULT IMAGE IN CASE THERE IS NO THUMB SET
            if(empty($thumb)){
                $thumb = wp_get_attachment_image(1569, 'full', false, array(
                    'class' => 'seo-img'
                ));
                $thumbSrc =  wp_get_attachment_image_src(1569, 'full');
                $thumbUrl =  $thumbSrc[0];
            }

            $catList =  get_the_category();
            $prefix = 'cat-';
            $cat = '';
            $catTags = '';

            foreach( $catList as $singleCat ){
                $currentName = $singleCat->slug;

                if(array_search($currentName, $filtersInSlug) !== false ){

                    $cat .= $prefix.$currentName.' ';

                    $catTags .= sprintf('
                     <span class="cat-tag">%1$s</span>
                        ',
                        $singleCat->name);

                }
            }

            $title = custom_get_title(80);
            $abstract = custom_get_excerpt(300, "content");
            /*$abstract = get_field('abstract');*/
            $date = get_the_date('j F Y');
            $link = get_the_permalink();


            $output .= sprintf('
            <div class="col-sm-6 col-md-4 grid-item cat-all %8$sis-visible">
                <div class="grid-item-inner">
                    <div class="img-wrapper">
                        <span class="tags-wrapper">%9$s</span>
                        %1$s
                        <div class="overlay-image" style="background-image: url(%2$s)"></div>
                    </div>
                    <div class="vertical article-meta">

                        <span class="news-date">%4$s</span>
                        <h3>%5$s</h3>
                        <p>%6$s</p>
                        <a href="%7$s" class="btn-std btn-discover">%10$s</a>

                    </div>
                </div>
            </div>
            ',
                $thumb,//1
                $thumbUrl,//2
                $cat,//3
                $date,//4
                $title,//5
                $abstract,//6
                $link,//7
                $cat,//8
                $catTags,
                pll__('Scopri di più'));//9
        }// ---- END WHILE
        $output .= '</div></div>';
    }// ---END IF

    return $output;
}
add_shortcode( 'edxiting_news_grid', 'filteredGrid_func' );
