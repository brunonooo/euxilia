<?php

//usage: [euxilia_examples_feed]
function examples_feed_shortcode( ) {

    $examples = get_posts(array(
        'post_type' => 'examples-'.THEME_SLUG,
        'posts_per_page' => -1,
    ));

    $result = '<div id="examples-feed">';

    if(!empty($examples)) {

        foreach($examples as $example) {
            setup_postdata($example);

            $colStyle = get_field('example_style', $example->ID) != 'Blocco';
            $classes = $colStyle ? 'flex flex-center example-cols' : 'example-block';
            $imgSize = $colStyle ? 'three-two' : 'sixteen-nine';
            $img = get_seo_img($example->ID);
            $title = get_the_title($example->ID);
            $content = apply_filters('the_content',get_the_content($example->ID));


            $result .= '<div class="example-context '.$classes.'">
<div class="example-row">
            <div class="example-col example-col-img">
                <div class="img-wrapper '.$imgSize.'">
                    '.$img.'
                </div>
            </div>
    
            <div class="example-col example-col-content">    
                 <div class="example-content">
                     <h3>'.$title.'</h3>
                     '.$content.'
                 </div>
             </div>
             </div>
            </div>';


            wp_reset_postdata();
        }

    }else {
        $result = 'Nothing found';
    }

    $result .= '</div>';

    return $result;
}

add_shortcode( 'euxilia_examples_feed', 'examples_feed_shortcode' );
?>