<?php
/**
 * EDXITING CUSTOM OPTION JOBS PAGE
 **/

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> __(THEME_NAME.'Jobs', 'edxiting'),
        'menu_title'	=> __(THEME_NAME.' Job Applications', 'edxiting'),
        'menu_slug' 	=> 'edxiting-jobs',
        'position'      => '28.8',
        'icon_url'      => 'dashicons-megaphone',
        'redirect'      => true
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> __('Italian Job Applications', 'edxiting'),
        'menu_title'	=> __('Italian Job Applications', 'edxiting'),
        'menu_slug' 	=> 'edxiting-jobs-it',
        'parent_slug' 	=> 'edxiting-jobs',
        'post_id'       => 'edxiting-jobs-it-id'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> __('English Job Applications', 'edxiting'),
        'menu_title'	=> __('English Job Applications', 'edxiting'),
        'menu_slug' 	=> 'edxiting-jobs-en',
        'parent_slug' 	=> 'edxiting-jobs',
        'post_id'       => 'edxiting-jobs-en-id'
    ));
}

/**
 * TOOLTIPS SHORTCODES
 **/

function makeMachineFriendly($string) {
    $string = strtolower($string);
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    $string = preg_replace("/[\s-]+/", " ", $string);
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

//usage: [edxiting_jobs lang="it"]
function jobs_func( $atts ) {

    $atts = shortcode_atts( array(
        'lang'    => 'it',
        'version' => 'default',
    ), $atts );

    $id = '';

    switch($atts['lang']){
        case 'it':
            $id = 'edxiting-jobs-it-id';
            $formTemplate = do_shortcode('[contact-form-7 id="991" title="JOB Contact Form Template"]');
            break;
        case 'en':
            $id = 'edxiting-jobs-en-id';
            $formTemplate = do_shortcode('[contact-form-7 id="2294" title="JOB Contact Form Template EN"]');
            break;
    }

    $jobsNum = count(get_field('jobs', $id));
    if( $jobsNum == 1 ){
        $columnClasses = 'col-xs-12 col-xs-offset-0 col-md-offset-2 col-md-8';
    }else {
        $columnClasses = 'col-sm-6';
    }

    if( have_rows('jobs', $id) ) {

        $jobsRequests = '';

        while ( have_rows('jobs', $id) ) {
            the_row();

            $title = get_sub_field('job_t');
            $description = get_sub_field('job_d');
            $longDescription = get_sub_field('job_d_long');
            $slug = makeMachineFriendly($title);
            $buttonText = pll__('Invia candidatura');

            $jobsRequests .= sprintf('
            <div class="%1$s single-job">
                <div id="%4$s" class="job-trigger-wrapper">
                    <div class="job-meta">
                        <h4>%2$s</h4>
                        %3$s
                        <div class="long-description">%6$s</div>
                    </div>
                    <a href="#form-%4$s" class="btn-std btn-outline job-form-activator">%5$s</a>
                </div>
            </div>',
                $columnClasses,
                $title,
                $description,
                $slug,
                $buttonText,
                $longDescription);
        }

        $jobs = sprintf('<div id="job-modal">
                             <div class="job-meta">
                             </div>
                             <div class="job-form-container">
                             <h4>%3$s</h4>
                             %2$s
                            </div>
                         </div>
                         <div class="container">
                             <div class="row">
                              %1$s
                             </div>
                           </div>
                         </div>',
            $jobsRequests,
            $formTemplate,
            pll__('PROPONITI COME CANDIDATO'));
    }
    return $jobs;
}

add_shortcode( 'edxiting_jobs', 'jobs_func' );
