<?php

/**
 * TIMELINE SHORTCODES
 **/

//usage: [edxiting_timeline]
function moments_func() {

    $moments = get_field('moments');

    if( have_rows('moments') ) {

        $moments = '<div class="time-wrapper"><span data-300-center-top="transform:translate3d(0,-100%,0)" data-300-center-bottom="transform:translate3d(0,0%,0);" class="time-flow"></span></div><div class="container"><div class="row">';
        $momentsCount = 0;

        while ( have_rows('moments') ) {
            the_row();

            $type = get_sub_field('moment_type');
            $title = get_sub_field('moment_t');
            $description = get_sub_field('moment_d');
            $img = get_sub_field('icona');
            $icon = '<img src=\''.$img['url'].'\' alt=\''.$img['alt'].'\'>';

            if($momentsCount % 2 === 0) {
                $position = 'col-xs-12 col-sm-offset-6 col-sm-6 moment-right';

                $moments .= sprintf('
               <div class="%1$s moment %2$s flight-ready">
               <div class="moment-inner vertical">
               <div class="timeline-graphics-wrapper vertical-middle">
                    <span class="pin"></span>
                    <span class="line"></span>
                    <span class="extra-line"></span>
               </div>
               <div class="moment-meta vertical-middle">
               <h4>%3$s</h4>
               %4$s
               <button class="et_pb_more_button et_pb_button popper" data-popup-icon="%4$s" data-popup-title="%3$s" data-popup-content="%6$s">%5$s</button>    
               </div>
               </div>
            </div>',
                    $type,
                    $position,
                    $title,
                    $icon,
                    __('Scopri di più', 'edxiting'),
                    $description);

            }else {
                $position = 'col-xs-12 col-sm-6 moment-left';

                $moments .= sprintf('
               <div class="%1$s moment %2$s flight-ready">
               <div class="moment-inner vertical">
               <div class="moment-meta vertical-middle">
               <h4>%3$s</h4>
               %4$s
               <button class="et_pb_more_button et_pb_button popper" data-popup-icon="%4$s" data-popup-title="%3$s" data-popup-content="%6$s">%5$s</button>    
               </div>
               <div class="timeline-graphics-wrapper vertical-middle">
                    <span class="pin"></span>
                    <span class="line"></span>
                    <span class="extra-line"></span>
               </div>
               </div>
            </div>',
                    $type,
                    $position,
                    $title,
                    $icon,
                    __('Scopri di più', 'Edxiting'),
                    $description);
            }

            ++$momentsCount;
        }
    }

    $moments .= '</div></div>'; // ----closing wrapper row
    return $moments;
}

add_shortcode( 'edxiting_timeline', 'moments_func' );
