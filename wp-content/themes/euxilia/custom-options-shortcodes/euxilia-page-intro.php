<?php

//usage: [euxilia_intro]
function intro_shortcode( ) {

    return file_get_contents(get_stylesheet_directory_uri().'/resources/assets/vectors/intro-svg.svg');
}

add_shortcode( 'euxilia_intro', 'intro_shortcode' );
