<?php
/**
 * SOCIAL ICONS SHORTCODES
 **/

//usage: [social_icon type="social-site" url="link"]
function social_func( $atts ) {

    // EMPTY ATTS FALLBACK
    $atts = shortcode_atts( array(
        'type'    => 'facebook',
        'url' => 'https://www.facebook.com/edxiting/',
    ), $atts );

    $iconClass = 'ion-social-facebook';

    // CHECKING IF INPUT IS VALID STUFF ( FACEBOOK IS DEFAULT )
    switch($atts['type']){
        case 'linkedin' :
            $iconClass = 'ion-social-linkedin';
            break;
        case 'twitter' :
            $iconClass = 'ion-social-twitter';
            break;
        case 'youtube' :
            $iconClass = 'ion-social-youtube';
            break;
        case 'google+' :
            $iconClass = 'ion-social-googleplus';
            break;
        default :
            $iconClass = 'ion-social-facebook';
            break;

    }

    $socialLink = sprintf(
        '<a href="%2$s" class="%1$s social-icon" target="_blank"></a>',
        $iconClass,
        $atts['url']
    );

    return $socialLink;
}
add_shortcode( 'social_icon', 'social_func' );
