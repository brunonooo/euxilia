<?php

/**
 * THEME CONSTS
 */
define('THEME_NAME', 'euxilia');
define('THEME_CONTEXT', 'euxilia');
define('THEME_SLUG', 'euxilia');

/**
 *  DISABLING WORDPRESS XML-RPC for security reasons
 **/
add_filter('xmlrpc_enabled', '__return_false');

/**
 * DISABLING META GENERATOR
 **/
remove_action('wp_head', 'wp_generator');

/**
 * DISABLING QUERY STRINGS FROM ASSETS
 **/
function remove_query_string_from_assets( $url ) {
    return remove_query_arg( 'ver', $url );
}

add_filter( 'style_loader_src', 'remove_query_string_from_assets' );

/**
 * SUPPORT FOR TITLE AND METAS
 **/
add_theme_support( 'title-tag' );

/**
 * SUPPORT FOR EXCERPT FOR PAGES
 **/
add_post_type_support( 'page', 'excerpt');

/**
 * LAZY LOAD PLACEHOLDER
 */
add_image_size( 'll-placeholder', 50, false );


/**
 * REMOVING TITLE ATTR
 **/
function removeTitleAttr( $menu ){
    return $menu = preg_replace('/ title=\"(.*?)\"/', '', $menu );
}

add_filter( 'wp_nav_menu', 'removeTitleAttr' );


/**
 * THEME SETUP
 **/
function support_setup() {
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
}

add_action( 'after_setup_theme', 'support_setup' );

/**
 * SET MAX SRCSET IMG WIDTH AND REMOVE FULLSIZE
 */
add_image_size('xs', 768);
add_image_size('sm', 900);
add_image_size('lg', 1280);
add_image_size( 'max-size', 1800, 900);

add_filter( 'wp_calculate_image_srcset', 'dq_add_custom_image_srcset', 10, 5 );

function dq_add_custom_image_srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ){

    $maxSize = 1920; // CHANGE THIS TO SET MAX SIZE

    // image base name
    $image_basename = wp_basename( $image_meta['file'] );
    // upload directory info array
    $upload_dir_info_arr = wp_get_upload_dir();
    // base url of upload directory
    $baseurl = $upload_dir_info_arr['baseurl'];

    // Uploads are (or have been) in year/month sub-directories.
    if ( $image_basename !== $image_meta['file'] ) {
        $dirname = dirname( $image_meta['file'] );

        if ( $dirname !== '.' ) {
            $image_baseurl = trailingslashit( $baseurl ) . $dirname;
        }
    }

    $image_baseurl = trailingslashit( $image_baseurl );

    foreach( $sources as $sourceKey => $sourceValue ){
        $key = intval($sourceKey);

        if($key > $maxSize){
            unset($sources[$sourceKey]);
        }
    }

    //return sources with new srcset value
    return $sources;
}