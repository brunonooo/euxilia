<?php

/**
 * EUXILIA CUSTOM OPTION PAGES
 **/

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' => _('Opzioni '.THEME_NAME),
        'menu_title' => _('Opzioni '.THEME_NAME),
        'menu_slug' => THEME_SLUG.'-options',
        'redirect' 		=> true
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> _('Condivisione Social'),
        'menu_title' 	=> _('Condivisione Social'),
        'parent_slug' 	=> THEME_SLUG.'-options',
    ));

}
