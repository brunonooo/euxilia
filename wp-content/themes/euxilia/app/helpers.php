<?php

/**
 * SAGE PATHS HELPER
 */
function asset_path($asset) {
    $uriStylesheet = get_stylesheet_directory_uri().'/dist';
    $dirStylesheet = __DIR__.'/../dist/assets.json';

    $manifest = new Man_JsonManifest( $dirStylesheet, $uriStylesheet );
    return $manifest->getUri($asset);
}


/**
 * ------ ADD ASYNC AND DEFER ATTRIBUTES
 */
function add_defer_attribute($tag, $handle) {
    $scripts_to_defer = array('gmaps');

    foreach($scripts_to_defer as $defer_script) {
        if ($defer_script === $handle) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
    }
    return $tag;
}

function add_async_attribute($tag, $handle) {
    $scripts_to_async = array('gmaps');

    foreach($scripts_to_async as $async_script) {
        if ($async_script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }
    return $tag;
}

add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


/**
 * ----------------------  IMAGES MANAGEMENT HELPERS
 */

/**
 * ------ GET SEO IMAGE FOR IMG WRAPPERS
 * ( works well with image blackmagic js script )
 */
function get_seo_img( $id=false, $size='large' , $lazy = true){
    if(!empty($id)){
        $currentPost = get_post($id);
    }else{
        $currentPost = get_post(get_the_ID());
    }

    $alt = $currentPost->post_title;
    // CASE ITS A MEDIA
    if( $currentPost->post_type != 'attachment' ){
        $currentPost = get_post(get_post_thumbnail_id($currentPost->ID));
    }

    $src = wp_get_attachment_image_url( $currentPost->ID, $size );
    $srcset = wp_get_attachment_image_srcset( $currentPost->ID, $size );
    $sizes = wp_get_attachment_image_sizes( $currentPost->ID, $size );

    $alt = get_post_meta( $currentPost->ID, '_wp_attachment_image_alt', true);

    if($lazy){
    $imgMarkup = sprintf('<noscript data-src="%1$s" data-sizes="%3$s" data-srcset="%2$s">
                                   <img class="cover" src="%1$s" alt="%4$s">
                                   </noscript>',
        $src,
        $srcset,
        $sizes,
        $alt);

    }else {
        $imgMarkup = sprintf('<img class="cover" src="%1$s" data-sizes="%3$s" data-srcset="%2$s" alt="%4$s">',
            $src,
            $srcset,
            $sizes,
            $alt);
    }
    return $imgMarkup;
}

function removeFileExtension($filename){
    $ext = getFileExtension($filename);
    return str_replace($ext, '', $filename);
}

function getFileExtension($filename){
    return substr( $filename , strrpos( $filename, '.' ) + 1 );
}

function get_alt($id){
    return get_post_meta( intval($id), '_wp_attachment_image_alt', true);
}

/**
 * ------ GET STATIC IMAGE MARKUP
 */
function static_media($name, $alt = false, $classes = false, $highDensity = false, $optimized=true){
    $ext = getFileExtension($name);
    $alt = ( $alt ? $alt : removeFileExtension($name) );
    $dir = ( $optimized ? '/dist' : '/resources/assets' );

    if($ext == 'svg'){
        $assetsPath = get_bloginfo('stylesheet_directory').$dir.'/vectors/';
    } else{
        $assetsPath = get_bloginfo('stylesheet_directory').$dir.'/images/';
    }

    echo sprintf('<img %1$s src="%2$s%3$s" alt="%4$s" %5$s>',
        ($classes ? 'class="'.$classes.'"' : ''),
        $assetsPath,
        $name,
        $alt,
        ($highDensity ? 'srcset="'.$assetsPath.$name.' 1x,'.$assetsPath.'@2x/'.$name.' 2x"' : '' )
    );
}

/**
 * ------ SLUGIFY text
 */
function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

/**
 * ------ LISTIFY text
 */
function listify($text) {
    $newText = slugify($text);

    // remove duplicate -
    $list = preg_replace('[-]', ',', $newText);

    return $list;
}


/**
 * ------ RETURNS INFO SELECT OPTIONS
 *
 *  requires list of wp objects
 *  ( to refactor !)
 */

function get_select_options($list){
    $options = '';

    foreach ($list as $listElement){

        $options .= '<option value="'.$listElement->slug.'">'.ucfirst($listElement->name).'</option>';
    }


    return $options;
}


/**
 *  ----------- GET CURRENT TERM
 *
 * get the current term which archive is being displayed
 * good for categories, tags and custom taxonomies
 *
 */
function getCurrentTerm(){
    if (!is_category() && !is_tag() && !is_tax()){
        return false;
    }

    $term_slug = get_query_var( 'term' );
    $taxonomyName = get_query_var( 'taxonomy' );
    return get_term_by('slug', $term_slug, $taxonomyName );
}


/**
 *  ----------- FIND NUMBERS - FORMAT NUMERIC VALUES
 *
 * separates numeric values from text values putting them
 * into span tags with the class 'num'
 *
 */

function findNumbers($str){
    $newString = $str;

    preg_match_all('!\d+!', $str, $matches);

    $count = 0;

    foreach( $matches[0] as $num ){
        if($count <= 0){
            $formattedNum = '<span class="num">'.$num.'</span>';
            $newString = str_replace( $num, $formattedNum, $newString, $count );
        }
    }

    return $newString;
}

/**
 *
 *
 * LANGUAGE SWITCHER FOR WPML
 *
 *
 */

function get_language_switcher(){
    $langSwitcher = '<ul class="lang-switcher list">';
    $langs = apply_filters( 'wpml_active_languages', NULL, array() );

    foreach ($langs as $lang ){

        // CHECK FOR ACTIVE LANG
        $langSwitcher .= '<li class="lang-switch '.( $lang['active'] !== 0 ? 'lang-current' : '' ).'">';
        $langSwitcher .= '<a href="'.$lang['url'].'">'.$lang['language_code'].'</a>';
        $langSwitcher .= '</li>';
    }

    $langSwitcher .= '</ul>';
    return $langSwitcher;
}



/**
 *  ----------- GET DOWNLOAD LINKS MARKUP AND STYLE
 *
 * @ARGS :
 * str   - filename, ( user customizable )
 * array - acf file array
 *
 */

function getDownloadsMarkup($fileName, $fileArray){

    $ext = getFileExtension($fileArray['url']);
    $downloadAttr = '';

    switch($ext){
        case 'jpg' :
            $btnStyle = 'btn-img';
            $downloadAttr = 'download="'.$fileArray['filename'].'"';
            break;
        case 'png' :
            $btnStyle = 'btn-img';
            $downloadAttr = 'download="'.$fileArray['filename'].'"';
            break;
        case 'pdf' :
            $btnStyle = 'btn-file';
            break;
        case 'zip' :
            $btnStyle = 'btn-zip';
            break;
        case 'rar' :
            $btnStyle = 'btn-zip';
            break;
        default :
            $btnStyle = 'btn-download';
            break;
    }


    return sprintf('
    <a class="btn %4$s" href="%2$s" %3$s target="_blank">%1$s</a>',
        $fileName,
        $fileArray['url'],
        $downloadAttr,
        $btnStyle);
}


/**
 * CUSTOM GET THE EXCERPT
 */

function custom_get_excerpt($limit, $source = null){

    if($source == "content" ? ($excerpt = get_the_content()) : ($excerpt = get_the_excerpt()));
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);

    if( strlen($excerpt) > $limit ) {

        $excerpt = substr($excerpt, 0, $limit);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
        $excerpt = $excerpt . '...';
    }
    return $excerpt;
}

/**
 * PAGER HACK
 */

function start_pager_hack( $queryArgs ){
    global $wp_query;
    global $temp_query;

    $paged = ( get_query_var('paged') ? get_query_var('paged') : 1 );
    $queryArgs['paged'] = $paged;

    $currentQuery = new WP_Query( $queryArgs );
    $temp_query = $wp_query;
    $wp_query   = NULL;
    $wp_query = $currentQuery;
}

function end_pager_hack(){
    global $wp_query;
    global $temp_query;

    $wp_query = NULL;
    $wp_query = $temp_query;
}

/**
 * FUNCTION TO ORDER POSTS BY TERMS ORDER
 * (not working with hierarchical Terms or multiple terms assignment yet)
 */
function match_terms_sort( $postList, $termsList){
    $reorderedPosts = array();
    $newList = $postList;

    foreach ($termsList as $term){
        foreach ($newList as $key => $value ){
            if(has_term( $term->term_id, 'category', $value)){
                $reorderedPosts[] = $value;
                unset($newList[$key]);
            };
        }
    }

    return $reorderedPosts;
}
