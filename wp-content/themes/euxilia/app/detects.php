<?php // Simple browser detection
$is_lynx = $is_gecko = $is_winIE = $is_macIE = $is_opera = $is_NS4 = $is_safari = $is_chrome = $is_iphone = $is_edge = $is_WIN = false;

if ( isset($_SERVER['HTTP_USER_AGENT']) ) {

    if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Lynx') !== false ) {
        $is_lynx = true;
    } elseif ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Edge' ) !== false ) {
        $is_edge = true;
    } elseif ( stripos($_SERVER['HTTP_USER_AGENT'], 'chrome') !== false ) {
        if ( stripos( $_SERVER['HTTP_USER_AGENT'], 'chromeframe' ) !== false ) {
            $is_admin = is_admin();
            /**
             * Filters whether Google Chrome Frame should be used, if available.
             *
             * @since 3.2.0
             *
             * @param bool $is_admin Whether to use the Google Chrome Frame. Default is the value of is_admin().
             */
            if ( $is_chrome = apply_filters( 'use_google_chrome_frame', $is_admin ) )
                header( 'X-UA-Compatible: chrome=1' );
            $is_winIE = ! $is_chrome;
        } else {
            $is_chrome = true;
        }
    } elseif ( stripos($_SERVER['HTTP_USER_AGENT'], 'safari') !== false ) {
        $is_safari = true;
    } elseif ( ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false ) && strpos($_SERVER['HTTP_USER_AGENT'], 'Win') !== false ) {
        $is_winIE = true;
    } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'Mac') !== false ) {
        $is_macIE = true;
    } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Gecko') !== false ) {
        $is_gecko = true;
    } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') !== false ) {
        $is_opera = true;
    } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Nav') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'Mozilla/4.') !== false ) {
        $is_NS4 = true;
    }

    if ( stripos($_SERVER['HTTP_USER_AGENT'], 'windows') !== false ) {
        $is_WIN = true;
    }
}

if ( $is_safari && stripos($_SERVER['HTTP_USER_AGENT'], 'mobile') !== false )
    $is_iphone = true;

$is_IE = ( $is_macIE || $is_winIE );

// Server detection

/**
 * Whether the server software is Apache or something else
 * @global bool $is_apache
 */
$is_apache = (strpos($_SERVER['SERVER_SOFTWARE'], 'Apache') !== false || strpos($_SERVER['SERVER_SOFTWARE'], 'LiteSpeed') !== false);

/**
 * Whether the server software is Nginx or something else
 * @global bool $is_nginx
 */
$is_nginx = (strpos($_SERVER['SERVER_SOFTWARE'], 'nginx') !== false);

/**
 * Whether the server software is IIS or something else
 * @global bool $is_IIS
 */
$is_IIS = !$is_apache && (strpos($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS') !== false || strpos($_SERVER['SERVER_SOFTWARE'], 'ExpressionDevServer') !== false);

/**
 * Whether the server software is IIS 7.X or greater
 * @global bool $is_iis7
 */
$is_iis7 = $is_IIS && intval( substr( $_SERVER['SERVER_SOFTWARE'], strpos( $_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS/' ) + 14 ) ) >= 7;

/*
 * Adds browser related classes to body
 * */
function browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone, $is_WIN;

    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) $classes[] = 'ie';
    else $classes[] = 'unknown';

    if($is_WIN) $classes[] = 'microsoft';

    if($is_iphone) $classes[] = 'iphone';
    return $classes;
}
// --------- ADDS BROWSER CLASS TO BODY
add_filter( 'body_class', 'browser_body_class', 12 );
?>
