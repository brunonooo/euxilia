<?php

if(!defined('CURRLANG') && function_exists('pll_current_language')){
    define('CURRLANG', pll_current_language());
}
/**
 ** Function to get translated permalink
 */
function fetchTranslatedLink($slug){
    $cPage = get_page_by_path($slug);
    if(function_exists('pll_current_language') && !empty($cPage)){
        $transPost = pll_get_post( $cPage->ID , pll_current_language());
    }
    return get_permalink($transPost);
};


/**
 ** Function to get translated slug
 */
function fetchTranslatedSlug($slug){
    $cPage = get_page_by_path($slug);

    if(function_exists('pll_current_language') && !empty($cPage)){
        $transPost = get_post( pll_get_post( $cPage->ID , pll_current_language() ));

        return $transPost->post_name;
    }
};

/**
 ** Function to get base language translated page slug
 */
function fetchBaseTranslationSlug($slug){
    $cPage = get_page_by_path($slug);

    if(function_exists('pll_current_language') && !empty($cPage)){
        $transPost = get_post( pll_get_post( $cPage->ID , 'it' ));

        return $transPost->post_name;
    }
};
/**
 ** Function to get translated post ID
 */
function fetchTranslatedPost($slug){
    $cPage = get_page_by_path($slug);

    if(function_exists('pll_current_language') && !empty($cPage)){
       $transPost = pll_get_post( $cPage->ID , pll_current_language());

        if(!$cPage){
            return get_the_ID();
        }else{
            return $transPost;
        }
    }
};

/**
 ** String translations
 */
$translations = array(
    array(
        'name'   => 'scopri-di-piu',
        'string' => 'Scopri di più',
    ),
    array(
        'name'   => 'scopri',
        'string' => 'Scopri',
    ),
    array(
        'name'   => 'accetto',
        'string' => 'Accetto',
    ),
);


$noooMessages = array(
    array(
        'name'   => 'Video Support Message',
        'string' => 'Il tuo browser non supporta il tag video',
        'group'  => 'Nooo Messages',
    ),
);

array_merge($translations, $noooMessages);

foreach ($translations as $translation){

    $group = ( empty($translation['group']) ? THEME_NAME : $translation['group'] );
    if(function_exists('pll_register_string')){
        pll_register_string( $translation['name'], $translation['string'], $group);
    }

}
