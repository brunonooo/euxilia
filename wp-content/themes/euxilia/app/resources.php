<?php
// ------------ ENQUEUE DIVI STYLES
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


/**
 * LINKING RESOURCES
 **/

function resources() {

    /**
     * STYLES
     **/

    wp_enqueue_style('g-fonts', 'https://fonts.googleapis.com/css?family=Quicksand|Muli:400,800');

    if(is_front_page()) {
        wp_enqueue_style('fullpage', get_bloginfo('stylesheet_directory') . '/node_modules/fullpage.js/dist/jquery.fullpage.min.css');
        wp_enqueue_style('lightslider', get_bloginfo('stylesheet_directory') . '/node_modules/lightslider/dist/css/lightslider.min.css');
    }

    if( is_page( fetchTranslatedSlug('blog') ) ){
        wp_enqueue_style('lightslider', get_bloginfo('stylesheet_directory') . '/node_modules/lightslider/dist/css/lightslider.min.css');
    }


    wp_enqueue_style( 'ionicons',  'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');
    wp_enqueue_style( 'main',   asset_path('styles/main.css'));

    /**
     * SCRIPTS
     **/

    if ( ! is_admin() ) {
        $jqueryHandle = wp_scripts()->query( 'jquery' );

        wp_deregister_script( 'jquery' );

        wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), array(), $jqueryHandle->ver, true );
    }

    wp_enqueue_script( 'jquery' );

    if( is_page( fetchTranslatedSlug('la-nostra-proposta-valore') ) ){
        wp_enqueue_script( 'skrollr',  get_bloginfo('stylesheet_directory') . '/node_modules/skrollr/dist/skrollr.min.js', array(), false, true );
    }

    if(is_front_page()) {
        wp_enqueue_script( 'fullpage',  get_bloginfo('stylesheet_directory') . '/node_modules/fullpage.js/dist/jquery.fullpage.js', array('jquery'), false, true );
    }

    if(is_page(fetchTranslatedSlug('being-lean'))){
        wp_enqueue_script('popup-manager', get_bloginfo('stylesheet_directory') . '/resources/js/popup-manager.js', array('jquery'), false, true);
    }

    if(is_page(fetchTranslatedSlug('contatti'))){
        wp_enqueue_script('gmaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDCioepAYK2zeTztB5uucDsjRFSnsPTHjI&callback=gc.mapsCheck', array(), false, true);
    }
    wp_enqueue_script('vendor', asset_path('scripts/vendor.js'), array(), false, true);
    wp_enqueue_script('main', asset_path('scripts/main.js'), array(), false, true);
}

add_action( 'wp_enqueue_scripts', 'resources' );
