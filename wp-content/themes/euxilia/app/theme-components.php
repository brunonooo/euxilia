<?php

/**
 * CUSTOM POST TYPES
 **/
function custom_post_types() {

    $labelsEsempi = array(
        'name'               => 'Esempi '.THEME_NAME,
        'singular_name'      => 'Esempio '.THEME_NAME,
        'menu_name'          => 'Esempi '.THEME_NAME,
        'name_admin_bar'     => 'Esempio '.THEME_NAME,
    );

    register_post_type( 'examples-'.THEME_NAME, array(
        'public'             => true,
        'labels'             => $labelsEsempi,
        'rewrite'            => array( 'slug' => 'esempi-creati' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'taxonomies'         => array('category'),
        'menu_position'      => 5,
        'menu_icon'          =>   'dashicons-awards',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    ));

}
add_action( 'init', 'custom_post_types' );