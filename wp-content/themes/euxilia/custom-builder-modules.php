<?php function ex_divi_child_theme_setup() {

    if ( class_exists('ET_Builder_Module')) {

        class ET_Builder_Section_AUXIELL extends ET_Builder_Section {
            function init() {
                $this->name = esc_html__( 'Section', 'et_builder' );
                $this->slug = 'et_pb_section';

                $this->whitelisted_fields = array(
                    'ctm_vertical_align',
                    'background_image',
                    'transparent_background',
                    'background_color',
                    'background_video_mp4',
                    'background_video_webm',
                    'background_video_width',
                    'background_video_height',
                    'ctm_background_video_attr',
                    'allow_player_pause',
                    'inner_shadow',
                    'parallax',
                    'parallax_method',
                    'custom_padding',
                    'custom_padding_tablet',
                    'custom_padding_phone',
                    'padding_mobile',
                    'module_id',
                    'module_class',
                    'make_fullwidth',
                    'use_custom_width',
                    'width_unit',
                    'custom_width_px',
                    'custom_width_percent',
                    'make_equal',
                    'use_custom_gutter',
                    'gutter_width',
                    'columns',
                    'fullwidth',
                    'specialty',
                    'background_color_1',
                    'background_color_2',
                    'background_color_3',
                    'bg_img_1',
                    'bg_img_2',
                    'bg_img_3',
                    'padding_top_1',
                    'padding_right_1',
                    'padding_bottom_1',
                    'padding_left_1',
                    'padding_top_2',
                    'padding_right_2',
                    'padding_bottom_2',
                    'padding_left_2',
                    'padding_top_3',
                    'padding_right_3',
                    'padding_bottom_3',
                    'padding_left_3',
                    'padding_1_tablet',
                    'padding_2_tablet',
                    'padding_3_tablet',
                    'padding_1_phone',
                    'padding_2_phone',
                    'padding_3_phone',
                    'admin_label',
                    'module_id_1',
                    'module_id_2',
                    'module_id_3',
                    'module_class_1',
                    'module_class_2',
                    'module_class_3',
                    'custom_css_before_1',
                    'custom_css_before_2',
                    'custom_css_before_3',
                    'custom_css_main_1',
                    'custom_css_main_2',
                    'custom_css_main_3',
                    'custom_css_after_1',
                    'custom_css_after_2',
                    'custom_css_after_3',
                );

                $this->fields_defaults = array(
                    'transparent_background' => array( 'default' ),
                    'background_color'       => array( '', 'only_default_setting' ),
                    'allow_player_pause'     => array( 'off' ),
                    'inner_shadow'           => array( 'off' ),
                    'parallax'               => array( 'off' ),
                    'parallax_method'        => array( 'on' ),
                    'padding_mobile'         => array( 'off' ),
                    'make_fullwidth'         => array( 'off' ),
                    'use_custom_width'       => array( 'off' ),
                    'width_unit'             => array( 'off' ),
                    'custom_width_px'        => array( '1080px', 'only_default_setting' ),
                    'custom_width_percent'   => array( '80%', 'only_default_setting' ),
                    'make_equal'             => array( 'off' ),
                    'use_custom_gutter'      => array( 'off' ),
                    'gutter_width'           => array( '' ),
                    'fullwidth'              => array( 'off' ),
                    'specialty'              => array( 'off' ),
                    'custom_padding_tablet'  => array( '' ),
                    'custom_padding_phone'   => array( '' ),
                    'ctm_vertical_align'    => array( 'off' ),
                );
            }

            function get_fields() {
                $fields = array(
                    'background_image' => array(
                        'label'              => esc_html__( 'Background Image', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Background Image', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Background', 'et_builder' ),
                        'description'        => esc_html__( 'If defined, this image will be used as the background for this module. To remove a background image, simply delete the URL from the settings field.', 'et_builder' ),
                    ),
                    'transparent_background' => array(
                        'label'             => esc_html__( 'Transparent Background Color', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'color_option',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_background_color',
                        ),
                        'description'       => esc_html__( 'Enabling this option will remove the background color of this section, allowing the website background color or background image to show through.', 'et_builder' ),
                    ),
                    'background_color' => array(
                        'label'           => esc_html__( 'Background Color', 'et_builder' ),
                        'type'            => 'color-alpha',
                        'default'         => '#ffffff',
                        'depends_show_if' => 'off',
                        'description'     => esc_html__( 'Define a custom background color for your module, or leave blank to use the default color.', 'et_builder' ),
                        'additional_code' => '<span class="et-pb-reset-setting reset-default-color" style="display: none;"></span>',
                    ),
                    'background_video_mp4' => array(
                        'label'              => esc_html__( 'Background Video MP4', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'data_type'          => 'video',
                        'upload_button_text' => esc_attr__( 'Upload a video', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Background Video MP4 File', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Background Video', 'et_builder' ),
                        'description'        => et_get_safe_localization( __( 'All videos should be uploaded in both .MP4 .WEBM formats to ensure maximum compatibility in all browsers. Upload the .MP4 version here. <b>Important Note: Video backgrounds are disabled from mobile devices. Instead, your background image will be used. For this reason, you should define both a background image and a background video to ensure best results.</b>', 'et_builder' ) ),
                    ),
                    'background_video_webm' => array(
                        'label'              => esc_html__( 'Background Video Webm', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'data_type'          => 'video',
                        'upload_button_text' => esc_attr__( 'Upload a video', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Background Video WEBM File', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Background Video', 'et_builder' ),
                        'description'        => et_get_safe_localization( __( 'All videos should be uploaded in both .MP4 .WEBM formats to ensure maximum compatibility in all browsers. Upload the .WEBM version here. <b>Important Note: Video backgrounds are disabled from mobile devices. Instead, your background image will be used. For this reason, you should define both a background image and a background video to ensure best results.</b>', 'et_builder' ) ),
                    ),
                    'background_video_width' => array(
                        'label'           => esc_html__( 'Background Video Width', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'In order for videos to be sized correctly, you must input the exact width (in pixels) of your video here.', 'et_builder' ),
                    ),
                    'background_video_height' => array(
                        'label'           => esc_html__( 'Background Video Height', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'In order for videos to be sized correctly, you must input the exact height (in pixels) of your video here.', 'et_builder' ),
                    ),
                    'ctm_background_video_attr' => array(
                        'label'           => esc_html__( 'Background Video Attributes', 'auxiell' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Add here extra attributes for the background video', 'auxiell' ),
                    ),
                    'allow_player_pause' => array(
                        'label'           => esc_html__( 'Pause Video', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Allow video to be paused by other players when they begin playing', 'et_builder' ),
                    ),
                    'inner_shadow' => array(
                        'label'           => esc_html__( 'Show Inner Shadow', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Here you can select whether or not your section has an inner shadow. This can look great when you have colored backgrounds or background images.', 'et_builder' ),
                    ),
                    'parallax' => array(
                        'label'             => esc_html__( 'Use Parallax Effect', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_parallax_method',
                        ),
                        'description'       => esc_html__( 'If enabled, your background image will stay fixed as your scroll, creating a fun parallax-like effect.', 'et_builder' ),
                    ),
                    'parallax_method' => array(
                        'label'             => esc_html__( 'Parallax Method', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off'  => esc_html__( 'CSS', 'et_builder' ),
                            'on'   => esc_html__( 'True Parallax', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Define the method, used for the parallax effect.', 'et_builder' ),
                    ),
                    'custom_padding' => array(
                        'label'           => esc_html__( 'Custom Padding', 'et_builder' ),
                        'type'            => 'custom_padding',
                        'mobile_options'  => true,
                        'option_category' => 'layout',
                        'description'     => esc_html__( 'Adjust padding to specific values, or leave blank to use the default padding.', 'et_builder' ),
                    ),
                    'custom_padding_tablet' => array(
                        'type' => 'skip',
                    ),
                    'custom_padding_phone' => array(
                        'type' => 'skip',
                    ),
                    'padding_mobile' => array(
                        'label'             => esc_html__( 'Keep Custom Padding on Mobile', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Allow custom padding to be retained on mobile screens', 'et_builder' ),
                    ),
                    'make_fullwidth' => array(
                        'label'             => esc_html__( 'Make This Section Fullwidth', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'off',
                        'tab_slug' => 'advanced',
                    ),
                    'use_custom_width' => array(
                        'label'             => esc_html__( 'Use Custom Width', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_make_fullwidth',
                            '#et_pb_custom_width',
                            '#et_pb_width_unit',
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'width_unit' => array(
                        'label'             => esc_html__( 'Unit', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'on'  => esc_html__( 'px', 'et_builder' ),
                            'off' => '%',
                        ),
                        'button_options' => array(
                            'button_type'       => 'equal',
                        ),
                        'depends_show_if' => 'on',
                        'affects'           => array(
                            '#et_pb_custom_width_px',
                            '#et_pb_custom_width_percent',
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'custom_width_px' => array(
                        'label'           => esc_html__( 'Custom Width', 'et_builder' ),
                        'type'            => 'range',
                        'option_category' => 'layout',
                        'depends_show_if' => 'on',
                        'range_settings'  => array(
                            'min'  => 500,
                            'max'  => 2600,
                            'step' => 1,
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'custom_width_percent' => array(
                        'label'           => esc_html__( 'Custom Width', 'et_builder' ),
                        'type'            => 'range',
                        'option_category' => 'layout',
                        'depends_show_if' => 'off',
                        'range_settings'  => array(
                            'min'  => 0,
                            'max'  => 100,
                            'step' => 1,
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'make_equal' => array(
                        'label'             => esc_html__( 'Equalize Column Heights', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'tab_slug'          => 'advanced',
                    ),
                    'use_custom_gutter' => array(
                        'label'             => esc_html__( 'Use Custom Gutter Width', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_gutter_width',
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'gutter_width' => array(
                        'label'           => esc_html__( 'Gutter Width', 'et_builder' ),
                        'type'            => 'range',
                        'option_category' => 'layout',
                        'range_settings'  => array(
                            'min'  => 1,
                            'max'  => 4,
                            'step' => 1,
                        ),
                        'depends_show_if' => 'on',
                        'tab_slug'        => 'advanced',
                    ),
                    'columns' => array(
                        'type'            => 'column_settings',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'advanced',
                    ),
                    'fullwidth' => array(
                        'type' => 'skip',
                    ),
                    'specialty' => array(
                        'type' => 'skip',
                    ),
                    'background_color_1' => array(
                        'type' => 'skip',
                    ),
                    'background_color_2' => array(
                        'type' => 'skip',
                    ),
                    'background_color_3' => array(
                        'type' => 'skip',
                    ),
                    'bg_img_1' => array(
                        'type' => 'skip',
                    ),
                    'bg_img_2' => array(
                        'type' => 'skip',
                    ),
                    'bg_img_3' => array(
                        'type' => 'skip',
                    ),
                    'padding_top_1' => array(
                        'type' => 'skip',
                    ),
                    'padding_right_1' => array(
                        'type' => 'skip',
                    ),
                    'padding_bottom_1' => array(
                        'type' => 'skip',
                    ),
                    'padding_left_1' => array(
                        'type' => 'skip',
                    ),
                    'padding_top_2' => array(
                        'type' => 'skip',
                    ),
                    'padding_right_2' => array(
                        'type' => 'skip',
                    ),
                    'padding_bottom_2' => array(
                        'type' => 'skip',
                    ),
                    'padding_left_2' => array(
                        'type' => 'skip',
                    ),
                    'padding_top_3' => array(
                        'type' => 'skip',
                    ),
                    'padding_right_3' => array(
                        'type' => 'skip',
                    ),
                    'padding_bottom_3' => array(
                        'type' => 'skip',
                    ),
                    'padding_left_3' => array(
                        'type' => 'skip',
                    ),
                    'padding_1_tablet' => array(
                        'type' => 'skip',
                    ),
                    'padding_2_tablet' => array(
                        'type' => 'skip',
                    ),
                    'padding_3_tablet' => array(
                        'type' => 'skip',
                    ),
                    'padding_1_phone' => array(
                        'type' => 'skip',
                    ),
                    'padding_2_phone' => array(
                        'type' => 'skip',
                    ),
                    'padding_3_phone' => array(
                        'type' => 'skip',
                    ),
                    'module_id_1' => array(
                        'type' => 'skip',
                    ),
                    'module_id_2' => array(
                        'type' => 'skip',
                    ),
                    'module_id_3' => array(
                        'type' => 'skip',
                    ),
                    'module_class_1' => array(
                        'type' => 'skip',
                    ),
                    'module_class_2' => array(
                        'type' => 'skip',
                    ),
                    'module_class_3' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_before_1' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_before_2' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_before_3' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_main_1' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_main_2' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_main_3' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_after_1' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_after_2' => array(
                        'type' => 'skip',
                    ),
                    'custom_css_after_3' => array(
                        'type' => 'skip',
                    ),
                    'columns_css' => array(
                        'type'            => 'column_settings_css',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'priority'        => '20',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the section in the builder for easy identification when collapsed.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'columns_css_fields' => array(
                        'type'            => 'column_settings_css_fields',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                    ),
                    'use_custom_width' => array(
                        'label'             => esc_html__( 'Use Custom Width', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                    ),
                    'ctm_vertical_align' => array(
                        'label'             => esc_html__( 'Vertically aling content', 'auxiell' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'basic_option',
                        'options'           => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Choose if content must be aligned vertically in section', 'auxiell' ),

                    ),
                );

                return $fields;
            }

            /**
             * {@inheritdoc}
             */
            /*function init()
            {
                $fields = parent::fields();
                $fields = array_reppace_recur

                    return $fi  ;
                parent::init();

                $this->fields_defaults[] = 'a';
            }*/

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $module_id               = $this->shortcode_atts['module_id'];
                $module_class            = $this->shortcode_atts['module_class'];
                $background_image        = $this->shortcode_atts['background_image'];
                $background_color        = $this->shortcode_atts['background_color'];
                $background_video_mp4    = $this->shortcode_atts['background_video_mp4'];
                $background_video_webm   = $this->shortcode_atts['background_video_webm'];
                $background_video_width  = $this->shortcode_atts['background_video_width'];
                $background_video_height = $this->shortcode_atts['background_video_height'];
                $ctm_background_video_attr = $this->shortcode_atts['ctm_background_video_attr'];
                $allow_player_pause      = $this->shortcode_atts['allow_player_pause'];
                $inner_shadow            = $this->shortcode_atts['inner_shadow'];
                $parallax                = $this->shortcode_atts['parallax'];
                $parallax_method         = $this->shortcode_atts['parallax_method'];
                $fullwidth               = $this->shortcode_atts['fullwidth'];
                $specialty               = $this->shortcode_atts['specialty'];
                $transparent_background  = $this->shortcode_atts['transparent_background'];
                $custom_padding          = $this->shortcode_atts['custom_padding'];
                $custom_padding_tablet   = $this->shortcode_atts['custom_padding_tablet'];
                $custom_padding_phone    = $this->shortcode_atts['custom_padding_phone'];
                $padding_mobile          = $this->shortcode_atts['padding_mobile'];
                $background_color_1      = $this->shortcode_atts['background_color_1'];
                $background_color_2      = $this->shortcode_atts['background_color_2'];
                $background_color_3      = $this->shortcode_atts['background_color_3'];
                $bg_img_1                = $this->shortcode_atts['bg_img_1'];
                $bg_img_2                = $this->shortcode_atts['bg_img_2'];
                $bg_img_3                = $this->shortcode_atts['bg_img_3'];
                $padding_top_1           = $this->shortcode_atts['padding_top_1'];
                $padding_right_1         = $this->shortcode_atts['padding_right_1'];
                $padding_bottom_1        = $this->shortcode_atts['padding_bottom_1'];
                $padding_left_1          = $this->shortcode_atts['padding_left_1'];
                $padding_top_2           = $this->shortcode_atts['padding_top_2'];
                $padding_right_2         = $this->shortcode_atts['padding_right_2'];
                $padding_bottom_2        = $this->shortcode_atts['padding_bottom_2'];
                $padding_left_2          = $this->shortcode_atts['padding_left_2'];
                $padding_top_3           = $this->shortcode_atts['padding_top_3'];
                $padding_right_3         = $this->shortcode_atts['padding_right_3'];
                $padding_bottom_3        = $this->shortcode_atts['padding_bottom_3'];
                $padding_left_3          = $this->shortcode_atts['padding_left_3'];
                $padding_1_tablet        = $this->shortcode_atts['padding_1_tablet'];
                $padding_2_tablet        = $this->shortcode_atts['padding_2_tablet'];
                $padding_3_tablet        = $this->shortcode_atts['padding_3_tablet'];
                $padding_1_phone         = $this->shortcode_atts['padding_1_phone'];
                $padding_2_phone         = $this->shortcode_atts['padding_2_phone'];
                $padding_3_phone         = $this->shortcode_atts['padding_3_phone'];
                $gutter_width            = $this->shortcode_atts['gutter_width'];
                $use_custom_width        = $this->shortcode_atts['use_custom_width'];
                $custom_width_px         = $this->shortcode_atts['custom_width_px'];
                $custom_width_percent    = $this->shortcode_atts['custom_width_percent'];
                $width_unit              = $this->shortcode_atts['width_unit'];
                $make_equal              = $this->shortcode_atts['make_equal'];
                $make_fullwidth          = $this->shortcode_atts['make_fullwidth'];
                $global_module           = $this->shortcode_atts['global_module'];
                $use_custom_gutter       = $this->shortcode_atts['use_custom_gutter'];
                $module_id_1             = $this->shortcode_atts['module_id_1'];
                $module_id_2             = $this->shortcode_atts['module_id_2'];
                $module_id_3             = $this->shortcode_atts['module_id_3'];
                $module_class_1          = $this->shortcode_atts['module_class_1'];
                $module_class_2          = $this->shortcode_atts['module_class_2'];
                $module_class_3          = $this->shortcode_atts['module_class_3'];
                $custom_css_before_1     = $this->shortcode_atts['custom_css_before_1'];
                $custom_css_before_2     = $this->shortcode_atts['custom_css_before_2'];
                $custom_css_before_3     = $this->shortcode_atts['custom_css_before_3'];
                $custom_css_main_1       = $this->shortcode_atts['custom_css_main_1'];
                $custom_css_main_2       = $this->shortcode_atts['custom_css_main_2'];
                $custom_css_main_3       = $this->shortcode_atts['custom_css_main_3'];
                $custom_css_after_1      = $this->shortcode_atts['custom_css_after_1'];
                $custom_css_after_2      = $this->shortcode_atts['custom_css_after_2'];
                $custom_css_after_3      = $this->shortcode_atts['custom_css_after_3'];
                $ctm_vertical_align      = $this->shortcode_atts['ctm_vertical_align'];

                if ( '' !== $global_module ) {
                    $global_content = et_pb_load_global_module( $global_module );

                    if ( '' !== $global_content ) {
                        return do_shortcode( $global_content );
                    }
                }

                $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
                $gutter_class = '';

                $padding_mobile_values = array(
                    'tablet' => explode( '|', $custom_padding_tablet ),
                    'phone'  => explode( '|', $custom_padding_phone ),
                );

                if ( 'on' === $specialty ) {
                    global $et_pb_column_backgrounds, $et_pb_column_paddings, $et_pb_columns_counter, $et_pb_column_css, $et_pb_column_paddings_mobile;
                    $module_class .= 'on' === $make_equal ? ' et_pb_equal_columns' : '';

                    if ( 'on' === $use_custom_gutter && '' !== $gutter_width ) {
                        $gutter_width = '0' === $gutter_width ? '1' : $gutter_width; // set the gutter to 1 if 0 entered by user
                        $gutter_class .= ' et_pb_gutters' . $gutter_width;
                    }

                    $et_pb_columns_counter = 0;
                    $et_pb_column_backgrounds = array(
                        array( $background_color_1, $bg_img_1 ),
                        array( $background_color_2, $bg_img_2 ),
                        array( $background_color_3, $bg_img_3 ),
                    );

                    $et_pb_column_paddings = array(
                        array(
                            'padding-top'    => $padding_top_1,
                            'padding-right'  => $padding_right_1,
                            'padding-bottom' => $padding_bottom_1,
                            'padding-left'   => $padding_left_1
                        ),
                        array(
                            'padding-top'    => $padding_top_2,
                            'padding-right'  => $padding_right_2,
                            'padding-bottom' => $padding_bottom_2,
                            'padding-left'   => $padding_left_2
                        ),
                        array(
                            'padding-top'    => $padding_top_3,
                            'padding-right'  => $padding_right_3,
                            'padding-bottom' => $padding_bottom_3,
                            'padding-left'   => $padding_left_3
                        ),
                    );

                    $et_pb_column_paddings_mobile = array(
                        array(
                            'tablet' => explode( '|', $padding_1_tablet ),
                            'phone'  => explode( '|', $padding_1_phone ),
                        ),
                        array(
                            'tablet' => explode( '|', $padding_2_tablet ),
                            'phone'  => explode( '|', $padding_2_phone ),
                        ),
                        array(
                            'tablet' => explode( '|', $padding_3_tablet ),
                            'phone'  => explode( '|', $padding_3_phone ),
                        ),
                    );

                    if ( 'on' === $make_fullwidth && 'off' === $use_custom_width ) {
                        $module_class .= ' et_pb_specialty_fullwidth';
                    }

                    if ( 'on' === $use_custom_width ) {
                        ET_Builder_Element::set_style( $function_name, array(
                            'selector'    => '%%order_class%% > .et_pb_row',
                            'declaration' => sprintf(
                                'max-width:%1$s !important;',
                                'on' === $width_unit ? esc_attr( $custom_width_px ) : esc_attr( $custom_width_percent )
                            ),
                        ) );
                    }

                    $et_pb_column_css = array(
                        'css_class'         => array( $module_class_1, $module_class_2, $module_class_3 ),
                        'css_id'            => array( $module_id_1, $module_id_2, $module_id_3 ),
                        'custom_css_before' => array( $custom_css_before_1, $custom_css_before_2, $custom_css_before_3 ),
                        'custom_css_main'   => array( $custom_css_main_1, $custom_css_main_2, $custom_css_main_3 ),
                        'custom_css_after'  => array( $custom_css_after_1, $custom_css_after_2, $custom_css_after_3 ),
                    );
                }

                $background_video = '';

                if ( '' !== $background_video_mp4 || '' !== $background_video_webm ) {
                    $background_video = sprintf(
                        '<div class="et_pb_section_video_bg%2$s">
					%1$s
				</div>',
                        do_shortcode( sprintf( '
					<video %5$s loop="loop" %3$s%4$s>
						%1$s
						%2$s
					</video>',
                            ( '' !== $background_video_mp4 ? sprintf( '<source type="video/mp4" src="%s" />', esc_attr( $background_video_mp4 ) ) : '' ),
                            ( '' !== $background_video_webm ? sprintf( '<source type="video/webm" src="%s" />', esc_attr( $background_video_webm ) ) : '' ),
                            ( '' !== $background_video_width ? sprintf( ' width="%s"', esc_attr( intval( $background_video_width ) ) ) : '' ),
                            ( '' !== $background_video_height ? sprintf( ' height="%s"', esc_attr( intval( $background_video_height ) ) ) : '' ),
                            $ctm_background_video_attr
                        ) ),
                        ( 'on' === $allow_player_pause ? ' et_pb_allow_player_pause' : '' )
                    );

                    wp_enqueue_style( 'wp-mediaelement' );
                    wp_enqueue_script( 'wp-mediaelement' );
                }

                // set the correct default value for $transparent_background option if plugin activated.
                if ( et_is_builder_plugin_active() && 'default' === $transparent_background ) {
                    $transparent_background = '' !== $background_color ? 'off' : 'on';
                } elseif ( 'default' === $transparent_background ) {
                    $transparent_background = 'off';
                }

                if ( '' !== $background_color && 'off' === $transparent_background ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%.et_pb_section',
                        'declaration' => sprintf(
                            'background-color:%s !important;',
                            esc_attr( $background_color )
                        ),
                    ) );
                }

                if ( '' !== $background_image && 'on' !== $parallax ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%',
                        'declaration' => sprintf(
                            'background-image:url(%s);',
                            esc_attr( $background_image )
                        ),
                    ) );
                }

                $padding_values = explode( '|', $custom_padding );

                if ( ! empty( $padding_values ) ) {
                    // old version of sections supports only top and bottom padding, so we need to handle it along with the full padding in the recent version
                    if ( 2 === count( $padding_values ) ) {
                        $padding_settings = array(
                            'top' => isset( $padding_values[0] ) ? $padding_values[0] : '',
                            'bottom' => isset( $padding_values[1] ) ? $padding_values[1] : '',
                        );
                    } else {
                        $padding_settings = array(
                            'top' => isset( $padding_values[0] ) ? $padding_values[0] : '',
                            'right' => isset( $padding_values[1] ) ? $padding_values[1] : '',
                            'bottom' => isset( $padding_values[2] ) ? $padding_values[2] : '',
                            'left' => isset( $padding_values[3] ) ? $padding_values[3] : '',
                        );
                    }

                    foreach( $padding_settings as $padding_side => $value ) {
                        if ( '' !== $value ) {
                            $element_style = array(
                                'selector'    => '%%order_class%%',
                                'declaration' => sprintf(
                                    'padding-%1$s: %2$s;',
                                    esc_html( $padding_side ),
                                    esc_html( $value )
                                ),
                            );

                            if ( 'on' !== $padding_mobile ) {
                                $element_style['media_query'] = ET_Builder_Element::get_media_query( 'min_width_981' );
                            }

                            ET_Builder_Element::set_style( $function_name, $element_style );
                        }
                    }
                }

                if ( ! empty( $padding_mobile_values['tablet'] ) || ! empty( $padding_values['phone'] ) ) {
                    $padding_mobile_values_processed = array();

                    foreach( array( 'tablet', 'phone' ) as $device ) {
                        if ( empty( $padding_mobile_values[$device] ) ) {
                            continue;
                        }

                        $padding_mobile_values_processed[ $device ] = array(
                            'padding-top'    => isset( $padding_mobile_values[$device][0] ) ? $padding_mobile_values[$device][0] : '',
                            'padding-right'  => isset( $padding_mobile_values[$device][1] ) ? $padding_mobile_values[$device][1] : '',
                            'padding-bottom' => isset( $padding_mobile_values[$device][2] ) ? $padding_mobile_values[$device][2] : '',
                            'padding-left'   => isset( $padding_mobile_values[$device][3] ) ? $padding_mobile_values[$device][3] : '',
                        );
                    }

                    if ( ! empty( $padding_mobile_values_processed ) ) {
                        et_pb_generate_responsive_css( $padding_mobile_values_processed, '%%order_class%%', '', $function_name );
                    }
                }

                if ( '' !== $background_video_mp4 || '' !== $background_video_webm || ( '' !== $background_color && 'off' === $transparent_background ) || '' !== $background_image ) {
                    $module_class .= ' et_pb_with_background';
                }

                if( 'on' === $ctm_vertical_align){
                    $verticalClass=" vertical";
                    $verticalMiddleStart='<div class="vertical-middle">';
                    $verticalMiddleEnd ='</div>';
                }else{
                    $verticalClass='';
                    $verticalMiddleStart='';
                    $verticalMiddleEnd ='';
                }

                $output = sprintf(
                    '<div%7$s class="et_pb_section%3$s%4$s%5$s%6$s%8$s%12$s%13$s%14$s">
%15$s
				%11$s
				%9$s
					%2$s
					%1$s
				%10$s
				%16$s
			</div> <!-- .et_pb_section -->',
                    do_shortcode( et_pb_fix_shortcodes( $content ) ),
                    $background_video,
                    ( '' !== $background_video ? ' et_pb_section_video et_pb_preload' : '' ),
                    ( ( 'off' !== $inner_shadow && ! ( '' !== $background_image && 'on' === $parallax && 'off' === $parallax_method ) ) ? ' et_pb_inner_shadow' : '' ),
                    ( 'on' === $parallax ? ' et_pb_section_parallax' : '' ),
                    ( 'off' !== $fullwidth ? ' et_pb_fullwidth_section' : '' ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                    ( 'on' === $specialty ?
                        sprintf( '<div class="et_pb_row%1$s">', $gutter_class )
                        : '' ),
                    ( 'on' === $specialty ? '</div> <!-- .et_pb_row -->' : '' ),
                    ( '' !== $background_image && 'on' === $parallax
                        ? sprintf(
                            '<div class="et_parallax_bg%2$s%3$s" style="background-image: url(%1$s);"></div>',
                            esc_attr( $background_image ),
                            ( 'off' === $parallax_method ? ' et_pb_parallax_css' : '' ),
                            ( ( 'off' !== $inner_shadow && 'off' === $parallax_method ) ? ' et_pb_inner_shadow' : '' )
                        )
                        : ''
                    ),
                    ( 'on' === $specialty ? ' et_section_specialty' : ' et_section_regular' ),
                    ( 'on' === $transparent_background ? ' et_section_transparent' : '' ),
                    $verticalClass,
                    $verticalMiddleStart,
                    $verticalMiddleEnd
                );

                return $output;

            }

        }

        $et_builder_section_auxiell = new ET_Builder_Section_AUXIELL;
        remove_shortcode( 'et_pb_section' );
        add_shortcode( 'et_pb_section', array($et_builder_section_auxiell, '_shortcode_callback') );

        class ET_Builder_Column_AUXIELL extends ET_Builder_Column {

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $type                        = $this->shortcode_atts['type'];
                $specialty_columns           = $this->shortcode_atts['specialty_columns'];
                $saved_specialty_column_type = $this->shortcode_atts['saved_specialty_column_type'];

                global $et_pb_all_column_settings,
                       $et_pb_all_column_settings_inner,
                       $et_specialty_column_type,
                       $et_pb_rendering_column_content,
                       $et_pb_rendering_column_content_row;

                $is_specialty_column = 'et_pb_column_inner' !== $function_name && '' !== $specialty_columns;

                $current_row_position = $et_pb_rendering_column_content_row ? 'internal_row' : 'regular_row';

                if ( 'et_pb_column_inner' !== $function_name ) {
                    $et_specialty_column_type = $type;
                    $array_index = isset( $et_pb_all_column_settings[ $current_row_position ] ) ? $et_pb_all_column_settings[ $current_row_position ]['et_pb_columns_counter'] : 0;
                    $backgrounds_array = isset( $et_pb_all_column_settings[ $current_row_position ] ) ? $et_pb_all_column_settings[ $current_row_position ]['et_pb_column_backgrounds'] : array();
                    $paddings_array = isset( $et_pb_all_column_settings[ $current_row_position ] ) ? $et_pb_all_column_settings[ $current_row_position ]['et_pb_column_paddings'] : array();
                    $paddings_mobile_array = isset( $et_pb_all_column_settings[ $current_row_position ] ) ? $et_pb_all_column_settings[ $current_row_position ]['et_pb_column_paddings_mobile'] : array();
                    $column_css_array = isset( $et_pb_all_column_settings[ $current_row_position ] ) ? $et_pb_all_column_settings[ $current_row_position ]['et_pb_column_css'] : array();
                    $keep_column_padding_mobile = isset( $et_pb_all_column_settings[ $current_row_position ] ) ? $et_pb_all_column_settings[ $current_row_position ]['keep_column_padding_mobile'] : 'on';
                    $column_parallax = isset( $et_pb_all_column_settings[ $current_row_position ] ) && isset( $et_pb_all_column_settings[ $current_row_position ]['et_pb_column_parallax'] ) ? $et_pb_all_column_settings[ $current_row_position ]['et_pb_column_parallax'] : '';
                    if ( isset( $et_pb_all_column_settings[ $current_row_position ] ) ) {
                        $et_pb_all_column_settings[ $current_row_position ]['et_pb_columns_counter']++;
                    }
                } else {
                    $array_index = $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_columns_inner_counter'];
                    $backgrounds_array = $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_column_inner_backgrounds'];
                    $paddings_array = $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_column_inner_paddings'];
                    $column_css_array = $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_column_inner_css'];
                    $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_columns_inner_counter']++;
                    $paddings_mobile_array = $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_column_inner_paddings_mobile'];
                    $keep_column_padding_mobile = $et_pb_all_column_settings_inner[ $current_row_position ]['keep_column_padding_mobile'];
                    $column_parallax = isset( $et_pb_all_column_settings_inner[ $current_row_position ] ) && isset( $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_column_parallax'] ) ? $et_pb_all_column_settings_inner[ $current_row_position ]['et_pb_column_parallax'] : '';
                }

                $background_color = isset( $backgrounds_array[$array_index][0] ) ? $backgrounds_array[$array_index][0] : '';
                $background_img = isset( $backgrounds_array[$array_index][1] ) ? $backgrounds_array[$array_index][1] : '';
                $padding_values = isset( $paddings_array[$array_index] ) ? $paddings_array[$array_index] : array();
                $padding_mobile_values = isset( $paddings_mobile_array[$array_index] ) ? $paddings_mobile_array[$array_index] : array();
                $padding_last_edited = isset( $padding_mobile_values['last_edited'] ) ? $padding_mobile_values['last_edited'] : 'off|desktop';
                $padding_responsive_active = et_pb_get_responsive_status( $padding_last_edited );
                $parallax_method = isset( $column_parallax[$array_index][0] ) && 'on' === $column_parallax[$array_index][0] ? $column_parallax[$array_index][1] : '';
                $custom_css_class = isset( $column_css_array['css_class'][$array_index] ) ? ' ' . $column_css_array['css_class'][$array_index] : '';
                $custom_css_id = isset( $column_css_array['css_id'][$array_index] ) ? $column_css_array['css_id'][$array_index] : '';
                $custom_css_before = isset( $column_css_array['custom_css_before'][$array_index] ) ? $column_css_array['custom_css_before'][$array_index] : '';
                $custom_css_main = isset( $column_css_array['custom_css_main'][$array_index] ) ? $column_css_array['custom_css_main'][$array_index] : '';
                $custom_css_after = isset( $column_css_array['custom_css_after'][$array_index] ) ? $column_css_array['custom_css_after'][$array_index] : '';

                if ( '' !== $background_color && 'rgba(0,0,0,0)' !== $background_color ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%',
                        'declaration' => sprintf(
                            'background-color:%s;',
                            esc_attr( $background_color )
                        ),
                    ) );
                }

                if ( '' !== $background_img && '' === $parallax_method ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%',
                        'declaration' => sprintf(
                            'background-image:url(%s);',
                            esc_attr( $background_img )
                        ),
                    ) );
                }

                if ( ! empty( $padding_values ) ) {
                    foreach( $padding_values as $position => $value ) {
                        if ( '' !== $value ) {
                            $element_style = array(
                                'selector'    => '%%order_class%%',
                                'declaration' => sprintf(
                                    '%1$s:%2$s;',
                                    esc_html( $position ),
                                    esc_html( et_builder_process_range_value( $value ) )
                                ),
                            );

                            // Backward compatibility. Keep Padding on Mobile is deprecated in favour of responsive inputs mechanism for custom padding
                            // To ensure that it is compatibility with previous version of Divi, this option is now only used as last resort if no
                            // responsive padding value is found,  and padding_mobile value is saved (which is set to off by default)
                            if ( in_array( $keep_column_padding_mobile, array( 'on', 'off' ) ) && 'on' !== $keep_column_padding_mobile && ! $padding_responsive_active ) {
                                $element_style['media_query'] = ET_Builder_Element::get_media_query( 'min_width_981' );
                            }

                            ET_Builder_Element::set_style( $function_name, $element_style );
                        }
                    }
                }

                if ( $padding_responsive_active && ( ! empty( $padding_mobile_values['tablet'] ) || ! empty( $padding_values['phone'] ) ) ) {
                    $padding_mobile_values_processed = array();

                    foreach( array( 'tablet', 'phone' ) as $device ) {
                        if ( empty( $padding_mobile_values[$device] ) ) {
                            continue;
                        }

                        $padding_mobile_values_processed[ $device ] = array(
                            'padding-top'    => isset( $padding_mobile_values[$device][0] ) ? $padding_mobile_values[$device][0] : '',
                            'padding-right'  => isset( $padding_mobile_values[$device][1] ) ? $padding_mobile_values[$device][1] : '',
                            'padding-bottom' => isset( $padding_mobile_values[$device][2] ) ? $padding_mobile_values[$device][2] : '',
                            'padding-left'   => isset( $padding_mobile_values[$device][3] ) ? $padding_mobile_values[$device][3] : '',
                        );
                    }

                    if ( ! empty( $padding_mobile_values_processed ) ) {
                        $padding_mobile_selector = 'et_pb_column_inner' !== $function_name ? '.et_pb_row > .et_pb_column%%order_class%%' : '.et_pb_row_inner > .et_pb_column%%order_class%%';
                        et_pb_generate_responsive_css( $padding_mobile_values_processed, $padding_mobile_selector, '', $function_name );
                    }
                }

                if ( '' !== $custom_css_before ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%:before',
                        'declaration' => trim( $custom_css_before ),
                    ) );
                }

                if ( '' !== $custom_css_main ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%',
                        'declaration' => trim( $custom_css_main ),
                    ) );
                }

                if ( '' !== $custom_css_after ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%:after',
                        'declaration' => trim( $custom_css_after ),
                    ) );
                }

                if ( 'et_pb_column_inner' === $function_name ) {
                    if ( '1_1' === $type ) {
                        $type = '4_4';
                    }

                    $et_specialty_column_type = '' !== $saved_specialty_column_type ? $saved_specialty_column_type : $et_specialty_column_type;

                    switch ( $et_specialty_column_type ) {
                        case '1_2':
                            if ( '1_2' === $type ) {
                                $type = '1_4';
                            }

                            break;
                        case '2_3':
                            if ( '1_2' === $type ) {
                                $type = '1_3';
                            }

                            break;
                        case '3_4':
                            if ( '1_2' === $type ) {
                                $type = '3_8';
                            } else if ( '1_3' === $type ) {
                                $type = '1_4';
                            }

                            break;
                    }
                }

                $inner_class = 'et_pb_column_inner' === $function_name ? ' et_pb_column_inner' : '';

                $class = 'et_pb_column_' . $type . $inner_class . $custom_css_class;

                $class = ET_Builder_Element::add_module_order_class( $class, $function_name );

                $inner_content = do_shortcode( et_pb_fix_shortcodes( $content ) );
                $class .= '' == trim( $inner_content ) ? ' et_pb_column_empty' : '';

                $class .= $is_specialty_column ? ' et_pb_specialty_column' : '';

                $output = sprintf(
                    '<div class="et_pb_column %1$s%3$s"%5$s>
                        <div class="col-inner">
                            %4$s
                            %2$s
                        </div>
                    </div> <!-- .et_pb_column -->',
                    esc_attr( $class ),
                    $inner_content,
                    ( '' !== $parallax_method ? ' et_pb_section_parallax' : '' ),
                    ( '' !== $background_img && '' !== $parallax_method
                        ? sprintf(
                            '<div class="et_parallax_bg%2$s" style="background-image: url(%1$s);"></div>',
                            esc_attr( $background_img ),
                            ( 'off' === $parallax_method ? ' et_pb_parallax_css' : '' )
                        )
                        : ''
                    ),
                    '' !== $custom_css_id ? sprintf( ' id="%1$s"', esc_attr( $custom_css_id ) ) : ''
                );

                return $output;

            }
        }
        $et_builder_column_auxiell = new ET_Builder_Column_AUXIELL;
        remove_shortcode( 'et_pb_column' );
        add_shortcode( 'et_pb_column', array($et_builder_column_auxiell, '_shortcode_callback') );

        class ET_Builder_Module_Blog_AUXIELL extends ET_Builder_Module {
            function init() {
                $this->name = esc_html__( 'Blog AUXIELL', 'et_builder' );
                $this->slug = 'et_pb_blog';

                $this->whitelisted_fields = array(
                    'fullwidth',
                    'posts_number',
                    'include_categories',
                    'meta_date',
                    'show_thumbnail',
                    'show_content',
                    'show_more',
                    'show_author',
                    'show_date',
                    'show_categories',
                    'show_comments',
                    'show_pagination',
                    'offset_number',
                    'background_layout',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'masonry_tile_background_color',
                    'use_dropshadow',
                    'use_overlay',
                    'overlay_icon_color',
                    'hover_overlay_color',
                    'hover_icon',
                );

                $this->fields_defaults = array(
                    'fullwidth'         => array( 'on' ),
                    'posts_number'      => array( 10, 'add_default_setting' ),
                    'meta_date'         => array( 'M j, Y', 'add_default_setting' ),
                    'show_thumbnail'    => array( 'on' ),
                    'show_content'      => array( 'off' ),
                    'show_more'         => array( 'off' ),
                    'show_author'       => array( 'on' ),
                    'show_date'         => array( 'on' ),
                    'show_categories'   => array( 'on' ),
                    'show_comments'     => array( 'off' ),
                    'show_pagination'   => array( 'on' ),
                    'offset_number'     => array( 0, 'only_default_setting' ),
                    'background_layout' => array( 'light' ),
                    'use_dropshadow'    => array( 'off' ),
                    'use_overlay'       => array( 'off' ),
                );

                $this->main_css_element = '%%order_class%% .et_pb_post';
                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__( 'Header', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} .entry-title",
                                'important' => 'all',
                            ),
                        ),
                        'meta' => array(
                            'label'    => esc_html__( 'Meta', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} .post-meta",
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__( 'Body', 'et_builder' ),
                            'css'      => array(
                                'color'        => "{$this->main_css_element}, {$this->main_css_element} .post-content *",
                                'line_height' => "{$this->main_css_element} p",
                            ),
                        ),
                    ),
                    'border' => array(),
                );
                $this->custom_css_options = array(
                    'title' => array(
                        'label'    => esc_html__( 'Title', 'et_builder' ),
                        'selector' => '.et_pb_post .entry-title',
                    ),
                    'post_meta' => array(
                        'label'    => esc_html__( 'Post Meta', 'et_builder' ),
                        'selector' => '.et_pb_post .post-meta',
                    ),
                    'pagenavi' => array(
                        'label'    => esc_html__( 'Pagenavi', 'et_builder' ),
                        'selector' => '.wp_pagenavi',
                    ),
                    'featured_image' => array(
                        'label'    => esc_html__( 'Featured Image', 'et_builder' ),
                        'selector' => '.et_pb_image_container',
                    ),
                    'read_more' => array(
                        'label'    => esc_html__( 'Read More Button', 'et_builder' ),
                        'selector' => '.et_pb_post .more-link',
                    ),
                );
            }

            function get_fields() {
                $fields = array(
                    'fullwidth' => array(
                        'label'             => esc_html__( 'Layout', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'on'  => esc_html__( 'Fullwidth', 'et_builder' ),
                            'off' => esc_html__( 'Grid', 'et_builder' ),
                            'two_cols' => esc_html__( 'Two columns', 'auxiell' ),
                            'examples_lev_1' => esc_html__( 'Examples liv 1', 'auxiell' ),
                            'examples_lev_2' => esc_html__( 'Examples liv 2', 'auxiell' ),
                            'examples_lev_3' => esc_html__( 'Examples liv 3', 'auxiell' )
                        ),
                        'affects'           => array(
                            '#et_pb_background_layout',
                            '#et_pb_use_dropshadow',
                            '#et_pb_masonry_tile_background_color',
                        ),
                        'description'        => esc_html__( 'Toggle between the various blog layout types.', 'et_builder' ),
                    ),
                    'posts_number' => array(
                        'label'             => esc_html__( 'Posts Number', 'et_builder' ),
                        'type'              => 'text',
                        'option_category'   => 'configuration',
                        'description'       => esc_html__( 'Choose how much posts you would like to display per page.', 'et_builder' ),
                    ),
                    'include_categories' => array(
                        'label'            => esc_html__( 'Include Categories', 'et_builder' ),
                        'renderer'         => 'et_builder_include_categories_option',
                        'option_category'  => 'basic_option',
                        'renderer_options' => array(
                            'use_terms' => false,
                        ),
                        'description'      => esc_html__( 'Choose which categories you would like to include in the feed.', 'et_builder' ),
                    ),
                    'meta_date' => array(
                        'label'             => esc_html__( 'Meta Date Format', 'et_builder' ),
                        'type'              => 'text',
                        'option_category'   => 'configuration',
                        'description'       => esc_html__( 'If you would like to adjust the date format, input the appropriate PHP date format here.', 'et_builder' ),
                    ),
                    'show_thumbnail' => array(
                        'label'             => esc_html__( 'Show Featured Image', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'This will turn thumbnails on and off.', 'et_builder' ),
                    ),
                    'show_content' => array(
                        'label'             => esc_html__( 'Content', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( 'Show Excerpt', 'et_builder' ),
                            'on'  => esc_html__( 'Show Content', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_show_more',
                        ),
                        'description'        => esc_html__( 'Showing the full content will not truncate your posts on the index page. Showing the excerpt will only display your excerpt text.', 'et_builder' ),
                    ),
                    'show_more' => array(
                        'label'             => esc_html__( 'Read More Button', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'off',
                        'description'       => esc_html__( 'Here you can define whether to show "read more" link after the excerpts or not.', 'et_builder' ),
                    ),
                    'show_author' => array(
                        'label'             => esc_html__( 'Show Author', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn on or off the author link.', 'et_builder' ),
                    ),
                    'show_date' => array(
                        'label'             => esc_html__( 'Show Date', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn the date on or off.', 'et_builder' ),
                    ),
                    'show_categories' => array(
                        'label'             => esc_html__( 'Show Categories', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn the category links on or off.', 'et_builder' ),
                    ),
                    'show_comments' => array(
                        'label'             => esc_html__( 'Show Comment Count', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn comment count on and off.', 'et_builder' ),
                    ),
                    'show_pagination' => array(
                        'label'             => esc_html__( 'Show Pagination', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn pagination on and off.', 'et_builder' ),
                    ),
                    'offset_number' => array(
                        'label'           => esc_html__( 'Offset Number', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'Choose how many posts you would like to offset by', 'et_builder' ),
                    ),
                    'use_overlay' => array(
                        'label'             => esc_html__( 'Featured Image Overlay', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_overlay_icon_color',
                            '#et_pb_hover_overlay_color',
                            '#et_pb_hover_icon',
                        ),
                        'description'       => esc_html__( 'If enabled, an overlay color and icon will be displayed when a visitors hovers over the featured image of a post.', 'et_builder' ),
                    ),
                    'overlay_icon_color' => array(
                        'label'             => esc_html__( 'Overlay Icon Color', 'et_builder' ),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Here you can define a custom color for the overlay icon', 'et_builder' ),
                    ),
                    'hover_overlay_color' => array(
                        'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Here you can define a custom color for the overlay', 'et_builder' ),
                    ),
                    'hover_icon' => array(
                        'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
                        'type'                => 'text',
                        'option_category'     => 'configuration',
                        'class'               => array( 'et-pb-font-icon' ),
                        'renderer'            => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'depends_show_if'     => 'on',
                        'description'         => esc_html__( 'Here you can define a custom icon for the overlay', 'et_builder' ),
                    ),
                    'background_layout' => array(
                        'label'       => esc_html__( 'Text Color', 'et_builder' ),
                        'type'        => 'select',
                        'option_category' => 'color_option',
                        'options'           => array(
                            'light' => esc_html__( 'Dark', 'et_builder' ),
                            'dark'  => esc_html__( 'Light', 'et_builder' ),
                        ),
                        'depends_default' => true,
                        'description' => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder' ),
                    ),
                    'masonry_tile_background_color' => array(
                        'label'             => esc_html__( 'Grid Tile Background Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'tab_slug'          => 'advanced',
                        'depends_show_if'   => 'off',
                    ),
                    'use_dropshadow' => array(
                        'label'             => esc_html__( 'Use Dropshadow', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'tab_slug'          => 'advanced',
                        'depends_show_if'   => 'off',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            function shortcode_callback( $atts, $content = null, $function_name )
            {
                $module_id = $this->shortcode_atts['module_id'];
                $module_class = $this->shortcode_atts['module_class'];
                $fullwidth = $this->shortcode_atts['fullwidth'];
                $posts_number = $this->shortcode_atts['posts_number'];
                $include_categories = $this->shortcode_atts['include_categories'];
                $meta_date = $this->shortcode_atts['meta_date'];
                $show_thumbnail = $this->shortcode_atts['show_thumbnail'];
                $show_content = $this->shortcode_atts['show_content'];
                $show_author = $this->shortcode_atts['show_author'];
                $show_date = $this->shortcode_atts['show_date'];
                $show_categories = $this->shortcode_atts['show_categories'];
                $show_comments = $this->shortcode_atts['show_comments'];
                $show_pagination = $this->shortcode_atts['show_pagination'];
                $background_layout = $this->shortcode_atts['background_layout'];
                $show_more = $this->shortcode_atts['show_more'];
                $offset_number = $this->shortcode_atts['offset_number'];
                $masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
                $use_dropshadow = $this->shortcode_atts['use_dropshadow'];
                $overlay_icon_color = $this->shortcode_atts['overlay_icon_color'];
                $hover_overlay_color = $this->shortcode_atts['hover_overlay_color'];
                $hover_icon = $this->shortcode_atts['hover_icon'];
                $use_overlay = $this->shortcode_atts['use_overlay'];

                global $paged;

                $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

                $container_is_closed = false;

                // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
                remove_all_filters('wp_audio_shortcode_library');
                remove_all_filters('wp_audio_shortcode');
                remove_all_filters('wp_audio_shortcode_class');

                if ('' !== $masonry_tile_background_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%%.et_pb_blog_grid .et_pb_post',
                        'declaration' => sprintf(
                            'background-color: %1$s;',
                            esc_html($masonry_tile_background_color)
                        ),
                    ));
                }

                if ('' !== $overlay_icon_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%% .et_overlay:before',
                        'declaration' => sprintf(
                            'color: %1$s !important;',
                            esc_html($overlay_icon_color)
                        ),
                    ));
                }

                if ('' !== $hover_overlay_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%% .et_overlay',
                        'declaration' => sprintf(
                            'background-color: %1$s;',
                            esc_html($hover_overlay_color)
                        ),
                    ));
                }

                if ('on' === $use_overlay) {
                    $data_icon = '' !== $hover_icon
                        ? sprintf(
                            ' data-icon="%1$s"',
                            esc_attr(et_pb_process_font_icon($hover_icon))
                        )
                        : '';

                    $overlay_output = sprintf(
                        '<span class="et_overlay%1$s"%2$s></span>',
                        ('' !== $hover_icon ? ' et_pb_inline_icon' : ''),
                        $data_icon
                    );
                }

                $overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

                if ('on' !== $fullwidth) {
                    if ('on' === $use_dropshadow) {
                        $module_class .= ' et_pb_blog_grid_dropshadow';
                    }

                    wp_enqueue_script('salvattore');

                    $background_layout = 'light';
                }

                $args = array('posts_per_page' => (int)$posts_number);

                $et_paged = is_front_page() ? get_query_var('page') : get_query_var('paged');

                if (is_front_page()) {
                    $paged = $et_paged;
                }

                if ('' !== $include_categories)
                    $args['cat'] = $include_categories;

                if (!is_search()) {
                    $args['paged'] = $et_paged;
                }

                if ('' !== $offset_number && !empty($offset_number)) {
                    /**
                     * Offset + pagination don't play well. Manual offset calculation required
                     * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
                     */
                    if ($paged > 1) {
                        $args['offset'] = (($et_paged - 1) * intval($posts_number)) + intval($offset_number);
                    } else {
                        $args['offset'] = intval($offset_number);
                    }
                }

                if (is_single() && !isset($args['post__not_in'])) {
                    $args['post__not_in'] = array(get_the_ID());
                }

                ob_start();

                query_posts($args);

                if (have_posts()) {
                    while (have_posts()) {
                        the_post();

                        $post_format = et_pb_post_format();

                        $thumb = '';

                        $width = 'on' === $fullwidth ? 1080 : 400;
                        $width = (int)apply_filters('et_pb_blog_image_width', $width);

                        $height = 'on' === $fullwidth ? 675 : 250;
                        $height = (int)apply_filters('et_pb_blog_image_height', $height);
                        $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
                        $titletext = get_the_title();
                        $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                        $thumb = $thumbnail["thumb"];

                        $no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

                        if (in_array($post_format, array('video', 'gallery'))) {
                            $no_thumb_class = '';
                        } ?>

                        <article
                            id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post' . $no_thumb_class . $overlay_class); ?>>

                            <?php
                            et_divi_post_format_content();

                            if (!in_array($post_format, array('link', 'audio', 'quote'))) {
                                if ('video' === $post_format && false !== ($first_video = et_get_first_video())) :
                                    printf(
                                        '<div class="et_main_video_container">
								%1$s
							</div>',
                                        $first_video
                                    );
                                elseif ('gallery' === $post_format) :
                                    et_pb_gallery_images('slider');
                                elseif ('' !== $thumb && 'on' === $show_thumbnail) :
                                    if ('on' !== $fullwidth) echo '<div class="et_pb_image_container">'; ?>
                                    <a href="<?php esc_url(the_permalink()); ?>" class="entry-featured-image-url">
                                        <?php print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height); ?>
                                        <?php if ('on' === $use_overlay) {
                                            echo $overlay_output;
                                        } ?>
                                    </a>
                                    <?php
                                    if ('on' !== $fullwidth) echo '</div> <!-- .et_pb_image_container -->';
                                endif;
                            } ?>

                            <?php if ('off' === $fullwidth || !in_array($post_format, array('link', 'audio', 'quote'))) { ?>
                                <?php if (!in_array($post_format, array('link', 'audio'))) { ?>
                                    <h2 class="entry-title"><a
                                            href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h2>
                                <?php } ?>

                                <?php
                                if ('on' === $show_author || 'on' === $show_date || 'on' === $show_categories || 'on' === $show_comments) {
                                    printf('<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>',
                                        (
                                        'on' === $show_author
                                            ? et_get_safe_localization(sprintf(__('by %s', 'et_builder'), '<span class="author vcard">' . et_pb_get_the_author_posts_link() . '</span>'))
                                            : ''
                                        ),
                                        (
                                        ('on' === $show_author && 'on' === $show_date)
                                            ? ' | '
                                            : ''
                                        ),
                                        (
                                        'on' === $show_date
                                            ? et_get_safe_localization(sprintf(__('%s', 'et_builder'), '<span class="published">' . esc_html(get_the_date($meta_date)) . '</span>'))
                                            : ''
                                        ),
                                        (
                                        (('on' === $show_author || 'on' === $show_date) && 'on' === $show_categories)
                                            ? ' | '
                                            : ''
                                        ),
                                        (
                                        'on' === $show_categories
                                            ? get_the_category_list(', ')
                                            : ''
                                        ),
                                        (
                                        (('on' === $show_author || 'on' === $show_date || 'on' === $show_categories) && 'on' === $show_comments)
                                            ? ' | '
                                            : ''
                                        ),
                                        (
                                        'on' === $show_comments
                                            ? sprintf(esc_html(_nx('1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder')), number_format_i18n(get_comments_number()))
                                            : ''
                                        )
                                    );
                                }

                                echo '<div class="post-content">';

                                $post_content = get_the_content();

                                // do not display the content if it contains Blog, Post Slider, Fullwidth Post Slider, or Portfolio modules to avoid infinite loops
                                if (!has_shortcode($post_content, 'et_pb_blog') && !has_shortcode($post_content, 'et_pb_portfolio') && !has_shortcode($post_content, 'et_pb_post_slider') && !has_shortcode($post_content, 'et_pb_fullwidth_post_slider')) {
                                    if ('on' === $show_content) {
                                        global $more;

                                        // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                                        if (et_pb_is_pagebuilder_used(get_the_ID())) {
                                            $more = 1;
                                            the_content();
                                        } else {
                                            $more = null;
                                            the_content(esc_html__('read more...', 'et_builder'));
                                        }
                                    } else {
                                        if (has_excerpt()) {
                                            the_excerpt();
                                        } else {
                                            echo wpautop(truncate_post(270, false));
                                        }
                                    }
                                } else if (has_excerpt()) {
                                    the_excerpt();
                                }

                                if ('on' !== $show_content) {
                                    $more = 'on' == $show_more ? sprintf(' <a href="%1$s" class="more-link" >%2$s</a>', esc_url(get_permalink()), esc_html__('read more', 'et_builder')) : '';
                                    echo $more;
                                }

                                echo '</div>';
                                ?>
                            <?php } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery' ?>

                        </article> <!-- .et_pb_post -->
                        <?php
                    } // endwhile

                    if ('on' === $show_pagination && !is_search()) {
                        echo '</div> <!-- .et_pb_posts -->';

                        $container_is_closed = true;

                        if (function_exists('wp_pagenavi')) {
                            wp_pagenavi();
                        } else {
                            if (et_is_builder_plugin_active()) {
                                include(ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php');
                            } else {
                                get_template_part('includes/navigation', 'index');
                            }
                        }
                    }

                    wp_reset_query();
                } else {
                    if (et_is_builder_plugin_active()) {
                        include(ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php');
                    } else {
                        get_template_part('includes/no-results', 'index');
                    }
                }

                $posts = ob_get_contents();

                ob_end_clean();


                if(!function_exists('get_two_cols_post_format')){
                    function get_two_cols_post_format($args)
                    {
                        // USING BUILDER QUERY TO GET POSTS
                        query_posts($args);

                        $twoColsMarkup = '';
                        if (have_posts()) {
                            while (have_posts()) {
                                the_post();
                                $thisID = get_the_ID();
                                $title = get_the_title();
                                $thumb = get_the_post_thumbnail();
                                $thumbUrl = get_the_post_thumbnail_url();
                                $date = get_the_date();
                                $permalink = get_the_permalink();
                                $abstract = custom_get_excerpt(100, "content");
                                /*$abstract = get_field('abstract', $thisID);*/

                                if (empty($thumb)) {
                                    $twoColsMarkup .= sprintf('
                                <article class="col-xs-12 col-sm-6">
                                <div class="article-wrapper">
                                <a href="%4$s" target="_blank">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-8">
                                          <div class="date">%2$s</div>
                                          <h5>%1$s</h5>
                                          <div class="abstract">%3$s</div>
                                        </div>
                                     </div>
                                     </a>
                                     </div>
                                </article>',
                                        $title,
                                        $date,
                                        $abstract,
                                        $permalink
                                    );
                                } else {
                                    $twoColsMarkup .= sprintf('
                                <article class="col-xs-12 col-sm-6">
                                <div class="article-wrapper">
                                <a href="%6$s" target="_blank">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <div class="img-wrapper">
                                                 %2$s
                                                <div class="overlay-image" style="background-image: url(%5$s);"></div>
                                             </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                          <div class="date">%3$s</div>
                                          <h5>%1$s</h5>
                                          <div class="abstract">%4$s</div>
                                        </div>
                                     </div>
                                     </a>
                                     </div>
                                </article>',
                                        $title,
                                        $thumb,
                                        $date,
                                        $abstract,
                                        $thumbUrl,
                                    $permalink
                                    );
                                }
                            }
                        }

                        wp_reset_query();

                        return sprintf('
                    <div class="container-fluid">
                    <div class="row" id="auxiell-two-col-grid">
                       %1$s
                    </div>
                    </div>',
                            $twoColsMarkup);
                    }
                }

                if(!function_exists('get_example_lev_1_post_format')){

                    function get_example_lev_1_post_format(){

                            $currentLang = pll_current_language();
                        $catExtension = ( 'it'!=$currentLang ? '-'.$currentLang : '' );

                        $args = array(
                            'post_type'     => 'examples-auxiell',
                            'category_name' => 'lev-1-examples'.$catExtension,
                            'lang'          => $currentLang,
                        );

                        // USING CUSTOM QUERY TO GET POSTS
                        $lev1Ex = new WP_Query($args);
                        $exLev1Markup = '';
                        $containerType = 'container-fluid';
                        $singleArticleClasses = 'col-xs-12 col-sm-6';

                        if( $lev1Ex->post_count === 1 ){
                            $containerType = 'container';
                            $singleArticleClasses = 'col-xs-12 lonely-example';
                        }

                        if ( $lev1Ex->have_posts() ) {
                            while ( $lev1Ex->have_posts() ) {
                                $lev1Ex->the_post();
                                $thisID = get_the_ID();

                                $title = get_the_title($thisID);
                                $videoTitle = str_replace(' ', '-', strtolower($title));
                                $thumb = get_the_post_thumbnail($thisID);
                                $thumbUrl = get_the_post_thumbnail_url($thisID);
                                $content = get_the_content($thisID);
                                $link = get_the_permalink($thisID);
                                $abstract = get_field('example_abstract', $thisID);
                                $videoBg = get_field('video_id', $thisID);

                                $ytVideoMarkup = sprintf('<div id="yt-player-%1$s" class="yt-player" data-ytvideo-id="%2$s" data-ytvideo-name="%1$s"></div>',
                                    $videoTitle,
                                    $videoBg,
                                    $videoTitle
                                    );

                                $exLev1Markup .= sprintf('
                                        <article class="%7$s single-example">
                                         <div class="img-wrapper">
                                                %2$s
                                                <div class="overlay-image" style="background-image: url(%5$s);"></div>
                                            </div>
                                            <div class="article-wrapper vertical">
                                            <div class="post-meta-wrapper vertical-middle">
                                                <h3>%1$s</h3>
                                                <div class="abstract">%4$s</div>
                                                <div class="btn-wrapper">
                                                %6$s%3$s
                                                </div>
                                                </div>
                                             </div>
                                            </article>',
                                    $title,
                                    $thumb,
                                    $permalink = (!empty($content) ? '<a href="' . $link . '" class="btn-std btn-blurb btn-opacity" target="_blank">'.pll__('Scopri').'</a>' : ''),
                                    $abstract,
                                    $thumbUrl,
                                    $videoBtn = (!empty($videoBg) ? '<a href="https://youtu.be/' . $videoBg . '" class="btn-std btn-white btn-play btn-blur et_pb_play_video_button example-video-button">'.pll__('Guarda il video').'</a>'.$ytVideoMarkup : ''),
                                    $singleArticleClasses
                                );
                            }

                            wp_reset_query();

                            return sprintf('
                    <div class="%2$s">
                    <div class="row lev-1-examples-grid">
                       %1$s
                    </div>
                    </div>
                    </div>',
                                $exLev1Markup,
                                $containerType);
                        }
                    }
                }

                if(!function_exists('get_example_lev_2_post_format')){
                    function get_example_lev_2_post_format() {
                        $currentLang = pll_current_language();
                        $catExtension = ( 'it'!=$currentLang ? '-'.$currentLang : '' );

                        $args = array(
                            'post_type' => 'examples-auxiell',
                            'category_name' => 'lev-2-examples'.$catExtension,
                            'lang'          => $currentLang,
                        );
                        // USING CUSTOM QUERY TO GET POSTS
                        $lev2Ex = new WP_Query($args);

                        $exLev2Markup = '';
                        if ( $lev2Ex->have_posts() ) {
                            while ( $lev2Ex->have_posts() ) {
                                $lev2Ex->the_post();
                                $thisID = get_the_ID();
                                $title = get_the_title($thisID);
                                $thumb = get_the_post_thumbnail($thisID);
                                $thumbUrl = get_the_post_thumbnail_url($thisID);
                                $content = get_the_content($thisID);
                                $link = get_the_permalink($thisID);
                                $abstract = custom_get_excerpt(200, "content");

                                /*$abstract = get_field('example_abstract', $thisID);*/
                                if (!empty($content)) {
                                    $permalink = '<a href="' . $link . '" class="btn-std btn-discover" target="_blank">SCOPRI</a>';
                                } else {
                                    $permalink = '';
                                }

                                $exLev2Markup .= sprintf('
                                <article class="col-xs-12 col-sm-4">
                                <div class="article-wrapper">
                             <a href="%6$s">  
                                  <div class="img-wrapper">
                                        %2$s
                                        <div class="overlay-image" style="background-image: url(%5$s)"></div>
                                    </div>
                                    </a>
                                    <div class="post-meta-wrapper">
                                        <a href="%6$s"><h5>%1$s</h5></a>
                                        <div class="abstract"><p>%4$s</p></div>
                                        %3$s
                                    </div>
                                    </div>
                                </article>',
                                    $title,
                                    $thumb,
                                    $permalink,
                                    $abstract,
                                    $thumbUrl,
                                    $link
                                );
                            }
                        }

                        wp_reset_query();

                        return sprintf('
                    <div class="container-fluid">
                    <div class="row lev-2-examples-grid">
                       %1$s
                       </div>
                    </div>
                    </div>',
                            $exLev2Markup);
                    }
                }

                if(!function_exists('get_example_lev_3_post_format')){
                    function get_example_lev_3_post_format(){
                        $args = array(
                            'post_type' => 'examples-auxiell',
                            'category_name' => 'lev-3-examples',
                            'lang' => 'it',
                            'orderby' => 'title',
                            'order' => 'ASC'
                        );
                        // USING CUSTOM QUERY TO GET POSTS
                        $lev3Ex = new WP_Query($args);

                        $sliderMarkup ='';
                        $callSeeMore = function_exists('pll__') ? pll__('View more') : 'View More';
                        if ( $lev3Ex->have_posts() ) {
                            $sliderMarkup = '<h4 class="with-after">'.pll__('Referenze').'</h4><ul id="examples-slider">';
                            while ( $lev3Ex->have_posts()) {
                                $lev3Ex->the_post();
                                $thisID = get_the_ID();
                                $exampleUrl = get_field('ext_link', $thisID);
                                $logo = get_field('example_logo', $thisID);

                                $sliderMarkup .= sprintf('
            <li>
                <a href="%3$s" target="_blank">
                    <div class="vertical">
                        <div class="vertical-middle">
                            <img src="%1$s" alt="%2$s"/>
                        </div>
                    </div>
                </a>
            </li>',
                                    $logo['url'],
                                    $logo['alt'],
                                    $exampleUrl
                                );
                            }
                            $sliderMarkup .= '</ul></div>';
                        }

                        wp_reset_postdata();

                        return $sliderMarkup;
                    }
                }

                $fullwidthClass = '';

                switch($fullwidth){
                    case 'on':
                        $fullwidthClass = 'et_pb_posts';
                        break;
                    case 'off':
                        $fullwidthClass = 'et_pb_blog_grid clearfix';
                        break;
                    case 'two_cols':
                        $fullwidthClass = 'et_pb_blog_two_col';
                        $posts = get_two_cols_post_format($args);
                        break;
                    case 'examples_lev_1':
                        $fullwidthClass = 'et_pb_blog_example_lev_1';
                        $posts = get_example_lev_1_post_format();
                        break;
                    case 'examples_lev_2':
                        $fullwidthClass = 'et_pb_blog_example_lev_2';
                        $posts = get_example_lev_2_post_format();
                        break;

                    case 'examples_lev_3':
                        $fullwidthClass = 'et_pb_blog_example_lev_3';
                        $posts = get_example_lev_3_post_format();
                        break;
                }

                $class = " et_pb_module et_pb_bg_layout_{$background_layout}";

                $output = sprintf(
                    '<div%5$s class="%1$s%3$s%6$s"%7$s>
				%2$s
			%4$s',
                    $fullwidthClass,
                    $posts,
                    esc_attr( $class ),
                    ( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                    ( 'on' !== $fullwidth ? ' data-columns' : '' )
                );

                if ( 'on' !== $fullwidth )
                    $output = sprintf( '<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output );

                return $output;
            }
        }
        $et_blog_section_auxiell = new ET_Builder_Module_Blog_AUXIELL;
        remove_shortcode( 'et_pb_blog' );
        add_shortcode( 'et_pb_blog', array($et_blog_section_auxiell, '_shortcode_callback') );

        class ET_Builder_Module_Blurb_AUXIELL extends ET_Builder_Module {
            function init() {
                $this->name = esc_html__( 'Blurb', 'auxiell' );
                $this->slug = 'et_pb_blurb';
                $this->main_css_element = '%%order_class%%.et_pb_blurb';

                $this->whitelisted_fields = array(
                    'title',
                    'url',
                    'url_new_window',
                    'use_icon',
                    'font_icon',
                    'icon_color',
                    'use_circle',
                    'circle_color',
                    'use_circle_border',
                    'circle_border_color',
                    'image',
                    'alt',
                    'is_sprite',
                    'icon_placement',
                    'animation',
                    'background_layout',
                    'text_orientation',
                    'content_new',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'max_width',
                    'use_icon_font_size',
                    'icon_font_size',
                    'max_width_tablet',
                    'max_width_phone',
                    'icon_font_size_tablet',
                    'icon_font_size_phone',
                );

                $et_accent_color = et_builder_accent_color();

                $this->fields_defaults = array(
                    'url_new_window'      => array( 'off' ),
                    'use_icon'            => array( 'off' ),
                    'icon_color'          => array( $et_accent_color, 'add_default_setting' ),
                    'use_circle'          => array( 'off' ),
                    'circle_color'        => array( $et_accent_color, 'only_default_setting' ),
                    'use_circle_border'   => array( 'off' ),
                    'circle_border_color' => array( $et_accent_color, 'only_default_setting' ),
                    'is_sprite'          => array('off'),
                    'icon_placement'      => array( 'top' ),
                    'animation'           => array( 'top' ),
                    'background_layout'   => array( 'light' ),
                    'text_orientation'    => array( 'center' ),
                    'use_icon_font_size'  => array( 'off' ),
                );

                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__( 'Header', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} h4, {$this->main_css_element} h4 a",
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__( 'Body', 'et_builder' ),
                            'css'      => array(
                                'line_height' => "{$this->main_css_element} p",
                            ),
                        ),
                    ),
                    'background' => array(
                        'settings' => array(
                            'color' => 'alpha',
                        ),
                    ),
                    'border' => array(),
                    'custom_margin_padding' => array(
                        'css' => array(
                            'important' => 'all',
                        ),
                    ),
                );
                $this->custom_css_options = array(
                    'blurb_image' => array(
                        'label'    => esc_html__( 'Blurb Image', 'et_builder' ),
                        'selector' => '.et_pb_main_blurb_image',
                    ),
                    'blurb_title' => array(
                        'label'    => esc_html__( 'Blurb Title', 'et_builder' ),
                        'selector' => 'h4',
                    ),
                    'blurb_content' => array(
                        'label'    => esc_html__( 'Blurb Content', 'et_builder' ),
                        'selector' => '.et_pb_blurb_content',
                    ),
                );
            }

            function get_fields() {
                $et_accent_color = et_builder_accent_color();

                $image_icon_placement = array(
                    'top' => esc_html__( 'Top', 'et_builder' ),
                );

                if ( ! is_rtl() ) {
                    $image_icon_placement['left'] = esc_html__( 'Left', 'et_builder' );
                } else {
                    $image_icon_placement['right'] = esc_html__( 'Right', 'et_builder' );
                }

                $fields = array(
                    'title' => array(
                        'label'           => esc_html__( 'Title', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'The title of your blurb will appear in bold below your blurb image.', 'et_builder' ),
                    ),
                    'url' => array(
                        'label'           => esc_html__( 'Url', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'If you would like to make your blurb a link, input your destination URL here.', 'et_builder' ),
                    ),
                    'url_new_window' => array(
                        'label'           => esc_html__( 'Url Opens', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'In The Same Window', 'et_builder' ),
                            'on'  => esc_html__( 'In The New Tab', 'et_builder' ),
                        ),
                        'description' => esc_html__( 'Here you can choose whether or not your link opens in a new window', 'et_builder' ),
                    ),
                    'use_icon' => array(
                        'label'           => esc_html__( 'Use Icon', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'basic_option',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'     => array(
                            '#et_pb_font_icon',
                            '#et_pb_use_circle',
                            '#et_pb_icon_color',
                            '#et_pb_image',
                            '#et_pb_alt',
                        ),
                        'description' => esc_html__( 'Here you can choose whether icon set below should be used.', 'et_builder' ),
                    ),
                    'font_icon' => array(
                        'label'               => esc_html__( 'Icon', 'et_builder' ),
                        'type'                => 'text',
                        'option_category'     => 'basic_option',
                        'class'               => array( 'et-pb-font-icon' ),
                        'renderer'            => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'description'         => esc_html__( 'Choose an icon to display with your blurb.', 'et_builder' ),
                        'depends_default'     => true,
                    ),
                    'icon_color' => array(
                        'label'             => esc_html__( 'Icon Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'description'       => esc_html__( 'Here you can define a custom color for your icon.', 'et_builder' ),
                        'depends_default'   => true,
                    ),
                    'use_circle' => array(
                        'label'           => esc_html__( 'Circle Icon', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_use_circle_border',
                            '#et_pb_circle_color',
                        ),
                        'description' => esc_html__( 'Here you can choose whether icon set above should display within a circle.', 'et_builder' ),
                        'depends_default'   => true,
                    ),
                    'circle_color' => array(
                        'label'           => esc_html__( 'Circle Color', 'et_builder' ),
                        'type'            => 'color',
                        'description'     => esc_html__( 'Here you can define a custom color for the icon circle.', 'et_builder' ),
                        'depends_default' => true,
                    ),
                    'use_circle_border' => array(
                        'label'           => esc_html__( 'Show Circle Border', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'layout',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_circle_border_color',
                        ),
                        'description' => esc_html__( 'Here you can choose whether if the icon circle border should display.', 'et_builder' ),
                        'depends_default'   => true,
                    ),
                    'circle_border_color' => array(
                        'label'           => esc_html__( 'Circle Border Color', 'et_builder' ),
                        'type'            => 'color',
                        'description'     => esc_html__( 'Here you can define a custom color for the icon circle border.', 'et_builder' ),
                        'depends_default' => true,
                    ),
                    'is_sprite' => array(
                        'label'           => esc_html__( 'Activate sprite animation', 'auxiell' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'basic_option',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description' => esc_html__( 'Here you can choose whether if the image is a sprite animation', 'auxiell' ),
                    ),
                    'image' => array(
                        'label'              => esc_html__( 'Image', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose an Image', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Image', 'et_builder' ),
                        'depends_show_if'    => 'off',
                        'description'        => esc_html__( 'Upload an image to display at the top of your blurb.', 'et_builder' ),
                    ),
                    'alt' => array(
                        'label'           => esc_html__( 'Image Alt Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the HTML ALT text for your image here.', 'et_builder' ),
                        'depends_show_if' => 'off',
                    ),
                    'icon_placement' => array(
                        'label'             => esc_html__( 'Image/Icon Placement', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => $image_icon_placement,
                        'description'       => esc_html__( 'Here you can choose where to place the icon.', 'et_builder' ),
                    ),
                    'animation' => array(
                        'label'             => esc_html__( 'Image/Icon Animation', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'top'    => esc_html__( 'Top To Bottom', 'et_builder' ),
                            'left'   => esc_html__( 'Left To Right', 'et_builder' ),
                            'right'  => esc_html__( 'Right To Left', 'et_builder' ),
                            'bottom' => esc_html__( 'Bottom To Top', 'et_builder' ),
                            'off'    => esc_html__( 'No Animation', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'This controls the direction of the lazy-loading animation.', 'et_builder' ),
                    ),
                    'background_layout' => array(
                        'label'             => esc_html__( 'Text Color', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'color_option',
                        'options'           => array(
                            'light' => esc_html__( 'Dark', 'et_builder' ),
                            'dark'  => esc_html__( 'Light', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder' ),
                    ),
                    'text_orientation' => array(
                        'label'             => esc_html__( 'Text Orientation', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => et_builder_get_text_orientation_options(),
                        'description'       => esc_html__( 'This will control how your blurb text is aligned.', 'et_builder' ),
                    ),
                    'content_new' => array(
                        'label'             => esc_html__( 'Content', 'et_builder' ),
                        'type'              => 'tiny_mce',
                        'option_category'   => 'basic_option',
                        'description'       => esc_html__( 'Input the main text content for your module here.', 'et_builder' ),
                    ),
                    'max_width' => array(
                        'label'           => esc_html__( 'Image Max Width', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'layout',
                        'tab_slug'        => 'advanced',
                        'mobile_options'  => true,
                        'validate_unit'   => true,
                    ),
                    'use_icon_font_size' => array(
                        'label'           => esc_html__( 'Use Icon Font Size', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'font_option',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'     => array(
                            '#et_pb_icon_font_size',
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'icon_font_size' => array(
                        'label'           => esc_html__( 'Icon Font Size', 'et_builder' ),
                        'type'            => 'range',
                        'option_category' => 'font_option',
                        'tab_slug'        => 'advanced',
                        'default'         => '96px',
                        'range_settings' => array(
                            'min'  => '1',
                            'max'  => '120',
                            'step' => '1',
                        ),
                        'mobile_options'  => true,
                        'depends_default' => true,
                    ),
                    'max_width_tablet' => array (
                        'type' => 'skip',
                    ),
                    'max_width_phone' => array (
                        'type' => 'skip',
                    ),
                    'icon_font_size_tablet' => array(
                        'type' => 'skip',
                    ),
                    'icon_font_size_phone' => array(
                        'type' => 'skip',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $module_id             = $this->shortcode_atts['module_id'];
                $module_class          = $this->shortcode_atts['module_class'];
                $title                 = $this->shortcode_atts['title'];
                $url                   = $this->shortcode_atts['url'];
                $image                 = $this->shortcode_atts['image'];
                $url_new_window        = $this->shortcode_atts['url_new_window'];
                $alt                   = $this->shortcode_atts['alt'];
                $isSprite              = $this->shortcode_atts['is_sprite'];
                $background_layout     = $this->shortcode_atts['background_layout'];
                $text_orientation      = $this->shortcode_atts['text_orientation'];
                $animation             = $this->shortcode_atts['animation'];
                $icon_placement        = $this->shortcode_atts['icon_placement'];
                $font_icon             = $this->shortcode_atts['font_icon'];
                $use_icon              = $this->shortcode_atts['use_icon'];
                $use_circle            = $this->shortcode_atts['use_circle'];
                $use_circle_border     = $this->shortcode_atts['use_circle_border'];
                $icon_color            = $this->shortcode_atts['icon_color'];
                $circle_color          = $this->shortcode_atts['circle_color'];
                $circle_border_color   = $this->shortcode_atts['circle_border_color'];
                $max_width             = $this->shortcode_atts['max_width'];
                $max_width_tablet      = $this->shortcode_atts['max_width_tablet'];
                $max_width_phone       = $this->shortcode_atts['max_width_phone'];
                $use_icon_font_size    = $this->shortcode_atts['use_icon_font_size'];
                $icon_font_size        = $this->shortcode_atts['icon_font_size'];
                $icon_font_size_tablet = $this->shortcode_atts['icon_font_size_tablet'];
                $icon_font_size_phone  = $this->shortcode_atts['icon_font_size_phone'];

                $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
                $imageUrl = $image;

                if ( 'off' !== $use_icon_font_size ) {
                    $font_size_values = array(
                        'desktop' => $icon_font_size,
                        'tablet'  => $icon_font_size_tablet,
                        'phone'   => $icon_font_size_phone,
                    );

                    et_pb_generate_responsive_css( $font_size_values, '%%order_class%% .et-pb-icon', 'font-size', $function_name );
                }

                if ( '' !== $max_width_tablet || '' !== $max_width_phone || '' !== $max_width ) {
                    $max_width_values = array(
                        'desktop' => $max_width,
                        'tablet'  => $max_width_tablet,
                        'phone'   => $max_width_phone,
                    );

                    et_pb_generate_responsive_css( $max_width_values, '%%order_class%% .et_pb_main_blurb_image img', 'max-width', $function_name );
                }

                if ( is_rtl() && 'left' === $text_orientation ) {
                    $text_orientation = 'right';
                }

                if ( is_rtl() && 'left' === $icon_placement ) {
                    $icon_placement = 'right';
                }

                if ( '' !== $title && '' !== $url ) {
                    $title = sprintf( '<a href="%1$s"%3$s>%2$s</a>',
                        esc_url( $url ),
                        $title,
                        ( 'on' === $url_new_window ? ' target="_blank"' : '' )
                    );
                }

                if ( '' !== $title ) {
                    $title = "<h4>{$title}</h4>";
                }

                if ( '' !== trim( $image ) || '' !== $font_icon ) {
                    if ( 'off' === $use_icon ) {
                        $image = sprintf(
                            '<img src="%1$s" alt="%2$s" class="et-waypoint%3$s" />',
                            esc_url( $image ),
                            esc_attr( $alt ),
                            esc_attr( " et_pb_animation_{$animation}" )
                        );
                    } else {
                        $icon_style = sprintf( 'color: %1$s;', esc_attr( $icon_color ) );

                        if ( 'on' === $use_circle ) {
                            $icon_style .= sprintf( ' background-color: %1$s;', esc_attr( $circle_color ) );

                            if ( 'on' === $use_circle_border ) {
                                $icon_style .= sprintf( ' border-color: %1$s;', esc_attr( $circle_border_color ) );
                            }
                        }

                        $image = sprintf(
                            '<span class="et-pb-icon et-waypoint%2$s%3$s%4$s" style="%5$s">%1$s</span>',
                            esc_attr( et_pb_process_font_icon( $font_icon ) ),
                            esc_attr( " et_pb_animation_{$animation}" ),
                            ( 'on' === $use_circle ? ' et-pb-icon-circle' : '' ),
                            ( 'on' === $use_circle && 'on' === $use_circle_border ? ' et-pb-icon-circle-border' : '' ),
                            $icon_style
                        );
                    }

                    $spriteMarkup = '';
                    $spriteClasses = '';
                    if($isSprite != 'off'){
                        $spriteMarkup = 'data-source="'.trim($imageUrl).'"';
                        $spriteClasses = 'sprite-icon';
                        $spriteUrl = $imageUrl;
                    }

                    $image = sprintf(
                        '<div %2$s class="et_pb_main_blurb_image %3$s">%1$s</div>',
                        ( '' !== $url
                            ? sprintf(
                                '<a href="%1$s"%3$s>%2$s</a>',
                                esc_url( $url ),
                                $image,
                                ( 'on' === $url_new_window ? ' target="_blank"' : '' )
                            )
                            : $image
                        ),
                        $spriteMarkup,
                        $spriteClasses
                    );
                }

                $class = " et_pb_module et_pb_bg_layout_{$background_layout} et_pb_text_align_{$text_orientation}";

                $output = sprintf(
                    '<div%5$s class="et_pb_blurb%4$s%6$s%7$s">
				<div class="et_pb_blurb_content">
					%2$s
					<div class="et_pb_blurb_container">
						%3$s
						%1$s
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->',
                    $this->shortcode_content,
                    $image,
                    $title,
                    esc_attr( $class ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                    sprintf( ' et_pb_blurb_position_%1$s', esc_attr( $icon_placement ) )
                );

                return $output;
            }
        }
        $et_blurb_auxiell = new ET_Builder_Module_Blurb_AUXIELL;
        remove_shortcode( 'et_pb_blurb' );
        add_shortcode( 'et_pb_blurb', array($et_blurb_auxiell, '_shortcode_callback') );

        class ET_Builder_Module_Slider_Single_Background extends ET_Builder_Module {
            function init() {
                $this->name            = esc_html__( 'Slider Single Background AUXIELL', 'auxiell' );
                $this->slug            = 'et_pb_slider_single_bg';
                $this->child_slug      = 'et_pb_slide_custom';
                $this->child_item_text = esc_html__( 'Slide Trasparent', 'et_builder' );

                $this->whitelisted_fields = array(
                    'show_arrows',
                    'show_pagination',
                    'auto',
                    'auto_speed',
                    'auto_ignore_hover',
                    'parallax',
                    'parallax_method',
                    'remove_inner_shadow',
                    'ctm_src',
                    'ctm_src_webm',
                    'ctm_video_attr',
                    'background_position',
                    'background_size',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'top_padding',
                    'bottom_padding',
                    'hide_content_on_mobile',
                    'hide_cta_on_mobile',
                    'show_image_video_mobile',
                    'top_padding_tablet',
                    'top_padding_phone',
                    'bottom_padding_tablet',
                    'bottom_padding_phone',
                );

                $this->fields_defaults = array(
                    'show_arrows'             => array( 'on' ),
                    'show_pagination'         => array( 'on' ),
                    'auto'                    => array( 'off' ),
                    'auto_speed'              => array( '7000' ),
                    'auto_ignore_hover'       => array( 'off' ),
                    'parallax'                => array( 'off' ),
                    'parallax_method'         => array( 'off' ),
                    'remove_inner_shadow'     => array( 'off' ),
                    'background_position'     => array( 'default' ),
                    'background_size'         => array( 'default' ),
                    'hide_content_on_mobile'  => array( 'off' ),
                    'hide_cta_on_mobile'      => array( 'off' ),
                    'show_image_video_mobile' => array( 'off' ),
                );

                $this->main_css_element = '%%order_class%%.et_pb_slider';
                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__( 'Header', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} .et_pb_slide_description .et_pb_slide_title",
                                'font_size_tablet' => "{$this->main_css_element} .et_pb_slides .et_pb_slide_description .et_pb_slide_title",
                                'font_size_phone'  => "{$this->main_css_element} .et_pb_slides .et_pb_slide_description .et_pb_slide_title",
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__( 'Body', 'et_builder' ),
                            'css'      => array(
                                'line_height' => "{$this->main_css_element}",
                                'main' => "{$this->main_css_element} .et_pb_slide_content",
                                'font_size_tablet' => "{$this->main_css_element} .et_pb_slides .et_pb_slide_content",
                                'font_size_phone' => "{$this->main_css_element} .et_pb_slides .et_pb_slide_content",
                            ),
                        ),
                    ),
                    'button' => array(
                        'button' => array(
                            'label' => esc_html__( 'Button', 'et_builder' ),
                        ),
                    ),
                );
                $this->custom_css_options = array(
                    'slide_description' => array(
                        'label'    => esc_html__( 'Slide Description', 'et_builder' ),
                        'selector' => '.et_pb_slide_description',
                    ),
                    'slide_title' => array(
                        'label'    => esc_html__( 'Slide Title', 'et_builder' ),
                        'selector' => '.et_pb_slide_description .et_pb_slide_title',
                    ),
                    'slide_button' => array(
                        'label'    => esc_html__( 'Slide Button', 'et_builder' ),
                        'selector' => '.et_pb_slider .et_pb_slide .et_pb_slide_description a.et_pb_more_button.et_pb_button',
                        'no_space_before_selector' => true,
                    ),
                    'slide_controllers' => array(
                        'label'    => esc_html__( 'Slide Controllers', 'et_builder' ),
                        'selector' => '.et-pb-controllers',
                    ),
                    'slide_active_controller' => array(
                        'label'    => esc_html__( 'Slide Active Controller', 'et_builder' ),
                        'selector' => '.et-pb-controllers .et-pb-active-control',
                    ),
                    'slide_image' => array(
                        'label'    => esc_html__( 'Slide Image', 'et_builder' ),
                        'selector' => '.et_pb_slide_image',
                    ),
                    'slide_arrows' => array(
                        'label'    => esc_html__( 'Slide Arrows', 'et_builder' ),
                        'selector' => '.et-pb-slider-arrows a',
                    ),
                );
            }

            function get_fields() {
                $fields = array(
                    'show_arrows'         => array(
                        'label'           => esc_html__( 'Arrows', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'on'  => esc_html__( 'Show Arrows', 'et_builder' ),
                            'off' => esc_html__( 'Hide Arrows', 'et_builder' ),
                        ),
                        'description'     => esc_html__( 'This setting will turn on and off the navigation arrows.', 'et_builder' ),
                    ),
                    'show_pagination' => array(
                        'label'             => esc_html__( 'Show Controls', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'This setting will turn on and off the circle buttons at the bottom of the slider.', 'et_builder' ),
                    ),
                    'auto' => array(
                        'label'           => esc_html__( 'Automatic Animation', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'affects' => array(
                            '#et_pb_auto_speed, #et_pb_auto_ignore_hover',
                        ),
                        'description'        => esc_html__( 'If you would like the slider to slide automatically, without the visitor having to click the next button, enable this option and then adjust the rotation speed below if desired.', 'et_builder' ),
                    ),
                    'auto_speed' => array(
                        'label'             => esc_html__( 'Automatic Animation Speed (in ms)', 'et_builder' ),
                        'type'              => 'text',
                        'option_category'   => 'configuration',
                        'depends_default'   => true,
                        'description'       => esc_html__( "Here you can designate how fast the slider fades between each slide, if 'Automatic Animation' option is enabled above. The higher the number the longer the pause between each rotation.", 'et_builder' ),
                    ),
                    'auto_ignore_hover' => array(
                        'label'           => esc_html__( 'Continue Automatic Slide on Hover', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'depends_default' => true,
                        'options'         => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'description' => esc_html__( 'Turning this on will allow automatic sliding to continue on mouse hover.', 'et_builder' ),
                    ),
                    'parallax' => array(
                        'label'           => esc_html__( 'Use Parallax effect', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_parallax_method',
                            '#et_pb_background_position',
                            '#et_pb_background_size',
                        ),
                        'description'        => esc_html__( 'Enabling this option will give your background images a fixed position as you scroll.', 'et_builder' ),
                    ),
                    'parallax_method' => array(
                        'label'           => esc_html__( 'Parallax method', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'CSS', 'et_builder' ),
                            'on'  => esc_html__( 'True Parallax', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Define the method, used for the parallax effect.', 'et_builder' ),
                    ),
                    'remove_inner_shadow' => array(
                        'label'           => esc_html__( 'Remove Inner Shadow', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                    ),
                    'ctm_src' => array(
                        'label'              => esc_html__( 'Video MP4/URL', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'data_type'          => 'video',
                        'upload_button_text' => esc_attr__( 'Upload a video', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Video MP4 File', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Video', 'et_builder' ),
                        'description'        => esc_html__( 'Upload your desired video in .MP4 format, or type in the URL to the video you would like to display', 'et_builder' ),
                    ),
                    'ctm_src_webm' => array(
                        'label'              => esc_html__( 'Video Webm', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'data_type'          => 'video',
                        'upload_button_text' => esc_attr__( 'Upload a video', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Video WEBM File', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Video', 'et_builder' ),
                        'description'        => esc_html__( 'Upload the .WEBM version of your video here. All uploaded videos should be in both .MP4 .WEBM formats to ensure maximum compatibility in all browsers.', 'et_builder' ),
                    ),
                    'ctm_video_attr' => array(
                        'label'              => esc_html__( 'Video Attributes', 'auxiell' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'Write here video additional attributes', 'auxiell' ),
                    ),
                    'background_position' => array(
                        'label'           => esc_html__( 'Background Image Position', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'layout',
                        'options' => array(
                            'default'       => esc_html__( 'Default', 'et_builder' ),
                            'top_left'      => esc_html__( 'Top Left', 'et_builder' ),
                            'top_center'    => esc_html__( 'Top Center', 'et_builder' ),
                            'top_right'     => esc_html__( 'Top Right', 'et_builder' ),
                            'center_right'  => esc_html__( 'Center Right', 'et_builder' ),
                            'center_left'   => esc_html__( 'Center Left', 'et_builder' ),
                            'bottom_left'   => esc_html__( 'Bottom Left', 'et_builder' ),
                            'bottom_center' => esc_html__( 'Bottom Center', 'et_builder' ),
                            'bottom_right'  => esc_html__( 'Bottom Right', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'off',
                    ),
                    'background_size' => array(
                        'label'           => esc_html__( 'Background Image Size', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'layout',
                        'options'         => array(
                            'default' => esc_html__( 'Default', 'et_builder' ),
                            'contain' => esc_html__( 'Fit', 'et_builder' ),
                            'initial' => esc_html__( 'Actual Size', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'off',
                    ),
                    'top_padding' => array(
                        'label'           => esc_html__( 'Top Padding', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'layout',
                        'tab_slug'        => 'advanced',
                        'mobile_options'  => true,
                        'validate_unit'   => true,
                    ),
                    'bottom_padding' => array(
                        'label'           => esc_html__( 'Bottom Padding', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'layout',
                        'tab_slug'        => 'advanced',
                        'mobile_options'  => true,
                        'validate_unit'   => true,
                    ),
                    'hide_content_on_mobile' => array(
                        'label'           => esc_html__( 'Hide Content On Mobile', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'layout',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'tab_slug'          => 'advanced',
                    ),
                    'hide_cta_on_mobile' => array(
                        'label'           => esc_html__( 'Hide CTA On Mobile', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'layout',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'tab_slug'          => 'advanced',
                    ),
                    'show_image_video_mobile' => array(
                        'label'           => esc_html__( 'Show Image / Video On Mobile', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'layout',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'tab_slug'        => 'advanced',
                    ),
                    'top_padding_tablet' => array(
                        'type' => 'skip',
                    ),
                    'top_padding_phone' => array(
                        'type' => 'skip',
                    ),
                    'bottom_padding_tablet' => array(
                        'type' => 'skip',
                    ),
                    'bottom_padding_phone' => array(
                        'type' => 'skip',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            function pre_shortcode_content() {
                global $et_pb_slider_has_video, $et_pb_slider_parallax, $et_pb_slider_parallax_method, $et_pb_slider_hide_mobile, $et_pb_slider_custom_icon, $et_pb_slider_item_num;

                $et_pb_slider_item_num = 0;

                $parallax                = $this->shortcode_atts['parallax'];
                $parallax_method         = $this->shortcode_atts['parallax_method'];
                $hide_content_on_mobile  = $this->shortcode_atts['hide_content_on_mobile'];
                $hide_cta_on_mobile      = $this->shortcode_atts['hide_cta_on_mobile'];
                $button_custom           = $this->shortcode_atts['custom_button'];
                $custom_icon             = $this->shortcode_atts['button_icon'];

                $et_pb_slider_has_video = false;

                $et_pb_slider_parallax = $parallax;

                $et_pb_slider_parallax_method = $parallax_method;

                $et_pb_slider_hide_mobile = array(
                    'hide_content_on_mobile'  => $hide_content_on_mobile,
                    'hide_cta_on_mobile'      => $hide_cta_on_mobile,
                );

                $et_pb_slider_custom_icon = 'on' === $button_custom ? $custom_icon : '';

            }

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $module_id               = $this->shortcode_atts['module_id'];
                $module_class            = $this->shortcode_atts['module_class'];
                $show_arrows             = $this->shortcode_atts['show_arrows'];
                $show_pagination         = $this->shortcode_atts['show_pagination'];
                $parallax                = $this->shortcode_atts['parallax'];
                $parallax_method         = $this->shortcode_atts['parallax_method'];
                $auto                    = $this->shortcode_atts['auto'];
                $auto_speed              = $this->shortcode_atts['auto_speed'];
                $auto_ignore_hover       = $this->shortcode_atts['auto_ignore_hover'];
                $top_padding             = $this->shortcode_atts['top_padding'];
                $body_font_size 		 = $this->shortcode_atts['body_font_size'];
                $bottom_padding          = $this->shortcode_atts['bottom_padding'];
                $top_padding_tablet      = $this->shortcode_atts['top_padding_tablet'];
                $top_padding_phone       = $this->shortcode_atts['top_padding_phone'];
                $bottom_padding_tablet   = $this->shortcode_atts['bottom_padding_tablet'];
                $bottom_padding_phone    = $this->shortcode_atts['bottom_padding_phone'];
                $remove_inner_shadow     = $this->shortcode_atts['remove_inner_shadow'];
                $ctm_src                     = $this->shortcode_atts['ctm_src'];
                $ctm_src_webm                = $this->shortcode_atts['ctm_src_webm'];
                $ctm_video_attr                = $this->shortcode_atts['ctm_video_attr'];
                $hide_content_on_mobile  = $this->shortcode_atts['hide_content_on_mobile'];
                $hide_cta_on_mobile      = $this->shortcode_atts['hide_cta_on_mobile'];
                $show_image_video_mobile = $this->shortcode_atts['show_image_video_mobile'];
                $background_position     = $this->shortcode_atts['background_position'];
                $background_size         = $this->shortcode_atts['background_size'];

                global $et_pb_slider_has_video, $et_pb_slider_parallax, $et_pb_slider_parallax_method, $et_pb_slider_hide_mobile, $et_pb_slider_custom_icon;

                $content = $this->shortcode_content;

                $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

                if ( '' !== $top_padding || '' !== $top_padding_tablet || '' !== $top_padding_phone ) {
                    $padding_values = array(
                        'desktop' => $top_padding,
                        'tablet'  => $top_padding_tablet,
                        'phone'   => $top_padding_phone,
                    );

                    et_pb_generate_responsive_css( $padding_values, '%%order_class%% .et_pb_slide_description, .et_pb_slider_fullwidth_off%%order_class%% .et_pb_slide_description', 'padding-top', $function_name );
                }

                if ( '' !== $bottom_padding || '' !== $bottom_padding_tablet || '' !== $bottom_padding_phone ) {
                    $padding_values = array(
                        'desktop' => $bottom_padding,
                        'tablet'  => $bottom_padding_tablet,
                        'phone'   => $bottom_padding_phone,
                    );

                    et_pb_generate_responsive_css( $padding_values, '%%order_class%% .et_pb_slide_description, .et_pb_slider_fullwidth_off%%order_class%% .et_pb_slide_description', 'padding-bottom', $function_name );
                }

                if ( '' !== $bottom_padding || '' !== $top_padding ) {
                    ET_Builder_Module::set_style( $function_name, array(
                        'selector'    => '%%order_class%% .et_pb_slide_description, .et_pb_slider_fullwidth_off%%order_class%% .et_pb_slide_description',
                        'declaration' => 'padding-right: 0; padding-left: 0;',
                    ) );
                }

                if ( 'default' !== $background_position && 'off' === $parallax ) {
                    $processed_position = str_replace( '_', ' ', $background_position );

                    ET_Builder_Module::set_style( $function_name, array(
                        'selector'    => '%%order_class%% .et_pb_slide',
                        'declaration' => sprintf(
                            'background-position: %1$s;',
                            esc_html( $processed_position )
                        ),
                    ) );
                }

                if ( 'default' !== $background_size && 'off' === $parallax ) {
                    ET_Builder_Module::set_style( $function_name, array(
                        'selector'    => '%%order_class%% .et_pb_slide',
                        'declaration' => sprintf(
                            '-moz-background-size: %1$s;
					-webkit-background-size: %1$s;
					background-size: %1$s;',
                            esc_html( $background_size )
                        ),
                    ) );
                }

                if ( '' !== $ctm_src ) {
                    if ( false !== et_pb_check_oembed_provider( esc_url( $ctm_src ) ) ) {
                        $video_src = wp_oembed_get( esc_url( $ctm_src ) );
                    } else {
                        $video_src = sprintf( '
					<video %3$s loop preload="auto" autoplay muted>
						%1$s
						%2$s
					</video>',
                            ( '' !== $ctm_src ? sprintf( '<source type="video/mp4" src="%s" />', esc_url( $ctm_src ) ) : '' ),
                            ( '' !== $ctm_src_webm ? sprintf( '<source type="video/webm" src="%s" />', esc_url( $ctm_src_webm ) ) : '' ),
                            $ctm_video_attr
                        );

                        wp_enqueue_style( 'wp-mediaelement' );
                        wp_enqueue_script( 'wp-mediaelement' );
                    }
                }

                if(!empty($video_src)){
                    $video_output = sprintf(
                        '<div class="et_pb_video_wrap">
				<div class="et_pb_video_box">
					%1$s
				</div>
			    </div>',
                        $video_src
                    );
                } else {
                    $video_output = '';
                }

                $fullwidth = 'et_pb_fullwidth_slider' === $function_name ? 'on' : 'off';

                $class  = '';
                $class .= 'off' === $fullwidth ? ' et_pb_slider_fullwidth_off' : '';
                $class .= 'off' === $show_arrows ? ' et_pb_slider_no_arrows' : '';
                $class .= 'off' === $show_pagination ? ' et_pb_slider_no_pagination' : '';
                $class .= 'on' === $parallax ? ' et_pb_slider_parallax' : '';
                $class .= 'on' === $auto ? ' et_slider_auto et_slider_speed_' . esc_attr( $auto_speed ) : '';
                $class .= 'on' === $auto_ignore_hover ? ' et_slider_auto_ignore_hover' : '';
                $class .= 'on' === $remove_inner_shadow ? ' et_pb_slider_no_shadow' : '';
                $class .= 'on' === $show_image_video_mobile ? ' et_pb_slider_show_image' : '';

                $output = sprintf(
                    '<div%4$s class="et_pb_module et_pb_slider%1$s%3$s%5$s">
                   %6$s
				<div class="et_pb_slides">
					%2$s
				</div> <!-- .et_pb_slides -->
			</div> <!-- .et_pb_slider -->
			',
                    $class,
                    $content,
                    ( $et_pb_slider_has_video ? ' et_pb_preload' : '' ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                    ( '' !== $video_output ? $video_output : '' )
                );

                return $output;
            }
        }
        new ET_Builder_Module_Slider_Single_Background;
        class ET_Builder_Module_Slider_Custom_Item extends ET_Builder_Module {
            function init() {
                $this->name                        = esc_html__( 'Slide Custom AUXIELL', 'et_builder' );
                $this->slug                        = 'et_pb_slide_custom';
                $this->type                        = 'child';
                $this->child_title_var             = 'admin_title';
                $this->child_title_fallback_var    = 'heading';

                $this->whitelisted_fields = array(
                    'heading',
                    'admin_title',
                    'button_text',
                    'button_link',
                    'ctm_button_play_text',
                    'ctm_button_play_link',
                    'background_image',
                    'background_position',
                    'background_size',
                    'background_color',
                    'image',
                    'alignment',
                    'video_url',
                    'image_alt',
                    'background_layout',
                    'video_bg_mp4',
                    'video_bg_webm',
                    'video_bg_width',
                    'video_bg_height',
                    'allow_player_pause',
                    'content_new',
                    'arrows_custom_color',
                    'dot_nav_custom_color',
                    'use_bg_overlay',
                    'use_text_overlay',
                    'bg_overlay_color',
                    'text_overlay_color',
                    'text_border_radius',
                );

                $this->fields_defaults = array(
                    'button_link'         => array( '#' ),
                    'ctm_button_play_link'         => array( '#' ),
                    'background_position' => array( 'default' ),
                    'background_size'     => array( 'default' ),
                    'background_color'    => array( '#ffffff', 'only_default_setting' ),
                    'alignment'           => array( 'center' ),
                    'background_layout'   => array( 'dark' ),
                    'allow_player_pause'  => array( 'off' ),
                );

                $this->advanced_setting_title_text = esc_html__( 'New Slide', 'et_builder' );
                $this->settings_text               = esc_html__( 'Slide Settings', 'et_builder' );
                $this->main_css_element = '%%order_class%%';
                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__( 'Header', 'et_builder' ),
                            'css'      => array(
                                'main' => ".et_pb_slider {$this->main_css_element} .et_pb_slide_description .et_pb_slide_title",
                                'important' => 'all',
                            ),
                            'line_height' => array(
                                'range_settings' => array(
                                    'min'  => '1',
                                    'max'  => '100',
                                    'step' => '0.1',
                                ),
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__( 'Body', 'et_builder' ),
                            'css'      => array(
                                'main'        => "{$this->main_css_element} .et_pb_slide_content",
                                'line_height' => "{$this->main_css_element} p",
                                'important'   => 'all',
                            ),
                            'line_height' => array(
                                'range_settings' => array(
                                    'min'  => '1',
                                    'max'  => '100',
                                    'step' => '0.1',
                                ),
                            ),
                        ),
                    ),
                    'button' => array(
                        'button' => array(
                            'label' => esc_html__( 'Button', 'et_builder' ),
                            'css'      => array(
                                'main' => ".et_pb_slider {$this->main_css_element}.et_pb_slide .et_pb_button",
                            ),
                        ),
                    ),
                    'ctm_button_play' => array(
                        'button' => array(
                            'label' => esc_html__( 'Button', 'et_builder' ),
                            'css'      => array(
                                'main' => ".et_pb_slider {$this->main_css_element}.et_pb_slide .et_pb_button",
                            ),
                        ),
                    ),
                );

                $this->custom_css_options = array(
                    'slide_title' => array(
                        'label'    => esc_html__( 'Slide Title', 'et_builder' ),
                        'selector' => '.et_pb_slide_description h2',
                    ),
                    'slide_container' => array(
                        'label'    => esc_html__( 'Slide Description Container', 'et_builder' ),
                        'selector' => '.et_pb_container',
                    ),
                    'slide_description' => array(
                        'label'    => esc_html__( 'Slide Description', 'et_builder' ),
                        'selector' => '.et_pb_slide_description',
                    ),
                    'slide_button' => array(
                        'label'    => esc_html__( 'Slide Button', 'et_builder' ),
                        'selector' => '.et_pb_slide .et_pb_container a.et_pb_more_button.et_pb_button',
                        'no_space_before_selector' => true,
                    ),
                    'slide_image' => array(
                        'label'    => esc_html__( 'Slide Image', 'et_builder' ),
                        'selector' => '.et_pb_slide_image',
                    ),
                );
            }

            function get_fields() {
                $fields = array(
                    'heading' => array(
                        'label'           => esc_html__( 'Heading', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the title text for your slide.', 'et_builder' ),
                    ),
                    'button_text' => array(
                        'label'           => esc_html__( 'Button Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the text for the slide button', 'et_builder' ),
                    ),
                    'button_link' => array(
                        'label'           => esc_html__( 'Button URL', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Input a destination URL for the slide button.', 'et_builder' ),
                    ),
                    'ctm_button_play_text' => array(
                        'label'           => esc_html__( 'Button Play Video Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the text for the play the video button', 'et_builder' ),
                    ),
                    'ctm_button_play_link' => array(
                        'label'           => esc_html__( 'Button Play Video Hash', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Input a hash for the play the video button.', 'et_builder' ),
                    ),
                    'background_image' => array(
                        'label'              => esc_html__( 'Background Image', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Background Image', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Background', 'et_builder' ),
                        'description'        => esc_html__( 'If defined, this image will be used as the background for this module. To remove a background image, simply delete the URL from the settings field.', 'et_builder' ),
                    ),
                    'background_position' => array(
                        'label'           => esc_html__( 'Background Image Position', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'layout',
                        'options'         => array(
                            'default'       => esc_html__( 'Default', 'et_builder' ),
                            'center'        => esc_html__( 'Center', 'et_builder' ),
                            'top_left'      => esc_html__( 'Top Left', 'et_builder' ),
                            'top_center'    => esc_html__( 'Top Center', 'et_builder' ),
                            'top_right'     => esc_html__( 'Top Right', 'et_builder' ),
                            'center_right'  => esc_html__( 'Center Right', 'et_builder' ),
                            'center_left'   => esc_html__( 'Center Left', 'et_builder' ),
                            'bottom_left'   => esc_html__( 'Bottom Left', 'et_builder' ),
                            'bottom_center' => esc_html__( 'Bottom Center', 'et_builder' ),
                            'bottom_right'  => esc_html__( 'Bottom Right', 'et_builder' ),
                        ),
                    ),
                    'background_size' => array(
                        'label'           => esc_html__( 'Background Image Size', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'layout',
                        'options'         => array(
                            'default' => esc_html__( 'Default', 'et_builder' ),
                            'cover'   => esc_html__( 'Cover', 'et_builder' ),
                            'contain' => esc_html__( 'Fit', 'et_builder' ),
                            'initial' => esc_html__( 'Actual Size', 'et_builder' ),
                        ),
                    ),
                    'background_color' => array(
                        'label'       => esc_html__( 'Background Color', 'et_builder' ),
                        'type'        => 'color-alpha',
                        'description' => esc_html__( 'Use the color picker to choose a background color for this module.', 'et_builder' ),
                    ),
                    'image' => array(
                        'label'              => esc_html__( 'Slide Image', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'configuration',
                        'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Slide Image', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Slide Image', 'et_builder' ),
                        'description'        => esc_html__( 'If defined, this slide image will appear to the left of your slide text. Upload an image, or leave blank for a text-only slide.', 'et_builder' ),
                    ),
                    'use_bg_overlay'      => array(
                        'label'           => esc_html__( 'Use Background Overlay', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_bg_overlay_color',
                        ),
                        'description'     => esc_html__( 'When enabled, a custom overlay color will be added above your background image and behind your slider content.', 'et_builder' ),
                    ),
                    'bg_overlay_color' => array(
                        'label'             => esc_html__( 'Background Overlay Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Use the color picker to choose a color for the background overlay.', 'et_builder' ),
                    ),
                    'use_text_overlay'      => array(
                        'label'           => esc_html__( 'Use Text Overlay', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_text_overlay_color',
                        ),
                        'description'     => esc_html__( 'When enabled, a background color is added behind the slider text to make it more readable atop background images.', 'et_builder' ),
                    ),
                    'text_overlay_color' => array(
                        'label'             => esc_html__( 'Text Overlay Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Use the color picker to choose a color for the text overlay.', 'et_builder' ),
                    ),
                    'alignment' => array(
                        'label'           => esc_html__( 'Slide Image Vertical Alignment', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'layout',
                        'options'         => array(
                            'center' => esc_html__( 'Center', 'et_builder' ),
                            'bottom' => esc_html__( 'Bottom', 'et_builder' ),
                        ),
                        'description' => esc_html__( 'This setting determines the vertical alignment of your slide image. Your image can either be vertically centered, or aligned to the bottom of your slide.', 'et_builder' ),
                    ),
                    'video_url' => array(
                        'label'           => esc_html__( 'Slide Video', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'If defined, this video will appear to the left of your slide text. Enter youtube or vimeo page url, or leave blank for a text-only slide.', 'et_builder' ),
                    ),
                    'image_alt' => array(
                        'label'           => esc_html__( 'Image Alternative Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'If you have a slide image defined, input your HTML ALT text for the image here.', 'et_builder' ),
                    ),
                    'background_layout' => array(
                        'label'           => esc_html__( 'Text Color', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'color_option',
                        'options'         => array(
                            'dark'  => esc_html__( 'Light', 'et_builder' ),
                            'light' => esc_html__( 'Dark', 'et_builder' ),
                        ),
                        'description'     => esc_html__( 'Here you can choose whether your text is light or dark. If you have a slide with a dark background, then choose light text. If you have a light background, then use dark text.' , 'et_builder' ),
                    ),
                    'video_bg_mp4' => array(
                        'label'              => esc_html__( 'Background Video MP4', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'data_type'          => 'video',
                        'upload_button_text' => esc_attr__( 'Upload a video', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Background Video MP4 File', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Background Video', 'et_builder' ),
                        'description'        => et_get_safe_localization( __( 'All videos should be uploaded in both .MP4 .WEBM formats to ensure maximum compatibility in all browsers. Upload the .MP4 version here. <b>Important Note: Video backgrounds are disabled from mobile devices. Instead, your background image will be used. For this reason, you should define both a background image and a background video to ensure best results.</b>', 'et_builder' ) ),
                    ),
                    'video_bg_webm' => array(
                        'label'              => esc_html__( 'Background Video Webm', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'data_type'          => 'video',
                        'upload_button_text' => esc_attr__( 'Upload a video', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose a Background Video WEBM File', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Background Video', 'et_builder' ),
                        'description'        => et_get_safe_localization( __( 'All videos should be uploaded in both .MP4 .WEBM formats to ensure maximum compatibility in all browsers. Upload the .WEBM version here. <b>Important Note: Video backgrounds are disabled from mobile devices. Instead, your background image will be used. For this reason, you should define both a background image and a background video to ensure best results.</b>', 'et_builder' ) ),
                    ),
                    'video_bg_width' => array(
                        'label'           => esc_html__( 'Background Video Width', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'In order for videos to be sized correctly, you must input the exact width (in pixels) of your video here.' ,'et_builder' ),
                    ),
                    'video_bg_height' => array(
                        'label'           => esc_html__( 'Background Video Height', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'In order for videos to be sized correctly, you must input the exact height (in pixels) of your video here.' ,'et_builder' ),
                    ),
                    'allow_player_pause' => array(
                        'label'           => esc_html__( 'Pause Video', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description'     => esc_html__( 'Allow video to be paused by other players when they begin playing' ,'et_builder' ),
                    ),
                    'content_new' => array(
                        'label'           => esc_html__( 'Content', 'et_builder' ),
                        'type'            => 'tiny_mce',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Input your main slide text content here.', 'et_builder' ),
                    ),
                    'arrows_custom_color' => array(
                        'label'        => esc_html__( 'Arrows Custom Color', 'et_builder' ),
                        'type'         => 'color',
                        'custom_color' => true,
                        'tab_slug'     => 'advanced',
                    ),
                    'dot_nav_custom_color' => array(
                        'label'        => esc_html__( 'Dot Nav Custom Color', 'et_builder' ),
                        'type'         => 'color',
                        'custom_color' => true,
                        'tab_slug'     => 'advanced',
                    ),
                    'admin_title' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the slide in the builder for easy identification.', 'et_builder' ),
                    ),
                    'text_border_radius' => array(
                        'label'           => esc_html__( 'Text Overlay Border Radius', 'et_builder' ),
                        'type'            => 'range',
                        'option_category' => 'layout',
                        'default'         => '3',
                        'range_settings'  => array(
                            'min'  => '0',
                            'max'  => '100',
                            'step' => '1',
                        ),
                        'tab_slug'        => 'advanced',
                    ),
                );
                return $fields;
            }

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $alignment            = $this->shortcode_atts['alignment'];
                $heading              = $this->shortcode_atts['heading'];
                $button_text          = $this->shortcode_atts['button_text'];
                $button_link          = $this->shortcode_atts['button_link'];
                $ctm_button_play_text          = $this->shortcode_atts['ctm_button_play_text'];
                $ctm_button_play_link          = $this->shortcode_atts['ctm_button_play_link'];
                $background_color     = $this->shortcode_atts['background_color'];
                $background_image     = $this->shortcode_atts['background_image'];
                $image                = $this->shortcode_atts['image'];
                $image_alt            = $this->shortcode_atts['image_alt'];
                $background_layout    = $this->shortcode_atts['background_layout'];
                $video_bg_webm        = $this->shortcode_atts['video_bg_webm'];
                $video_bg_mp4         = $this->shortcode_atts['video_bg_mp4'];
                $video_bg_width       = $this->shortcode_atts['video_bg_width'];
                $video_bg_height      = $this->shortcode_atts['video_bg_height'];
                $video_url            = $this->shortcode_atts['video_url'];
                $allow_player_pause   = $this->shortcode_atts['allow_player_pause'];
                $dot_nav_custom_color = $this->shortcode_atts['dot_nav_custom_color'];
                $arrows_custom_color  = $this->shortcode_atts['arrows_custom_color'];
                $custom_icon          = $this->shortcode_atts['button_icon'];
                $button_custom        = $this->shortcode_atts['custom_button'];
                $background_position  = $this->shortcode_atts['background_position'];
                $background_size      = $this->shortcode_atts['background_size'];
                $use_bg_overlay       = $this->shortcode_atts['use_bg_overlay'];
                $bg_overlay_color     = $this->shortcode_atts['bg_overlay_color'];
                $use_text_overlay     = $this->shortcode_atts['use_text_overlay'];
                $text_overlay_color   = $this->shortcode_atts['text_overlay_color'];
                $text_border_radius   = $this->shortcode_atts['text_border_radius'];

                global $et_pb_slider_has_video, $et_pb_slider_parallax, $et_pb_slider_parallax_method, $et_pb_slider_hide_mobile, $et_pb_slider_custom_icon, $et_pb_slider_item_num;

                $background_video = '';

                $et_pb_slider_item_num++;

                $hide_on_mobile_class = self::HIDE_ON_MOBILE;

                $first_video = false;

                $custom_slide_icon = 'on' === $button_custom && '' !== $custom_icon ? $custom_icon : $et_pb_slider_custom_icon;

                if ( '' !== $video_bg_mp4 || '' !== $video_bg_webm ) {
                    if ( ! $et_pb_slider_has_video )
                        $first_video = true;

                    $background_video = sprintf(
                        '<div class="et_pb_section_video_bg%2$s%3$s">
					%1$s
				</div>',
                        do_shortcode( sprintf( '
					<video loop="loop" %3$s%4$s>
						%1$s
						%2$s
					</video>',
                            ( '' !== $video_bg_mp4 ? sprintf( '<source type="video/mp4" src="%s" />', esc_url( $video_bg_mp4 ) ) : '' ),
                            ( '' !== $video_bg_webm ? sprintf( '<source type="video/webm" src="%s" />', esc_url( $video_bg_webm ) ) : '' ),
                            ( '' !== $video_bg_width ? sprintf( ' width="%s"', esc_attr( intval( $video_bg_width ) ) ) : '' ),
                            ( '' !== $video_bg_height ? sprintf( ' height="%s"', esc_attr( intval( $video_bg_height ) ) ) : '' ),
                            ( '' !== $background_image ? sprintf( ' poster="%s"', esc_url( $background_image ) ) : '' )
                        ) ),
                        ( $first_video ? ' et_pb_first_video' : '' ),
                        ( 'on' === $allow_player_pause ? ' et_pb_allow_player_pause' : '' )
                    );

                    $et_pb_slider_has_video = true;

                    wp_enqueue_style( 'wp-mediaelement' );
                    wp_enqueue_script( 'wp-mediaelement' );
                }

                if ( '' !== $heading ) {
                    $heading = sprintf( $heading);
                    $heading = '<h2 class="et_pb_slide_title">' . $heading . '</h2>';
                }

                $button = '';

                if ( '' !== $button_text ) {
                    $button = sprintf( '<a href="%1$s" class="et_pb_more_button et_pb_button%3$s%5$s"%4$s>%2$s</a>',
                        esc_url( $button_link ),
                        esc_html( $button_text ),
                        ( 'on' === $et_pb_slider_hide_mobile['hide_cta_on_mobile'] ? esc_attr( " {$hide_on_mobile_class}" ) : '' ),
                        '' !== $custom_slide_icon ? sprintf(
                            ' data-icon="%1$s"',
                            esc_attr( et_pb_process_font_icon( $custom_slide_icon ) )
                        ) : '',
                        '' !== $custom_slide_icon ? ' et_pb_custom_button_icon' : ''
                    );
                }

                $ctm_button_play = '';

                if ( '' !== $ctm_button_play_text ) {
                    $ctm_button_play = sprintf( '<a href="%1$s" class="et_pb_play_video_button et_pb_button%3$s%5$s"%4$s><span class="ion-arrow-right-b"></span>%2$s</a>',
                        esc_url( $ctm_button_play_link ),
                        esc_html( $ctm_button_play_text ),
                        ( 'on' === $et_pb_slider_hide_mobile['hide_cta_on_mobile'] ? esc_attr( " {$hide_on_mobile_class}" ) : '' ),
                        '' !== $custom_slide_icon ? sprintf(
                            ' data-icon="%1$s"',
                            esc_attr( et_pb_process_font_icon( $custom_slide_icon ) )
                        ) : '',
                        '' !== $custom_slide_icon ? ' et_pb_custom_button_icon' : ''
                    );
                }

                $style = $class = '';

                if ( '' !== $background_color ) {
                    $style .= sprintf( 'background-color:%s;',
                        esc_attr( $background_color )
                    );
                }

                if ( '' !== $background_image && 'on' !== $et_pb_slider_parallax ) {
                    $style .= sprintf( 'background-image:url(%s);',
                        esc_attr( $background_image )
                    );
                }

                if ( 'on' === $use_bg_overlay && '' !== $bg_overlay_color ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%.et_pb_slide .et_pb_slide_overlay_container',
                        'declaration' => sprintf(
                            'background-color: %1$s;',
                            esc_html( $bg_overlay_color )
                        ),
                    ) );
                }

                if ( 'on' === $use_text_overlay && '' !== $text_overlay_color ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%.et_pb_slide .et_pb_slide_title, %%order_class%%.et_pb_slide .et_pb_slide_content',
                        'declaration' => sprintf(
                            'background-color: %1$s;',
                            esc_html( $text_overlay_color )
                        ),
                    ) );
                }

                if ( '' !== $text_border_radius ) {
                    $border_radius_value = et_builder_process_range_value( $text_border_radius );
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%.et_pb_slider_with_text_overlay h2.et_pb_slide_title',
                        'declaration' => sprintf(
                            '-webkit-border-top-left-radius: %1$s;
					-webkit-border-top-right-radius: %1$s;
					-moz-border-radius-topleft: %1$s;
					-moz-border-radius-topright: %1$s;
					border-top-left-radius: %1$s;
					border-top-right-radius: %1$s;',
                            esc_html( $border_radius_value )
                        ),
                    ) );
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%.et_pb_slider_with_text_overlay .et_pb_slide_content',
                        'declaration' => sprintf(
                            '-webkit-border-bottom-right-radius: %1$s;
					-webkit-border-bottom-left-radius: %1$s;
					-moz-border-radius-bottomright: %1$s;
					-moz-border-radius-bottomleft: %1$s;
					border-bottom-right-radius: %1$s;
					border-bottom-left-radius: %1$s;',
                            esc_html( $border_radius_value )
                        ),
                    ) );
                }

                $style = '' !== $style ? " style='{$style}'" : '';

                $image = '' !== $image
                    ? sprintf( '<div class="et_pb_slide_image"><img src="%1$s" alt="%2$s" /></div>',
                        esc_url( $image ),
                        esc_attr( $image_alt )
                    )
                    : '';

                if ( '' !== $video_url ) {
                    global $wp_embed;

                    $video_embed = apply_filters( 'the_content', $wp_embed->shortcode( '', esc_url( $video_url ) ) );

                    $video_embed = preg_replace('/<embed /','<embed wmode="transparent" ',$video_embed);
                    $video_embed = preg_replace('/<\/object>/','<param name="wmode" value="transparent" /></object>',$video_embed);

                    $image = sprintf( '<div class="et_pb_slide_video">%1$s</div>',
                        $video_embed
                    );
                }

                if ( '' !== $image ) $class = ' et_pb_slide_with_image';

                if ( '' !== $video_url ) $class .= ' et_pb_slide_with_video';

                $class .= " et_pb_bg_layout_{$background_layout}";

                $class .= 'on' === $use_bg_overlay ? ' et_pb_slider_with_overlay' : '';
                $class .= 'on' === $use_text_overlay ? ' et_pb_slider_with_text_overlay' : '';

                if ( 'bottom' !== $alignment ) {
                    $class .= " et_pb_media_alignment_{$alignment}";
                }

                $data_dot_nav_custom_color = '' !== $dot_nav_custom_color
                    ? sprintf( ' data-dots_color="%1$s"', esc_attr( $dot_nav_custom_color ) )
                    : '';

                $data_arrows_custom_color = '' !== $arrows_custom_color
                    ? sprintf( ' data-arrows_color="%1$s"', esc_attr( $arrows_custom_color ) )
                    : '';

                if ( 'default' !== $background_position && 'off' === $et_pb_slider_parallax ) {
                    $processed_position = str_replace( '_', ' ', $background_position );

                    ET_Builder_Module::set_style( $function_name, array(
                        'selector'    => '.et_pb_slider %%order_class%%',
                        'declaration' => sprintf(
                            'background-position: %1$s;',
                            esc_html( $processed_position )
                        ),
                    ) );
                }

                if ( 'default' !== $background_size && 'off' === $et_pb_slider_parallax ) {
                    ET_Builder_Module::set_style( $function_name, array(
                        'selector'    => '.et_pb_slider %%order_class%%',
                        'declaration' => sprintf(
                            '-moz-background-size: %1$s;
					-webkit-background-size: %1$s;
					background-size: %1$s;',
                            esc_html( $background_size )
                        ),
                    ) );

                    if ( 'initial' === $background_size ) {
                        ET_Builder_Module::set_style( $function_name, array(
                            'selector'    => 'body.ie %%order_class%%.et_pb_slide',
                            'declaration' => sprintf(
                                '-moz-background-size: %1$s;
						-webkit-background-size: %1$s;
						background-size: %1$s;',
                                'auto'
                            ),
                        ) );
                    }
                }

                $class = ET_Builder_Element::add_module_order_class( $class, $function_name );

                if ( 1 === $et_pb_slider_item_num ) {
                    $class .= " et-pb-active-slide";
                }

                $output = sprintf(
                    '<div class="et_pb_slide%6$s"%4$s%10$s%11$s>
				%8$s
				%12$s
				<div class="et_pb_container clearfix">
					%5$s
					<div class="et_pb_slide_description">
						%1$s
						<div class="et_pb_slide_content%9$s">%2$s</div>
						%3$s%13$s
					</div> <!-- .et_pb_slide_description -->
				</div> <!-- .et_pb_container -->
				%7$s
			</div> <!-- .et_pb_slide -->
			',
                    $heading,
                    $this->shortcode_content,
                    $button,
                    $style,
                    $image,
                    esc_attr( $class ),
                    ( '' !== $background_video ? $background_video : '' ),
                    ( '' !== $background_image && 'on' === $et_pb_slider_parallax ? sprintf( '<div class="et_parallax_bg%2$s" style="background-image: url(%1$s);"></div>', esc_attr( $background_image ), ( 'off' === $et_pb_slider_parallax_method ? ' et_pb_parallax_css' : '' ) ) : '' ),
                    ( 'on' === $et_pb_slider_hide_mobile['hide_content_on_mobile'] ? esc_attr( " {$hide_on_mobile_class}" ) : '' ),
                    $data_dot_nav_custom_color,
                    $data_arrows_custom_color,
                    'on' === $use_bg_overlay ? '<div class="et_pb_slide_overlay_container"></div>' : '',
                    $ctm_button_play
                );

                return $output;
            }
        }
        new ET_Builder_Module_Slider_Custom_Item;
        class ET_Builder_Module_Blog_Horizontal_Home extends ET_Builder_Module {
            function init() {
                $this->name = esc_html__( 'Blog Horizontal Home AUXIELL', 'et_builder' );
                $this->slug = 'et_pb_blog_horiz';

                $this->whitelisted_fields = array(
                    'fullwidth',
                    'posts_number',
                    'include_categories',
                    'meta_date',
                    'show_thumbnail',
                    'show_content',
                    'show_more',
                    //'show_author',
                    'show_date',
                    //'show_categories',
                    //'show_comments',
                    //'show_pagination',
                    'offset_number',
                    'background_layout',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'masonry_tile_background_color',
                    'use_dropshadow',
                    'use_overlay',
                    'overlay_icon_color',
                    'hover_overlay_color',
                    'hover_icon',
                );

                $this->fields_defaults = array(
                    'fullwidth'         => array( 'on' ),
                    'posts_number'      => array( 10, 'add_default_setting' ),
                    'meta_date'         => array( 'j/m/Y', 'add_default_setting' ),
                    'show_thumbnail'    => array( 'on' ),
                    'show_content'      => array( 'off' ),
                    'show_more'         => array( 'off' ),
                    //'show_author'       => array( 'on' ),
                    'show_date'         => array( 'on' ),
                    //'show_categories'   => array( 'on' ),
                    //'show_comments'     => array( 'off' ),
                    //'show_pagination'   => array( 'on' ),
                    'offset_number'     => array( 0, 'only_default_setting' ),
                    'background_layout' => array( 'light' ),
                    'use_dropshadow'    => array( 'off' ),
                    'use_overlay'       => array( 'off' ),
                );

                $this->main_css_element = '%%order_class%% .et_pb_post';
                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__( 'Header', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} .entry-title",
                                'important' => 'all',
                            ),
                        ),
                        'meta' => array(
                            'label'    => esc_html__( 'Meta', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} .post-meta",
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__( 'Body', 'et_builder' ),
                            'css'      => array(
                                'color'        => "{$this->main_css_element}, {$this->main_css_element} .post-content *",
                                'line_height' => "{$this->main_css_element} p",
                            ),
                        ),
                    ),
                    'border' => array(),
                );
                $this->custom_css_options = array(
                    'title' => array(
                        'label'    => esc_html__( 'Title', 'et_builder' ),
                        'selector' => '.et_pb_post .entry-title',
                    ),
                    'post_meta' => array(
                        'label'    => esc_html__( 'Post Meta', 'et_builder' ),
                        'selector' => '.et_pb_post .post-meta',
                    ),
                    'pagenavi' => array(
                        'label'    => esc_html__( 'Pagenavi', 'et_builder' ),
                        'selector' => '.wp_pagenavi',
                    ),
                    'featured_image' => array(
                        'label'    => esc_html__( 'Featured Image', 'et_builder' ),
                        'selector' => '.et_pb_image_container',
                    ),
                    'read_more' => array(
                        'label'    => esc_html__( 'Read More Button', 'et_builder' ),
                        'selector' => '.et_pb_post .more-link',
                    ),
                );
            }

            function get_fields() {
                $fields = array(
                    'fullwidth' => array(
                        'label'             => esc_html__( 'Layout', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'on'  => esc_html__( 'Fullwidth', 'et_builder' ),
                            'off' => esc_html__( 'Grid', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_background_layout',
                            '#et_pb_use_dropshadow',
                            '#et_pb_masonry_tile_background_color',
                        ),
                        'description'        => esc_html__( 'Toggle between the various blog layout types.', 'et_builder' ),
                    ),
                    'posts_number' => array(
                        'label'             => esc_html__( 'Posts Number', 'et_builder' ),
                        'type'              => 'text',
                        'option_category'   => 'configuration',
                        'description'       => esc_html__( 'Choose how much posts you would like to display per page.', 'et_builder' ),
                    ),
                    'include_categories' => array(
                        'label'            => esc_html__( 'Include Categories', 'et_builder' ),
                        'renderer'         => 'et_builder_include_categories_option',
                        'option_category'  => 'basic_option',
                        'renderer_options' => array(
                            'use_terms' => false,
                        ),
                        'description'      => esc_html__( 'Choose which categories you would like to include in the feed.', 'et_builder' ),
                    ),
                    'meta_date' => array(
                        'label'             => esc_html__( 'Meta Date Format', 'et_builder' ),
                        'type'              => 'text',
                        'option_category'   => 'configuration',
                        'description'       => esc_html__( 'If you would like to adjust the date format, input the appropriate PHP date format here.', 'et_builder' ),
                    ),
                    'show_thumbnail' => array(
                        'label'             => esc_html__( 'Show Featured Image', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'This will turn thumbnails on and off.', 'et_builder' ),
                    ),
                    'show_content' => array(
                        'label'             => esc_html__( 'Content', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( 'Show Excerpt', 'et_builder' ),
                            'on'  => esc_html__( 'Show Content', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_show_more',
                        ),
                        'description'        => esc_html__( 'Showing the full content will not truncate your posts on the index page. Showing the excerpt will only display your excerpt text.', 'et_builder' ),
                    ),
                    'show_more' => array(
                        'label'             => esc_html__( 'Read More Button', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'off',
                        'description'       => esc_html__( 'Here you can define whether to show "read more" link after the excerpts or not.', 'et_builder' ),
                    ),
                    /*'show_author' => array(
                        'label'             => esc_html__( 'Show Author', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn on or off the author link.', 'et_builder' ),
                    ),*/
                    'show_date' => array(
                        'label'             => esc_html__( 'Show Date', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn the date on or off.', 'et_builder' ),
                    ),
                    /*'show_categories' => array(
                        'label'             => esc_html__( 'Show Categories', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn the category links on or off.', 'et_builder' ),
                    ),
                    'show_comments' => array(
                        'label'             => esc_html__( 'Show Comment Count', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn comment count on and off.', 'et_builder' ),
                    ),
                    'show_pagination' => array(
                        'label'             => esc_html__( 'Show Pagination', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'description'        => esc_html__( 'Turn pagination on and off.', 'et_builder' ),
                    ),*/
                    'offset_number' => array(
                        'label'           => esc_html__( 'Offset Number', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'Choose how many posts you would like to offset by', 'et_builder' ),
                    ),
                    'use_overlay' => array(
                        'label'             => esc_html__( 'Featured Image Overlay', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_overlay_icon_color',
                            '#et_pb_hover_overlay_color',
                            '#et_pb_hover_icon',
                        ),
                        'description'       => esc_html__( 'If enabled, an overlay color and icon will be displayed when a visitors hovers over the featured image of a post.', 'et_builder' ),
                    ),
                    'overlay_icon_color' => array(
                        'label'             => esc_html__( 'Overlay Icon Color', 'et_builder' ),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Here you can define a custom color for the overlay icon', 'et_builder' ),
                    ),
                    'hover_overlay_color' => array(
                        'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Here you can define a custom color for the overlay', 'et_builder' ),
                    ),
                    'hover_icon' => array(
                        'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
                        'type'                => 'text',
                        'option_category'     => 'configuration',
                        'class'               => array( 'et-pb-font-icon' ),
                        'renderer'            => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'depends_show_if'     => 'on',
                        'description'         => esc_html__( 'Here you can define a custom icon for the overlay', 'et_builder' ),
                    ),
                    'background_layout' => array(
                        'label'       => esc_html__( 'Text Color', 'et_builder' ),
                        'type'        => 'select',
                        'option_category' => 'color_option',
                        'options'           => array(
                            'light' => esc_html__( 'Dark', 'et_builder' ),
                            'dark'  => esc_html__( 'Light', 'et_builder' ),
                        ),
                        'depends_default' => true,
                        'description' => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder' ),
                    ),
                    'masonry_tile_background_color' => array(
                        'label'             => esc_html__( 'Grid Tile Background Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'tab_slug'          => 'advanced',
                        'depends_show_if'   => 'off',
                    ),
                    'use_dropshadow' => array(
                        'label'             => esc_html__( 'Use Dropshadow', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'tab_slug'          => 'advanced',
                        'depends_show_if'   => 'off',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $module_id           = $this->shortcode_atts['module_id'];
                $module_class        = $this->shortcode_atts['module_class'];
                $fullwidth           = $this->shortcode_atts['fullwidth'];
                $posts_number        = $this->shortcode_atts['posts_number'];
                $include_categories  = $this->shortcode_atts['include_categories'];
                $meta_date           = $this->shortcode_atts['meta_date'];
                $show_thumbnail      = $this->shortcode_atts['show_thumbnail'];
                $show_content        = $this->shortcode_atts['show_content'];
                /*$show_author         = $this->shortcode_atts['show_author'];*/
                $show_date           = $this->shortcode_atts['show_date'];
                /*$show_categories     = $this->shortcode_atts['show_categories'];
                $show_comments       = $this->shortcode_atts['show_comments'];
                $show_pagination     = $this->shortcode_atts['show_pagination'];*/
                $background_layout   = $this->shortcode_atts['background_layout'];
                $show_more           = $this->shortcode_atts['show_more'];
                $offset_number       = $this->shortcode_atts['offset_number'];
                $masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
                $use_dropshadow      = $this->shortcode_atts['use_dropshadow'];
                $overlay_icon_color  = $this->shortcode_atts['overlay_icon_color'];
                $hover_overlay_color = $this->shortcode_atts['hover_overlay_color'];
                $hover_icon          = $this->shortcode_atts['hover_icon'];
                $use_overlay         = $this->shortcode_atts['use_overlay'];

                global $paged;

                $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

                $container_is_closed = false;

                // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
                remove_all_filters( 'wp_audio_shortcode_library' );
                remove_all_filters( 'wp_audio_shortcode' );
                remove_all_filters( 'wp_audio_shortcode_class');

                if ( '' !== $masonry_tile_background_color ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%.et_pb_blog_grid .et_pb_post',
                        'declaration' => sprintf(
                            'background-color: %1$s;',
                            esc_html( $masonry_tile_background_color )
                        ),
                    ) );
                }

                if ( '' !== $overlay_icon_color ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%% .et_overlay:before',
                        'declaration' => sprintf(
                            'color: %1$s !important;',
                            esc_html( $overlay_icon_color )
                        ),
                    ) );
                }

                if ( '' !== $hover_overlay_color ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%% .et_overlay',
                        'declaration' => sprintf(
                            'background-color: %1$s;',
                            esc_html( $hover_overlay_color )
                        ),
                    ) );
                }

                if ( 'on' === $use_overlay ) {
                    $data_icon = '' !== $hover_icon
                        ? sprintf(
                            ' data-icon="%1$s"',
                            esc_attr( et_pb_process_font_icon( $hover_icon ) )
                        )
                        : '';

                    $overlay_output = sprintf(
                        '<span class="et_overlay%1$s"%2$s></span>',
                        ( '' !== $hover_icon ? ' et_pb_inline_icon' : '' ),
                        $data_icon
                    );
                }

                $overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

                if ( 'on' !== $fullwidth ){
                    if ( 'on' === $use_dropshadow ) {
                        $module_class .= ' et_pb_blog_grid_dropshadow';
                    }

                    wp_enqueue_script( 'salvattore' );

                    $background_layout = 'light';
                }

                $args = array( 'posts_per_page' => (int) $posts_number );

                $et_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );

                if ( is_front_page() ) {
                    $paged = $et_paged;
                }

                if ( '' !== $include_categories )
                    $args['cat'] = $include_categories;

                if ( ! is_search() ) {
                    $args['paged'] = $et_paged;
                }

                if ( '' !== $offset_number && ! empty( $offset_number ) ) {
                    /**
                     * Offset + pagination don't play well. Manual offset calculation required
                     * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
                     */
                    if ( $paged > 1 ) {
                        $args['offset'] = ( ( $et_paged - 1 ) * intval( $posts_number ) ) + intval( $offset_number );
                    } else {
                        $args['offset'] = intval( $offset_number );
                    }
                }

                if ( is_single() && ! isset( $args['post__not_in'] ) ) {
                    $args['post__not_in'] = array( get_the_ID() );
                }

                ob_start();

                query_posts( $args );

                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();

                        $post_format = et_pb_post_format();

                        $thumb = '';

                        $width = 'on' === $fullwidth ? 1080 : 400;
                        $width = (int) apply_filters( 'et_pb_blog_image_width', $width );

                        $height = 'on' === $fullwidth ? 675 : 250;
                        $height = (int) apply_filters( 'et_pb_blog_image_height', $height );
                        $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
                        $titletext = get_the_title();
                        $thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
                        $thumb = $thumbnail["thumb"];

                        $no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

                        if ( in_array( $post_format, array( 'video', 'gallery' ) ) ) {
                            $no_thumb_class = '';
                        } ?>
                        <li>
                            <article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' . $no_thumb_class . $overlay_class  ); ?>>

                                <?php

                                if ( 'on' === $show_date ) {
                                    printf( '<p class="post-meta">%1$s</p>',
                                        (
                                        'on' === $show_date
                                            ? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $meta_date ) ) . '</span>' ) )
                                            : ''
                                        )
                                    );
                                }


                                et_divi_post_format_content();

                                if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
                                    if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
                                        printf(
                                            '<div class="et_main_video_container">
								%1$s
							</div>',
                                            $first_video
                                        );
                                    elseif ( 'gallery' === $post_format ) :
                                        et_pb_gallery_images( 'slider' );
                                    elseif ( '' !== $thumb && 'on' === $show_thumbnail ) :
                                        if ( 'on' !== $fullwidth ) echo '<div class="et_pb_image_container">'; ?>
                                        <a href="<?php esc_url( the_permalink() ); ?>" class="entry-featured-image-url">
                                            <?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
                                            <?php if ( 'on' === $use_overlay ) {
                                                echo $overlay_output;
                                            } ?>
                                        </a>
                                        <?php
                                        if ( 'on' !== $fullwidth ) echo '</div> <!-- .et_pb_image_container -->';
                                    endif;
                                } ?>

                                <?php if ( 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) { ?>
                                <?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) { ?>
                                <div class="post-meta-wrapper">
                                    <h3 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h3>
                                    <?php } ?>

                                    <?php
                                    /*if ( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories || 'on' === $show_comments ) {
                                        printf( '<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>',
                                            (
                                            'on' === $show_author
                                                ? et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  et_pb_get_the_author_posts_link() . '</span>' ) )
                                                : ''
                                            ),
                                            (
                                            ( 'on' === $show_author && 'on' === $show_date )
                                                ? ' | '
                                                : ''
                                            ),
                                            (
                                            'on' === $show_date
                                                ? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $meta_date ) ) . '</span>' ) )
                                                : ''
                                            ),
                                            (
                                            (( 'on' === $show_author || 'on' === $show_date ) && 'on' === $show_categories)
                                                ? ' | '
                                                : ''
                                            ),
                                            (
                                            'on' === $show_categories
                                                ? get_the_category_list(', ')
                                                : ''
                                            ),
                                            (
                                            (( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories ) && 'on' === $show_comments)
                                                ? ' | '
                                                : ''
                                            ),
                                            (
                                            'on' === $show_comments
                                                ? sprintf( esc_html( _nx( '1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) )
                                                : ''
                                            )
                                        );
                                    }*/

                                    echo '<div class="post-content">';

                                    $post_content = get_the_content();

                                    // do not display the content if it contains Blog, Post Slider, Fullwidth Post Slider, or Portfolio modules to avoid infinite loops
                                    if ( ! has_shortcode( $post_content, 'et_pb_blog' ) && ! has_shortcode( $post_content, 'et_pb_portfolio' ) && ! has_shortcode( $post_content, 'et_pb_post_slider' ) && ! has_shortcode( $post_content, 'et_pb_fullwidth_post_slider' ) ) {
                                        if ( 'on' === $show_content ) {
                                            global $more;
                                            global $acf;


                                            // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                                            if ( et_pb_is_pagebuilder_used( get_the_ID() ) ) {
                                                $more = 1;
                                                the_content();
                                            } else {
                                                $more = null;
                                                if(!empty($acf)){
                                                    $abstract = get_field('abstract', get_the_ID());
                                                    $moreLink = get_permalink();
                                                    $moreText = (defined( 'POLYLANG_VERSION' )) ? pll__('Read More') : 'Read More';

                                                    echo sprintf(
                                                        '<p>%1$s...</p><a href="%3$s" class="more-link" >%2$s</a>',
                                                        $abstract,
                                                        $moreText,
                                                        $moreLink
                                                    );

                                                }else{
                                                    the_content( esc_html__( 'read more...', 'et_builder' ) );
                                                }

                                            }


                                        } else {
                                            if ( has_excerpt() ) {
                                                the_excerpt();
                                            } else {
                                                echo wpautop( truncate_post( 270, false ) );
                                            }
                                        }
                                    } else if ( has_excerpt() ) {
                                        the_excerpt();
                                    }

                                    if ( 'on' !== $show_content ) {
                                        $more = 'on' == $show_more ? sprintf( ' <a href="%1$s" class="more-link" >%2$s</a>' , esc_url( get_permalink() ), esc_html__( 'read more', 'et_builder' ) )  : '';
                                        echo $more;
                                    }

                                    echo '</div></div>';
                                    ?>
                                    <?php } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery' ?>
                            </article> <!-- .et_pb_post -->
                        </li>
                        <?php
                    } // endwhile


                    /*if ( 'on' === $show_pagination && ! is_search() ) {
                        echo '</div> <!-- .et_pb_posts -->';

                        $container_is_closed = true;

                        if ( function_exists( 'wp_pagenavi' ) ) {
                            wp_pagenavi();
                        } else {
                            if ( et_is_builder_plugin_active() ) {
                                include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
                            } else {
                                get_template_part( 'includes/navigation', 'index' );
                            }
                        }
                    }*/

                    wp_reset_query();
                } else {
                    if ( et_is_builder_plugin_active() ) {
                        include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
                    } else {
                        get_template_part( 'includes/no-results', 'index' );
                    }
                }

                $posts = ob_get_contents();

                ob_end_clean();

                $class = " et_pb_module et_pb_bg_layout_{$background_layout}";
                $callSeeMore = pll__('See all News');
                $callBackToStart = pll__("Torna all'inizio");
                $blogLink = fetchTranslatedLink('blog');

                $output = sprintf(
                    '<div%5$s class="%1$s%3$s%6$s"%7$s>
                       <ul class="newsfeed-slider-horizontal">
				            %2$s
				            <li><a id="back-to-start" class="btn-std btn-blurb"><span class="ion-ios-arrow-thin-left"></span>%10$s</a></li>
			                %4$s
			           </ul>
			           <a href="%9$s" class="see-more-ls-call">%8$s</a>',
                    ( 'on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix' ),
                    $posts,
                    esc_attr( $class ),
                    ( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                    ( 'on' !== $fullwidth ? ' data-columns' : '' ),
                    $callSeeMore,
                    $blogLink,
                    $callBackToStart
                );

                if ( 'on' !== $fullwidth )
                    $output = sprintf( '<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output );

                return $output;
            }
        }
        new ET_Builder_Module_Blog_Horizontal_Home;
        /*class ET_Builder_Module_Blurb_AUXIELL extends ET_Builder_Module {
            function init() {
                $this->name = esc_html__( 'Blurb AUXIELL', 'auxiell' );
                $this->slug = 'et_pb_blurb_auxiell';
                $this->main_css_element = '%%order_class%%.et_pb_blurb';

                $this->whitelisted_fields = array(
                    'title',
                    'url',
                    'url_new_window',
                    'use_icon',
                    'font_icon',
                    'icon_color',
                    'use_circle',
                    'circle_color',
                    'use_circle_border',
                    'circle_border_color',
                    'image',
                    'alt',
                    'icon_placement',
                    'animation',
                    'background_layout',
                    'text_orientation',
                    'ctm_has_button',
                    'ctm_button_text',
                    'ctm_button_url',
                    'content_new',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'max_width',
                    'use_icon_font_size',
                    'icon_font_size',
                    'max_width_tablet',
                    'max_width_phone',
                    'icon_font_size_tablet',
                    'icon_font_size_phone',
                );

                $et_accent_color = et_builder_accent_color();

                $this->fields_defaults = array(
                    'url_new_window'      => array( 'off' ),
                    'use_icon'            => array( 'off' ),
                    'icon_color'          => array( $et_accent_color, 'add_default_setting' ),
                    'use_circle'          => array( 'off' ),
                    'circle_color'        => array( $et_accent_color, 'only_default_setting' ),
                    'use_circle_border'   => array( 'off' ),
                    'circle_border_color' => array( $et_accent_color, 'only_default_setting' ),
                    'icon_placement'      => array( 'top' ),
                    'animation'           => array( 'top' ),
                    'background_layout'   => array( 'light' ),
                    'text_orientation'    => array( 'center' ),
                    'ctm_has_button'      => array('off'),
                    'use_icon_font_size'  => array( 'off' ),
                );

                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__( 'Header', 'et_builder' ),
                            'css'      => array(
                                'main' => "{$this->main_css_element} h4, {$this->main_css_element} h4 a",
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__( 'Body', 'et_builder' ),
                            'css'      => array(
                                'line_height' => "{$this->main_css_element} p",
                            ),
                        ),
                    ),
                    'background' => array(
                        'settings' => array(
                            'color' => 'alpha',
                        ),
                    ),
                    'border' => array(),
                    'custom_margin_padding' => array(
                        'css' => array(
                            'important' => 'all',
                        ),
                    ),
                );
                $this->custom_css_options = array(
                    'blurb_image' => array(
                        'label'    => esc_html__( 'Blurb Image', 'et_builder' ),
                        'selector' => '.et_pb_main_blurb_image',
                    ),
                    'blurb_title' => array(
                        'label'    => esc_html__( 'Blurb Title', 'et_builder' ),
                        'selector' => 'h4',
                    ),
                    'blurb_content' => array(
                        'label'    => esc_html__( 'Blurb Content', 'et_builder' ),
                        'selector' => '.et_pb_blurb_content',
                    ),
                );
            }

            function get_fields() {
                $et_accent_color = et_builder_accent_color();

                $image_icon_placement = array(
                    'top' => esc_html__( 'Top', 'et_builder' ),
                );

                if ( ! is_rtl() ) {
                    $image_icon_placement['left'] = esc_html__( 'Left', 'et_builder' );
                } else {
                    $image_icon_placement['right'] = esc_html__( 'Right', 'et_builder' );
                }

                $fields = array(
                    'title' => array(
                        'label'           => esc_html__( 'Title', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'The title of your blurb will appear in bold below your blurb image.', 'et_builder' ),
                    ),
                    'url' => array(
                        'label'           => esc_html__( 'Url', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'If you would like to make your blurb a link, input your destination URL here.', 'et_builder' ),
                    ),
                    'url_new_window' => array(
                        'label'           => esc_html__( 'Url Opens', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'In The Same Window', 'et_builder' ),
                            'on'  => esc_html__( 'In The New Tab', 'et_builder' ),
                        ),
                        'description' => esc_html__( 'Here you can choose whether or not your link opens in a new window', 'et_builder' ),
                    ),
                    'use_icon' => array(
                        'label'           => esc_html__( 'Use Icon', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'basic_option',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'     => array(
                            '#et_pb_font_icon',
                            '#et_pb_use_circle',
                            '#et_pb_icon_color',
                            '#et_pb_image',
                            '#et_pb_alt',
                        ),
                        'description' => esc_html__( 'Here you can choose whether icon set below should be used.', 'et_builder' ),
                    ),
                    'font_icon' => array(
                        'label'               => esc_html__( 'Icon', 'et_builder' ),
                        'type'                => 'text',
                        'option_category'     => 'basic_option',
                        'class'               => array( 'et-pb-font-icon' ),
                        'renderer'            => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'description'         => esc_html__( 'Choose an icon to display with your blurb.', 'et_builder' ),
                        'depends_default'     => true,
                    ),
                    'icon_color' => array(
                        'label'             => esc_html__( 'Icon Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'description'       => esc_html__( 'Here you can define a custom color for your icon.', 'et_builder' ),
                        'depends_default'   => true,
                    ),
                    'use_circle' => array(
                        'label'           => esc_html__( 'Circle Icon', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_use_circle_border',
                            '#et_pb_circle_color',
                        ),
                        'description' => esc_html__( 'Here you can choose whether icon set above should display within a circle.', 'et_builder' ),
                        'depends_default'   => true,
                    ),
                    'circle_color' => array(
                        'label'           => esc_html__( 'Circle Color', 'et_builder' ),
                        'type'            => 'color',
                        'description'     => esc_html__( 'Here you can define a custom color for the icon circle.', 'et_builder' ),
                        'depends_default' => true,
                    ),
                    'use_circle_border' => array(
                        'label'           => esc_html__( 'Show Circle Border', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'layout',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_circle_border_color',
                        ),
                        'description' => esc_html__( 'Here you can choose whether if the icon circle border should display.', 'et_builder' ),
                        'depends_default'   => true,
                    ),
                    'circle_border_color' => array(
                        'label'           => esc_html__( 'Circle Border Color', 'et_builder' ),
                        'type'            => 'color',
                        'description'     => esc_html__( 'Here you can define a custom color for the icon circle border.', 'et_builder' ),
                        'depends_default' => true,
                    ),
                    'image' => array(
                        'label'              => esc_html__( 'Image', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose an Image', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Image', 'et_builder' ),
                        'depends_show_if'    => 'off',
                        'description'        => esc_html__( 'Upload an image to display at the top of your blurb.', 'et_builder' ),
                    ),
                    'alt' => array(
                        'label'           => esc_html__( 'Image Alt Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the HTML ALT text for your image here.', 'et_builder' ),
                        'depends_show_if' => 'off',
                    ),
                    'icon_placement' => array(
                        'label'             => esc_html__( 'Image/Icon Placement', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => $image_icon_placement,
                        'description'       => esc_html__( 'Here you can choose where to place the icon.', 'et_builder' ),
                    ),
                    'animation' => array(
                        'label'             => esc_html__( 'Image/Icon Animation', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'top'    => esc_html__( 'Top To Bottom', 'et_builder' ),
                            'left'   => esc_html__( 'Left To Right', 'et_builder' ),
                            'right'  => esc_html__( 'Right To Left', 'et_builder' ),
                            'bottom' => esc_html__( 'Bottom To Top', 'et_builder' ),
                            'off'    => esc_html__( 'No Animation', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'This controls the direction of the lazy-loading animation.', 'et_builder' ),
                    ),
                    'background_layout' => array(
                        'label'             => esc_html__( 'Text Color', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'color_option',
                        'options'           => array(
                            'light' => esc_html__( 'Dark', 'et_builder' ),
                            'dark'  => esc_html__( 'Light', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder' ),
                    ),
                    'text_orientation' => array(
                        'label'             => esc_html__( 'Text Orientation', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => et_builder_get_text_orientation_options(),
                        'description'       => esc_html__( 'This will control how your blurb text is aligned.', 'et_builder' ),
                    ),
                    'ctm_has_button' => array(
                        'label'           => esc_html__( 'Has Button', 'auxiell' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'basic_option',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description' => esc_html__( 'Here you can choose whether if the link button should display.', 'auxiell' ),
                    ),
                    'ctm_button_text' => array(
                        'label'           => esc_html__( 'Button Text', 'auxiell' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the text for your link button here.', 'auxiell' ),
                    ),
                    'ctm_button_url' => array(
                        'label'           => esc_html__( 'Button URL', 'auxiell' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'Define the url for your link button here.', 'auxiell' ),
                    ),
                    'content_new' => array(
                        'label'             => esc_html__( 'Content', 'et_builder' ),
                        'type'              => 'tiny_mce',
                        'option_category'   => 'basic_option',
                        'description'       => esc_html__( 'Input the main text content for your module here.', 'et_builder' ),
                    ),
                    'max_width' => array(
                        'label'           => esc_html__( 'Image Max Width', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'layout',
                        'tab_slug'        => 'advanced',
                        'mobile_options'  => true,
                        'validate_unit'   => true,
                    ),
                    'use_icon_font_size' => array(
                        'label'           => esc_html__( 'Use Icon Font Size', 'et_builder' ),
                        'type'            => 'yes_no_button',
                        'option_category' => 'font_option',
                        'options'         => array(
                            'off' => esc_html__( 'No', 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'     => array(
                            '#et_pb_icon_font_size',
                        ),
                        'tab_slug' => 'advanced',
                    ),
                    'icon_font_size' => array(
                        'label'           => esc_html__( 'Icon Font Size', 'et_builder' ),
                        'type'            => 'range',
                        'option_category' => 'font_option',
                        'tab_slug'        => 'advanced',
                        'default'         => '96px',
                        'range_settings' => array(
                            'min'  => '1',
                            'max'  => '120',
                            'step' => '1',
                        ),
                        'mobile_options'  => true,
                        'depends_default' => true,
                    ),
                    'max_width_tablet' => array (
                        'type' => 'skip',
                    ),
                    'max_width_phone' => array (
                        'type' => 'skip',
                    ),
                    'icon_font_size_tablet' => array(
                        'type' => 'skip',
                    ),
                    'icon_font_size_phone' => array(
                        'type' => 'skip',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $module_id             = $this->shortcode_atts['module_id'];
                $module_class          = $this->shortcode_atts['module_class'];
                $title                 = $this->shortcode_atts['title'];
                $url                   = $this->shortcode_atts['url'];
                $image                 = $this->shortcode_atts['image'];
                $url_new_window        = $this->shortcode_atts['url_new_window'];
                $alt                   = $this->shortcode_atts['alt'];
                $background_layout     = $this->shortcode_atts['background_layout'];
                $text_orientation      = $this->shortcode_atts['text_orientation'];
                $ctm_has_button      = $this->shortcode_atts['ctm_has_button'];
                $ctm_button_text      = $this->shortcode_atts['ctm_button_text'];
                $ctm_button_url      = $this->shortcode_atts['ctm_button_url'];
                $animation             = $this->shortcode_atts['animation'];
                $icon_placement        = $this->shortcode_atts['icon_placement'];
                $font_icon             = $this->shortcode_atts['font_icon'];
                $use_icon              = $this->shortcode_atts['use_icon'];
                $use_circle            = $this->shortcode_atts['use_circle'];
                $use_circle_border     = $this->shortcode_atts['use_circle_border'];
                $icon_color            = $this->shortcode_atts['icon_color'];
                $circle_color          = $this->shortcode_atts['circle_color'];
                $circle_border_color   = $this->shortcode_atts['circle_border_color'];
                $max_width             = $this->shortcode_atts['max_width'];
                $max_width_tablet      = $this->shortcode_atts['max_width_tablet'];
                $max_width_phone       = $this->shortcode_atts['max_width_phone'];
                $use_icon_font_size    = $this->shortcode_atts['use_icon_font_size'];
                $icon_font_size        = $this->shortcode_atts['icon_font_size'];
                $icon_font_size_tablet = $this->shortcode_atts['icon_font_size_tablet'];
                $icon_font_size_phone  = $this->shortcode_atts['icon_font_size_phone'];

                $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

                if ( 'off' !== $use_icon_font_size ) {
                    $font_size_values = array(
                        'desktop' => $icon_font_size,
                        'tablet'  => $icon_font_size_tablet,
                        'phone'   => $icon_font_size_phone,
                    );

                    et_pb_generate_responsive_css( $font_size_values, '%%order_class%% .et-pb-icon', 'font-size', $function_name );
                }

                if ( '' !== $max_width_tablet || '' !== $max_width_phone || '' !== $max_width ) {
                    $max_width_values = array(
                        'desktop' => $max_width,
                        'tablet'  => $max_width_tablet,
                        'phone'   => $max_width_phone,
                    );

                    et_pb_generate_responsive_css( $max_width_values, '%%order_class%% .et_pb_main_blurb_image img', 'max-width', $function_name );
                }

                if ( is_rtl() && 'left' === $text_orientation ) {
                    $text_orientation = 'right';
                }

                if ( is_rtl() && 'left' === $icon_placement ) {
                    $icon_placement = 'right';
                }

                if ( '' !== $title && '' !== $url ) {
                    $title = sprintf( '<a href="%1$s"%3$s>%2$s</a>',
                        esc_url( $url ),
                        esc_html( $title ),
                        ( 'on' === $url_new_window ? ' target="_blank"' : '' )
                    );
                }

                if ( '' !== $title ) {
                    $title = "<h4>{$title}</h4>";
                }

                if ( '' !== trim( $image ) || '' !== $font_icon ) {
                    if ( 'off' === $use_icon ) {
                        $image = sprintf(
                            '<img src="%1$s" alt="%2$s" class="et-waypoint%3$s" />',
                            esc_url( $image ),
                            esc_attr( $alt ),
                            esc_attr( " et_pb_animation_{$animation}" )
                        );
                    } else {
                        $icon_style = sprintf( 'color: %1$s;', esc_attr( $icon_color ) );

                        if ( 'on' === $use_circle ) {
                            $icon_style .= sprintf( ' background-color: %1$s;', esc_attr( $circle_color ) );

                            if ( 'on' === $use_circle_border ) {
                                $icon_style .= sprintf( ' border-color: %1$s;', esc_attr( $circle_border_color ) );
                            }
                        }

                        $image = sprintf(
                            '<span class="et-pb-icon et-waypoint%2$s%3$s%4$s" style="%5$s">%1$s</span>',
                            esc_attr( et_pb_process_font_icon( $font_icon ) ),
                            esc_attr( " et_pb_animation_{$animation}" ),
                            ( 'on' === $use_circle ? ' et-pb-icon-circle' : '' ),
                            ( 'on' === $use_circle && 'on' === $use_circle_border ? ' et-pb-icon-circle-border' : '' ),
                            $icon_style
                        );
                    }

                    $image = sprintf(
                        '<div class="et_pb_main_blurb_image">%1$s</div>',
                        ( '' !== $url
                            ? sprintf(
                                '<a href="%1$s"%3$s>%2$s</a>',
                                esc_url( $url ),
                                $image,
                                ( 'on' === $url_new_window ? ' target="_blank"' : '' )
                            )
                            : $image
                        )
                    );
                }

                $class = " et_pb_module et_pb_bg_layout_{$background_layout} et_pb_text_align_{$text_orientation}";

                if('on' === $ctm_has_button){
                    $button = sprintf('
                        <a href="%1$s" class="et_pb_button btn-std btn-auxiell-blurb" target="_blank">%2$s</a>',
                        ('' !== $ctm_button_url ? $ctm_button_url : ''),
                        ('' !== $ctm_button_text ? $ctm_button_text : '')
                    );
                }else{
                    $button = '';
                }

                $output = sprintf(
                    '<div%5$s class="et_pb_blurb%4$s%6$s%7$s">
                <div class="et_pb_blurb_content">
                    %2$s
                    <div class="et_pb_blurb_container">
                        %3$s
                        %1$s
                        %8$s
                    </div>
                </div> <!-- .et_pb_blurb_content -->
            </div> <!-- .et_pb_blurb -->',
                    $this->shortcode_content,
                    $image,
                    $title,
                    esc_attr( $class ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                    sprintf( ' et_pb_blurb_position_%1$s', esc_attr( $icon_placement ) ),
                    $button
                );

                return $output;
            }
        }
        new ET_Builder_Module_Blurb_AUXIELL;*/
        class ET_Builder_Module_Image_Background_AUXIELL extends ET_Builder_Module {
            function init() {
                $this->name = esc_html__( 'Image Background AUXIELL', 'auxiell' );
                $this->slug = 'et_pb_image_bg_auxiell';

                $this->whitelisted_fields = array(
                    'src',
                    'alt',
                    'title_text',
                    'show_in_lightbox',
                    'url',
                    'url_new_window',
                    'animation',
                    'sticky',
                    'align',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'max_width',
                    'force_fullwidth',
                    'always_center_on_mobile',
                    'use_overlay',
                    'overlay_icon_color',
                    'hover_overlay_color',
                    'hover_icon',
                    'max_width_tablet',
                    'max_width_phone',
                );

                $this->fields_defaults = array(
                    'show_in_lightbox'        => array( 'off' ),
                    'url_new_window'          => array( 'off' ),
                    'animation'               => array( 'left' ),
                    'sticky'                  => array( 'off' ),
                    'align'                   => array( 'left' ),
                    'force_fullwidth'         => array( 'off' ),
                    'always_center_on_mobile' => array( 'on' ),
                    'use_overlay'             => array( 'off' ),
                );

                $this->advanced_options = array(
                    'border'                => array(),
                    'custom_margin_padding' => array(
                        'use_padding' => false,
                        'css' => array(
                            'important' => 'all',
                        ),
                    ),
                );
            }

            function get_fields() {
                // List of animation options
                $animation_options_list = array(
                    'left'    => esc_html__( 'Left To Right', 'et_builder' ),
                    'right'   => esc_html__( 'Right To Left', 'et_builder' ),
                    'top'     => esc_html__( 'Top To Bottom', 'et_builder' ),
                    'bottom'  => esc_html__( 'Bottom To Top', 'et_builder' ),
                    'fade_in' => esc_html__( 'Fade In', 'et_builder' ),
                    'off'     => esc_html__( 'No Animation', 'et_builder' ),
                );

                $animation_option_name       = sprintf( '%1$s-animation', $this->slug );
                $default_animation_direction = ET_Global_Settings::get_value( $animation_option_name );

                // If user modifies default animation option via Customizer, we'll need to change the order
                if ( 'left' !== $default_animation_direction && ! empty( $default_animation_direction ) && array_key_exists( $default_animation_direction, $animation_options_list ) ) {
                    // The options, sans user's preferred direction
                    $animation_options_wo_default = $animation_options_list;
                    unset( $animation_options_wo_default[ $default_animation_direction ] );

                    // All animation options
                    $animation_options = array_merge(
                        array( $default_animation_direction => $animation_options_list[$default_animation_direction] ),
                        $animation_options_wo_default
                    );
                } else {
                    // Simply copy the animation options
                    $animation_options = $animation_options_list;
                }

                $fields = array(
                    'src' => array(
                        'label'              => esc_html__( 'Image URL', 'et_builder' ),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
                        'choose_text'        => esc_attr__( 'Choose an Image', 'et_builder' ),
                        'update_text'        => esc_attr__( 'Set As Image', 'et_builder' ),
                        'description'        => esc_html__( 'Upload your desired image, or type in the URL to the image you would like to display.', 'et_builder' ),
                    ),
                    'alt' => array(
                        'label'           => esc_html__( 'Image Alternative Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'This defines the HTML ALT text. A short description of your image can be placed here.', 'et_builder' ),
                    ),
                    'title_text' => array(
                        'label'           => esc_html__( 'Image Title Text', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__( 'This defines the HTML Title text.', 'et_builder' ),
                    ),
                    'show_in_lightbox' => array(
                        'label'             => esc_html__( 'Open in Lightbox', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( "No", 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_url',
                            '#et_pb_url_new_window',
                            '#et_pb_use_overlay'
                        ),
                        'description'       => esc_html__( 'Here you can choose whether or not the image should open in Lightbox. Note: if you select to open the image in Lightbox, url options below will be ignored.', 'et_builder' ),
                    ),
                    'url' => array(
                        'label'           => esc_html__( 'Link URL', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'depends_show_if' => 'off',
                        'affects'         => array(
                            '#et_pb_use_overlay',
                        ),
                        'description'     => esc_html__( 'If you would like your image to be a link, input your destination URL here. No link will be created if this field is left blank.', 'et_builder' ),
                    ),
                    'url_new_window' => array(
                        'label'             => esc_html__( 'Url Opens', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off' => esc_html__( 'In The Same Window', 'et_builder' ),
                            'on'  => esc_html__( 'In The New Tab', 'et_builder' ),
                        ),
                        'depends_show_if'   => 'off',
                        'description'       => esc_html__( 'Here you can choose whether or not your link opens in a new window', 'et_builder' ),
                    ),
                    'use_overlay' => array(
                        'label'             => esc_html__( 'Image Overlay', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( 'Off', 'et_builder' ),
                            'on'  => esc_html__( 'On', 'et_builder' ),
                        ),
                        'affects'           => array(
                            '#et_pb_overlay_icon_color',
                            '#et_pb_hover_overlay_color',
                            '#et_pb_hover_icon',
                        ),
                        'depends_default'   => true,
                        'description'       => esc_html__( 'If enabled, an overlay color and icon will be displayed when a visitors hovers over the image', 'et_builder' ),
                    ),
                    'overlay_icon_color' => array(
                        'label'             => esc_html__( 'Overlay Icon Color', 'et_builder' ),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Here you can define a custom color for the overlay icon', 'et_builder' ),
                    ),
                    'hover_overlay_color' => array(
                        'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'custom_color'      => true,
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Here you can define a custom color for the overlay', 'et_builder' ),
                    ),
                    'hover_icon' => array(
                        'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
                        'type'                => 'text',
                        'option_category'     => 'configuration',
                        'class'               => array( 'et-pb-font-icon' ),
                        'renderer'            => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'depends_show_if'     => 'on',
                        'description'       => esc_html__( 'Here you can define a custom icon for the overlay', 'et_builder' ),
                    ),
                    'animation' => array(
                        'label'             => esc_html__( 'Animation', 'et_builder' ),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => $animation_options,
                        'description'       => esc_html__( 'This controls the direction of the lazy-loading animation.', 'et_builder' ),
                    ),
                    'sticky' => array(
                        'label'             => esc_html__( 'Remove Space Below The Image', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off'     => esc_html__( 'No', 'et_builder' ),
                            'on'      => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Here you can choose whether or not the image should have a space below it.', 'et_builder' ),
                    ),
                    'align' => array(
                        'label'           => esc_html__( 'Image Alignment', 'et_builder' ),
                        'type'            => 'select',
                        'option_category' => 'layout',
                        'options' => array(
                            'left'   => esc_html__( 'Left', 'et_builder' ),
                            'center' => esc_html__( 'Center', 'et_builder' ),
                            'right'  => esc_html__( 'Right', 'et_builder' ),
                        ),
                        'description'       => esc_html__( 'Here you can choose the image alignment.', 'et_builder' ),
                    ),
                    'max_width' => array(
                        'label'           => esc_html__( 'Image Max Width', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'layout',
                        'tab_slug'        => 'advanced',
                        'mobile_options'  => true,
                        'validate_unit'   => true,
                    ),
                    'force_fullwidth' => array(
                        'label'             => esc_html__( 'Force Fullwidth', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'off' => esc_html__( "No", 'et_builder' ),
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                        ),
                        'tab_slug'    => 'advanced',
                    ),
                    'always_center_on_mobile' => array(
                        'label'             => esc_html__( 'Always Center Image On Mobile', 'et_builder' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'layout',
                        'options'           => array(
                            'on'  => esc_html__( 'Yes', 'et_builder' ),
                            'off' => esc_html__( "No", 'et_builder' ),
                        ),
                        'tab_slug'    => 'advanced',
                    ),
                    'max_width_tablet' => array(
                        'type' => 'skip',
                    ),
                    'max_width_phone' => array(
                        'type' => 'skip',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__( 'Disable on', 'et_builder' ),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__( 'Phone', 'et_builder' ),
                            'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
                            'desktop' => esc_html__( 'Desktop', 'et_builder' ),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__( 'Admin Label', 'et_builder' ),
                        'type'        => 'text',
                        'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__( 'CSS ID', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__( 'CSS Class', 'et_builder' ),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );

                return $fields;
            }

            function shortcode_callback( $atts, $content = null, $function_name ) {
                $module_id               = $this->shortcode_atts['module_id'];
                $module_class            = $this->shortcode_atts['module_class'];
                $src                     = $this->shortcode_atts['src'];
                $alt                     = $this->shortcode_atts['alt'];
                $title_text              = $this->shortcode_atts['title_text'];
                $animation               = $this->shortcode_atts['animation'];
                $url                     = $this->shortcode_atts['url'];
                $url_new_window          = $this->shortcode_atts['url_new_window'];
                $show_in_lightbox        = $this->shortcode_atts['show_in_lightbox'];
                $sticky                  = $this->shortcode_atts['sticky'];
                $align                   = $this->shortcode_atts['align'];
                $max_width               = $this->shortcode_atts['max_width'];
                $max_width_tablet        = $this->shortcode_atts['max_width_tablet'];
                $max_width_phone         = $this->shortcode_atts['max_width_phone'];
                $force_fullwidth         = $this->shortcode_atts['force_fullwidth'];
                $always_center_on_mobile = $this->shortcode_atts['always_center_on_mobile'];
                $overlay_icon_color      = $this->shortcode_atts['overlay_icon_color'];
                $hover_overlay_color     = $this->shortcode_atts['hover_overlay_color'];
                $hover_icon              = $this->shortcode_atts['hover_icon'];
                $use_overlay             = $this->shortcode_atts['use_overlay'];

                $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

                if ( 'on' === $always_center_on_mobile ) {
                    $module_class .= ' et_always_center_on_mobile';
                }

                // overlay can be applied only if image has link or if lightbox enabled
                $is_overlay_applied = 'on' === $use_overlay && ( 'on' === $show_in_lightbox || ( 'off' === $show_in_lightbox && '' !== $url ) ) ? 'on' : 'off';

                if ( '' !== $max_width_tablet || '' !== $max_width_phone || '' !== $max_width ) {
                    $max_width_values = array(
                        'desktop' => $max_width,
                        'tablet'  => $max_width_tablet,
                        'phone'   => $max_width_phone,
                    );

                    et_pb_generate_responsive_css( $max_width_values, '%%order_class%%', 'max-width', $function_name );
                }

                if ( 'on' === $force_fullwidth ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%% img',
                        'declaration' => 'width: 100%;',
                    ) );
                }

                if ( $this->fields_defaults['align'][0] !== $align ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%',
                        'declaration' => sprintf(
                            'text-align: %1$s;',
                            esc_html( $align )
                        ),
                    ) );
                }

                if ( 'center' !== $align ) {
                    ET_Builder_Element::set_style( $function_name, array(
                        'selector'    => '%%order_class%%',
                        'declaration' => sprintf(
                            'margin-%1$s: 0;',
                            esc_html( $align )
                        ),
                    ) );
                }

                if ( 'on' === $is_overlay_applied ) {
                    if ( '' !== $overlay_icon_color ) {
                        ET_Builder_Element::set_style( $function_name, array(
                            'selector'    => '%%order_class%% .et_overlay:before',
                            'declaration' => sprintf(
                                'color: %1$s !important;',
                                esc_html( $overlay_icon_color )
                            ),
                        ) );
                    }

                    if ( '' !== $hover_overlay_color ) {
                        ET_Builder_Element::set_style( $function_name, array(
                            'selector'    => '%%order_class%% .et_overlay',
                            'declaration' => sprintf(
                                'background-color: %1$s;',
                                esc_html( $hover_overlay_color )
                            ),
                        ) );
                    }

                    $data_icon = '' !== $hover_icon
                        ? sprintf(
                            ' data-icon="%1$s"',
                            esc_attr( et_pb_process_font_icon( $hover_icon ) )
                        )
                        : '';

                    $overlay_output = sprintf(
                        '<span class="et_overlay%1$s"%2$s></span>',
                        ( '' !== $hover_icon ? ' et_pb_inline_icon' : '' ),
                        $data_icon
                    );
                }

                $output = sprintf(
                    '<img src="%1$s" alt="%2$s"%3$s />
<div class="bg-image" style="background-image:url(%1$s)"></div>
			%4$s',
                    esc_url( $src ),
                    esc_attr( $alt ),
                    ( '' !== $title_text ? sprintf( ' title="%1$s"', esc_attr( $title_text ) ) : '' ),
                    'on' === $is_overlay_applied ? $overlay_output : ''
                );

                if ( 'on' === $show_in_lightbox ) {
                    $output = sprintf( '<a href="%1$s" class="et_pb_lightbox_image" title="%3$s">%2$s</a>',
                        esc_url( $src ),
                        $output,
                        esc_attr( $alt )
                    );
                } else if ( '' !== $url ) {
                    $output = sprintf( '<a href="%1$s"%3$s>%2$s</a>',
                        esc_url( $url ),
                        $output,
                        ( 'on' === $url_new_window ? ' target="_blank"' : '' )
                    );
                }

                $animation = '' === $animation ? ET_Global_Settings::get_value( 'et_pb_image-animation' ) : $animation;

                $output = sprintf(
                    '<div%5$s class="et_pb_module et-waypoint et_pb_image%2$s%3$s%4$s%6$s background-image-auxiell">
				%1$s
			</div>',
                    $output,
                    esc_attr( " et_pb_animation_{$animation}" ),
                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( ltrim( $module_class ) ) ) : '' ),
                    ( 'on' === $sticky ? esc_attr( ' et_pb_image_sticky' ) : '' ),
                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                    'on' === $is_overlay_applied ? ' et_pb_has_overlay' : ''
                );

                return $output;
            }
        }
        new ET_Builder_Module_Image_Background_AUXIELL;
    };

}
add_action('et_builder_ready', 'ex_divi_child_theme_setup');
